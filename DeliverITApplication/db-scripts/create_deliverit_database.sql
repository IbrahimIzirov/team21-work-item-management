DROP SCHEMA IF EXISTS `deliverit`;
create database if not exists deliverit;
use deliverit;

create or replace table categories
(
    category_id          int auto_increment
        primary key,
    category_name        varchar(20) not null,
    category_description text        not null
);

create or replace table countries
(
    country_id   int auto_increment
        primary key,
    country_name varchar(30) not null,
    constraint countries_country_name_uindex
        unique (country_name)
);

create or replace table cities
(
    city_id    int auto_increment
        primary key,
    city_name  varchar(20) not null,
    country_id int         not null,
    constraint cities_countries_fk
        foreign key (country_id) references countries (country_id)
);

create or replace table addresses
(
    address_id  int auto_increment
        primary key,
    city_id     int  not null,
    street_name text null,
    constraint addresses_cities_fk
        foreign key (city_id) references cities (city_id)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    value   varchar(30) not null
);

create or replace table shipment_status
(
    shipment_status_id int auto_increment
        primary key,
    description        varchar(30) not null,
    constraint order_status_description_uindex
        unique (description)
);

create or replace table users
(
    user_id       int auto_increment
        primary key,
    username    varchar(30) not null,
    password     varchar(30) not null,
    first_name    varchar(15) not null,
    last_name     varchar(15) not null,
    email_address text        not null,
    address_id    int         not null,
    constraint customers_address_fk
        foreign key (address_id) references addresses (address_id)
);

create or replace table users_roles
(
    user_id int not null,
    role_id int not null,
    constraint users_roles_roles_fk
        foreign key (role_id) references roles (role_id),
    constraint users_roles_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table warehouses
(
    warehouse_id int auto_increment
        primary key,
    address_id   int not null,
    constraint warehouses_address_fk
        foreign key (address_id) references addresses (address_id)
);

create or replace table parcels
(
    parcel_id    int auto_increment
        primary key,
    warehouse_id int    not null,
    user_id      int    not null,
    weight       double null,
    category_id  int    not null,
    constraint parcels__categories_fk
        foreign key (category_id) references categories (category_id),
    constraint parcels_users_fk
        foreign key (user_id) references users (user_id),
    constraint parcels_warehouse_fk
        foreign key (warehouse_id) references warehouses (warehouse_id)
);

create or replace table shipments
(
    shipment_id     int auto_increment
        primary key,
    shipment_date   date not null,
    departure_date  date not null,
    arrival_date    date not null,
    shipment_status int  not null,
    warehouse_id    int  not null,
    constraint orders_status_fk
        foreign key (shipment_status) references shipment_status (shipment_status_id),
    constraint shipments_warehouse_fk
        foreign key (warehouse_id) references warehouses (warehouse_id)
);

create or replace table shipments_details
(
    shipment_id int not null,
    parcel_id   int not null,
    constraint order_details_orders_fk
        foreign key (shipment_id) references shipments (shipment_id),
    constraint order_details_parcels__fk
        foreign key (parcel_id) references parcels (parcel_id)
);

