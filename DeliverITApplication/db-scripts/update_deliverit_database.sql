use deliverit;

INSERT INTO `countries` (`country_id`, `country_name`)
VALUES (1, 'UK'),
       (2, 'USA'),
       (3, 'Bulgaria'),
       (4, 'Romania'),
       (5, 'Germany'),
       (6, 'Spanish'),
       (7, 'Portugal'),
       (8, 'Slovakia'),
       (9, 'Albania'),
       (10, 'Russia');

INSERT INTO `cities` (`city_id`, `city_name`, country_id)
VALUES (1, 'West', 1),
       (2, 'Calgary', 2),
       (3, 'Sofia', 3),
       (4, 'Bucharest', 4),
       (5, 'Berlin', 5),
       (6, 'Madrid', 6),
       (7, 'Lisabon', 7),
       (8, 'Bratislava', 8),
       (9, 'Tirana', 9),
       (10, 'Moscow', 10);



INSERT INTO `addresses` (`address_id`, street_name, city_id)
VALUES (1, '108 Lakeside Court', 5),
       (2, '1343 Prospect St', 3),
       (3, '1648 Eastgate Lane', 2),
       (4, '2284 Azalea Avenue', 6),
       (5, '2947 Vine Lane', 8),
       (6, '3067 Maya', 10),
       (7, '3197 Thornhill Place', 1),
       (8, '3284 S. Blank Avenue', 4),
       (9, '332 Laguna Niguel', 7),
       (10, '3454 Bel Air Drive', 9);

INSERT INTO `users` (`user_id`, username, password, first_name, last_name, email_address, address_id)
VALUES (1, 'ibrahim', 1234, 'Ibrahim', 'Izirov', 'ibrahim.izirov@gmail.com', 9),
       (2, 'kevin', 1234, 'Kevin', 'Brown', 'kevinbrown@gmail.com', 5),
       (3, 'roberto', 1234, 'Roberto', 'Tamburello', 'robroberto@abv.bg', 4),
       (4, 'rob', 1234, 'Rob', 'Walters', 'robwalters@yahoo.com', 2),
       (5, 'thierry', 1234, 'Thierry', 'Hers', 'thierryhers@gmail.com', 3),
       (6, 'david', 1234, 'David', 'Bradley', 'davidbrad@gmail.com', 6),
       (7, 'jolynn', 1234, 'JoLynn', 'Dobney', 'jolynn.dobney@yahoo.com', 7),
       (8, 'ruth', 1234, 'Ruth', 'Ellerbrock', 'ruthellerbrock@abv.bg', 1),
       (9, 'gail', 1234, 'Gail', 'Erickson', 'gail.erickson@abv.bg', 10),
       (10, 'todor', 1234, 'Todor', 'Kanev', 'todor_kanchev@yahoo.com', 8);

INSERT INTO `roles` (`role_id`, `value`)
VALUES (1, 'Customer'),
       (2, 'Employee');

INSERT INTO `users_roles`(`user_id`, `role_id`)
VALUES (1, 2),
       (2, 2),
       (3, 2),
       (4, 2),
       (5, 1),
       (6, 1),
       (7, 1),
       (8, 1),
       (9, 1),
       (10, 2);


INSERT INTO `warehouses` (`warehouse_id`, address_id)
VALUES (1, 5),
       (2, 3),
       (3, 4),
       (4, 8),
       (5, 1),
       (6, 4);

INSERT INTO `categories` (`category_id`, category_name, category_description)
VALUES (1, 'Electronics', 'Electronic packaging is the design and production of enclosures for electronic devices.'),
       (2, 'Cosmetics', 'Cosmetics have some of MakeUp, lipsticks, shampoos, shower gels & etc.'),
       (3, 'Medical', 'Medical products for health life - vitamins, food supplements, pills, proteins.'),
       (4, 'Food', 'Some of food semi-finished products, fresh food, bio food.'),
       (5, 'Toys, hobby', 'Toys for children or babies, things from different hobbies.');

INSERT INTO `shipment_status` (`shipment_status_id`, description)
VALUES (1, 'Preparing'),
       (2, 'On the way'),
       (3, 'Completed');

INSERT INTO `shipments` (`shipment_id`, shipment_date, departure_date, arrival_date, shipment_status, warehouse_id)
VALUES (1, '2000-01-24', '2000-01-25', '2000-01-27', 1, 2),
       (2, '2000-02-24', '2000-02-25', '2000-02-26', 2, 4),
       (3, '2000-03-24', '2000-03-25', '2000-03-26', 2, 2),
       (4, '2000-04-24', '2000-04-25', '2000-04-28', 2, 1),
       (5, '2000-05-24', '2000-05-25', '2000-05-29', 3, 3),
       (6, '2000-06-24', '2000-06-25', '2000-06-30', 1, 4),
       (7, '2000-07-24', '2000-07-25', '2000-07-26', 1, 3),
       (8, '2000-08-24', '2000-08-25', '2000-08-27', 3, 1),
       (9, '2000-09-24', '2000-09-25', '2000-09-28', 2, 4),
       (10, '2000-10-24', '2000-10-25', '2000-10-26', 2, 1);

INSERT INTO `parcels` (`parcel_id`, warehouse_id, user_id, weight, category_id)
VALUES (1, 1, 6, 12.14, 2),
       (2, 3, 5, 20.00, 3),
       (3, 5, 8, 8.09, 4),
       (4, 6, 2, 2.04, 1),
       (5, 3, 2, 10.02, 5),
       (6, 2, 2, 9.81, 3),
       (7, 4, 3, 4.53, 4),
       (8, 3, 3, 3.12, 5),
       (9, 2, 2, 7.12, 1),
       (10, 4, 6, 8.12, 3);

INSERT INTO `shipments_details` (`shipment_id`, parcel_id)
VALUES (1, 2),
       (2, 3),
       (3, 4),
       (4, 6),
       (5, 7),
       (6, 8),
       (7, 9),
       (8, 10),
       (9, 1),
       (10, 5);