# TEAM 18

Trello link: https://trello.com/b/kb7xwBen/deliverit

Documentation:
Swagger-UI - http://localhost:8080/swagger-ui.html

---
## DeliverIT

![alt](https://image.freepik.com/free-vector/delivery-logo-template-with-gradient-effect_23-2147886226.jpg)

### A web application that helps shippers streamline supply chain operations, simplify the shipping process and increase logistical efficiency. ... Also known as a transportation management system.

#Areas
---
- **Public part** - visible to visitors.
- **Private part** - available to registered users only.
- **Administration part** - Available to administrators only(full control).

#Project description:
DeliverIT is a web application that serves the needs of a freight forwarding company.
DeliverIT's customers can place orders on international shopping sites (like Amazon.de or eBay.com)
and have their parcels delivered to the company's warehouses.
Here is an example use case – you’ve order something online from a foreign shopping site and you wish to leave handling,
customs fees, and transportation to somebody else. Here is where DeliverIT comes in.
Their main warehouse is in Bulgaria, for example, and they have other warehouses in different countries,
like Germany, Spain, USA, etc. When placing your order, you address it to a DeliverIT warehouse.
When the package arrives, the employees see who it is for and create a parcel in the system.
If your parcel appears on the site, then it has arrived at the warehouse successfully and the next step
is for it to depart to the main warehouse.

#Steps to build
Prerequisites: In order to build and run the project you must first install all the related IDE,
Frameworks and tools described above in technologies used section.

1. Clone or download the repository.
2. Open the project in your IDE.
3. Run provided SQL scripts in db-scripts folder of the project.
4. Build the project and run it.
To load all functionality form DeliverIT (the source code) you have
one options:
Once the project is started go to swagger documentation (http://localhost:8080/swagger-ui.html) and you will see them all
endpoints you can access.
- Use postman and run localhost:8080/api/{endpoints}.
You can now use the application freely.

#Database diagram
![](Images/deliverit.png)

