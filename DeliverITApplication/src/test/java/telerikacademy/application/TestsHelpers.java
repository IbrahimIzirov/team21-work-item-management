package telerikacademy.application;

import telerikacademy.models.*;

import java.time.LocalDate;
import java.util.Set;

public class TestsHelpers {

    public static Countries createMockCountry() {
        var mockCountry = new Countries();
        mockCountry.setId(1);
        mockCountry.setName("Portugal");

        return mockCountry;
    }

    public static Cities createMockCity() {
        var mockCity = new Cities();
        mockCity.setId(1);
        mockCity.setName("Tavira");
        mockCity.setCountryId(createMockCountry());

        return mockCity;
    }

    public static Address createMockAddress() {
        var mockAddress = new Address();
        mockAddress.setId(1);
        mockAddress.setCityId(createMockCity());
        mockAddress.setStreetName("Pariha voi 32");

        return mockAddress;
    }

    public static Warehouses createMockWarehouse() {
        var mockWarehouse = new Warehouses();
        mockWarehouse.setId(1);
        mockWarehouse.setAddressId(createMockAddress());

        return mockWarehouse;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("Cosmetics");
        mockCategory.setDescription("Cosmetics have some of MakeUp");

        return mockCategory;
    }

    public static Parcels createMockParcels() {
        var mockParcel = new Parcels();
        mockParcel.setId(1);
        mockParcel.setUserId(createMockUser());
        mockParcel.setCategoryId(createMockCategory());
        mockParcel.setWeight(12.13);
        mockParcel.setWarehouseId(createMockWarehouse());

        return mockParcel;
    }

    public static Shipments createMockShipment() {
        var mockShipment = new Shipments();
        mockShipment.setId(1);
        mockShipment.setArrivalDate(LocalDate.of(2021, 2, 21));
        mockShipment.setShipmentDate(LocalDate.now());
        mockShipment.setWarehouses(createMockWarehouse());
        mockShipment.setDepartureDate(LocalDate.of(2021, 2, 23));
        mockShipment.setParcelsSet(Set.of(new Parcels(3,
                createMockWarehouse(),
                createMockUser(),
                10.11,
                createMockCategory())));

        return mockShipment;
    }

    public static Users createMockUser() {
        var mockUser = new Users();
        mockUser.setRoles(Set.of(new Roles(1, "Customer")));
        mockUser.setId(1);
        mockUser.setFirstName("John");
        mockUser.setLastName("West");
        mockUser.setEmail("john@gmail.com");
        mockUser.setAddressId(createMockAddress());

        return mockUser;
    }


}