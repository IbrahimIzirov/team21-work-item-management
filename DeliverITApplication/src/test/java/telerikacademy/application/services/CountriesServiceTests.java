package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Countries;
import telerikacademy.models.Roles;
import telerikacademy.repositories.contract.CountriesRepository;
import telerikacademy.services.CountriesServiceImpl;

import javax.management.relation.Role;

import java.util.Set;

import static telerikacademy.application.TestsHelpers.createMockCountry;
import static telerikacademy.application.TestsHelpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class CountriesServiceTests {

    @Mock
    CountriesRepository mockRepository;

    @InjectMocks
    CountriesServiceImpl service;

    @Test
    public void getAll_Should_ReturnAllCountries() {
        var mockCountry = createMockCountry();

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();

    }

    @Test
    public void getByName_Should_ReturnCountry_WithSameName() {
        var mockCountry = createMockCountry();

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCountry);

        Countries result = service.getByName(mockCountry.getName());

        Assertions.assertEquals(mockCountry.getId(), result.getId());
        Assertions.assertEquals(mockCountry.getName(), result.getName());
    }


    @Test
    public void getById_Should_ReturnCountry_When_MatchExists() {

        var mockCountry = createMockCountry();

        Mockito.when(mockRepository.getById(mockCountry.getId()))
                .thenReturn(mockCountry);

        Countries result = service.getById(mockCountry.getId());

        Assertions.assertEquals(mockCountry.getId(), result.getId());
        Assertions.assertEquals(mockCountry.getName(), result.getName());
    }

    @Test
    public void create_Should_Throw_When_CountryWithSameNameExist() {
        //Arrange
        var mockCountry = createMockCountry();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCountry);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCountry, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_CountryWithSameNameExist() {
        var mockCountry = createMockCountry();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(mockCountry.getName()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockCountry, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Countries.class));
    }

    @Test
    public void update_Should_Throw_When_CountryNameIsTaken() {
        var mockCountry = createMockCountry();
        var anotherMockCountry = createMockCountry();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        anotherMockCountry.setId(2);

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockCountry);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCountry, mockUser));
    }

    @Test
    public void update_Should_Throw_When_CountryNotFound() {
        var mockCountry = createMockCountry();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockCountry, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(Mockito.any(Countries.class));
    }

    @Test
    public void delete_Should_CallRepository_WithSameId() {
        var mockCountry = createMockCountry();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.delete(mockCountry.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).
                delete(Mockito.anyInt());
    }



}
