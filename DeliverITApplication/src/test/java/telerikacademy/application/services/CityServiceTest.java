package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.shadow.com.univocity.parsers.common.input.LineSeparatorDetector;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Cities;
import telerikacademy.models.Countries;
import telerikacademy.models.Roles;
import telerikacademy.repositories.CitiesRepositoryImpl;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.services.CityServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static telerikacademy.application.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {


    @Mock
    CitiesRepository mockRepository;

    @InjectMocks
    CityServiceImpl service;

    @Test
    public void getAll_Should_Return_CitiesList_WhenCallRepository() {
        // Arrange
        Cities mockCity = createMockCity();
        List<Cities> citiesList = new ArrayList<>();
        citiesList.add(mockCity);

        Mockito.when(service.getAll()).thenReturn(citiesList);

        //Act
        List<Cities> result = service.getAll();

        // Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getById_Should_Return_City_WhenMatched() {
        // Arrange
        Cities mockCity = createMockCity();

        Mockito.when(service.getById(Mockito.anyInt())).thenReturn(mockCity);

        //Act
        Cities result = service.getById(3);

        // Assert
        Assertions.assertEquals(mockCity.getId(), result.getId());
        Assertions.assertEquals(mockCity.getName(), result.getName());
        Assertions.assertEquals(mockCity.getName(), result.getName());
    }

    @Test
    public void getByName_Should_Return_City_whenValidParameter() {
        Cities mockCity = createMockCity();

        Mockito.when(service.getByName(Mockito.anyString())).thenReturn(mockCity);

        Cities result = service.getByName("Tavira");

        Assertions.assertEquals(mockCity.getName(), result.getName());
    }

    @Test
    public void create_Should_Throw_When_CityWithSameNameExist() {
        //Arrange
        var mockCity = createMockCity();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCity);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCity, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_CityWithSameNameExist() {
        var mockCity = createMockCity();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(mockCity.getName()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockCity, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Cities.class));
    }

    @Test
    public void update_Should_Throw_When_CityNameIsTaken() {
        var mockCity = createMockCity();
        var anotherMockCity = createMockCity();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        anotherMockCity.setId(2);

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockCity);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCity, mockUser));
    }

    @Test
    public void update_Should_Throw_When_CityNotFound() {
        var mockCity = createMockCity();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockCity, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(Mockito.any(Cities.class));
    }

    @Test
    public void delete_Should_CallRepository_WithSameId() {
        var mockCity = createMockCity();
        var mockUser = createMockUser();
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.delete(mockCity.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1)).
                delete(Mockito.anyInt());
    }
}
