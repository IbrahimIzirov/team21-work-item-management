package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.*;
import telerikacademy.repositories.contract.WarehouseRepository;
import telerikacademy.services.WarehouseServiceImpl;

import java.util.Set;

import static telerikacademy.application.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceTests {

    @Mock
    WarehouseRepository mockRepository;

    @InjectMocks
    WarehouseServiceImpl service;

    private Countries mockCountry;
    private Cities mockCity;
    private Address mockAddress;
    private Users mockUser;
    private Warehouses mockWarehouse;

    @BeforeEach
    public void before() {
         mockCountry = createMockCountry();
         mockCity = createMockCity();
         mockAddress = createMockAddress();
         mockUser = createMockUser();
         mockWarehouse = createMockWarehouse();
    }


    @Test
    public void getAll_Should_CallRepository_When_CallMethod(){
        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_Should_ReturnWarehouse_When_MatchExists() {
        //Arrange
        Mockito.when(mockRepository.getById(2))
                .thenReturn(new Warehouses(2, mockAddress));

        //Act
        Warehouses result = service.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals(1, result.getAddressId().getId());
    }

    @Test
    public void create_Should_ReturnNewWarehouse_When_CorrectInput() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.create(mockWarehouse,mockUser);

        //Assert
        Mockito.verify(mockRepository,Mockito.times(1))
                .create(mockWarehouse);
    }

    @Test
    public void update_Should_CallRepository_When_IdMatch(){
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(service.getById(Mockito.anyInt())).thenReturn(mockWarehouse);

        //Act
        service.update(mockWarehouse,mockUser);

        //Assert
        Mockito.verify(mockRepository,Mockito.times(1))
                .update(mockWarehouse);
    }

    @Test
    public void update_Should_Throw_When_WarehouseIdNotFound(){
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(service.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException("Warehouse","Id",String.valueOf(mockWarehouse.getId())));

        //Assert, Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(mockWarehouse,mockUser));
    }

    @Test
    public void delete_Should_CallRepository_When_MethodCall(){
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.delete(Mockito.anyInt(),mockUser);

        //Assert
        Mockito.verify(mockRepository,Mockito.times(1))
                .delete(Mockito.anyInt());

    }


}
