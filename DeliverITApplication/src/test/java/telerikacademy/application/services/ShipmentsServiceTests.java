package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Roles;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.ShipmentsRepository;
import telerikacademy.services.ShipmentsServiceImpl;

import java.util.Optional;
import java.util.Set;

import static telerikacademy.application.TestsHelpers.*;

@ExtendWith(MockitoExtension.class)
public class ShipmentsServiceTests {

    @Mock
    ShipmentsRepository mockRepository;

    @InjectMocks
    ShipmentsServiceImpl service;

    private Users mockUser;
    private Users actionUser;
    private Shipments mockShipment;
    private Parcels mockParcel;

    @BeforeEach
    public void before() {
        mockUser = createMockUser();
        actionUser = createMockUser();
        mockShipment = createMockShipment();
        mockParcel = createMockParcels();
    }

    @Test
    public void getAll_Should_ReturnAllShipments_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.getAll(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();

    }

    @Test
    public void getAll_Should_Thrown_When_UserIsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }

    @Test
    public void getById_Should_ReturnShipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.getById(Mockito.anyInt(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(mockUser.getId(), actionUser));
    }

    @Test
    public void getByStatus_Should_ReturnShipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.getByStatus(Optional.empty(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByStatus(Optional.empty());
    }

    @Test
    public void getByStatus_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByStatus(Optional.empty(), actionUser));
    }

    @Test
    public void create_Should_Create_Shipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.create(mockShipment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockShipment);
    }

    @Test
    public void create_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockShipment, actionUser));
    }

    @Test
    public void update_Should_Update_Shipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.update(mockShipment, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockShipment);
    }

    @Test
    public void update_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockShipment, actionUser));
    }

    @Test
    public void delete_Should_Delete_Shipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.delete(mockShipment.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockShipment.getId());
    }

    @Test
    public void delete_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockShipment.getId(), actionUser));
    }

    @Test
    public void filterByCustomer_Should_Return_ListOfShipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.filterByCustomer(Mockito.anyInt(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByCustomer(Mockito.anyInt());
    }

    @Test
    public void filterByCustomer_Should_Throw_When_User_IsNotAuthenticated() {
        actionUser.setId(3);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.filterByCustomer(actionUser.getId(), mockUser));
    }

    @Test
    public void filterByWarehouse_Should_Return_ListOfShipment_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        service.filterByWarehouse(Optional.empty(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .filterByWarehouse(Optional.empty());
    }

    @Test
    public void filterByWarehouse_Should_Throw_When_User_IsNotAuthenticated() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.filterByWarehouse(Optional.empty(), mockUser));
    }

    @Test
    public void addToShipment_Should_CallRepository_When_CorrectDate() {

        service.addToShipments(mockShipment, mockParcel);

        Mockito.verify(mockRepository, Mockito.times(1))
                .addToShipments(mockShipment, mockParcel);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockShipment);
    }

    @Test
    public void deleteToShipment_Should_Thrown_When_InCorrectDate() {

        mockParcel.setId(10);
        mockShipment.setParcelsSet(Set.of(mockParcel));

        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> service.deleteToShipments(mockShipment, mockParcel));
    }
}
