package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.stereotype.Service;
import telerikacademy.models.Address;
import telerikacademy.models.Cities;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.services.AddressesServiceImpl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static telerikacademy.application.TestsHelpers.createMockAddress;
import static telerikacademy.application.TestsHelpers.createMockCity;

@ExtendWith(MockitoExtension.class)
public class AddressesServiceTest {

    @Mock
    AddressesRepository mockRepository;

    @InjectMocks
    AddressesServiceImpl service;


    @Test
    public void getById_Should_ReturnAddress_When_MacheExist(){
        //Arrange
        Cities mockCity = createMockCity();

        Mockito.when(mockRepository.getById(2)).thenReturn(new Address(2,mockCity,"Hristo Botev"));

        //Act
        Address result = service.getById(2);

        //Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals(mockCity.getId(), result.getCityId().getId());
        Assertions.assertEquals("Hristo Botev", result.getStreetName());
    }

    @Test
    public void getAll_Should_CallRepository_When_TryGetAllAddresses(){
        //Arrange
        Address mockAddress = createMockAddress();
        List<Address> addressList = new ArrayList<>();
        addressList.add(mockAddress);

        Mockito.when(mockRepository.getAll()).thenReturn(addressList);

        //Act
        List<Address> result = service.getAll();

        //Assert
        Assertions.assertEquals(1, result.size());

    }
}
