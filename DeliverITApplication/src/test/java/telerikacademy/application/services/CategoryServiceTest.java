package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Category;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.CategoryRepository;
import telerikacademy.services.CategoryServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static telerikacademy.application.TestsHelpers.createMockCategory;
import static telerikacademy.application.TestsHelpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    private Category mockCategory ;
    private Users mockUser ;


    @BeforeEach
    public void setup(){
        mockCategory = createMockCategory();
        mockUser = createMockUser();

    }


    @Test
    public void getAll_Should_ReturnCategoryList_When_CAllMethod() {
        // Arrange

        List<Category> categoryList = new ArrayList<>();
        categoryList.add(mockCategory);

        Mockito.when(service.getAll()).thenReturn(categoryList);

        // Act
        List<Category> result = service.getAll();

        // Assert
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getById_Should_ReturnCategory_When_IdMatched() {
        // Arrange
        Mockito.when(service.getById(Mockito.anyInt())).thenReturn(mockCategory);

        // Act
        Category result = service.getById(6);

        // Assert
        Assertions.assertEquals(mockCategory.getId(), result.getId());
        Assertions.assertEquals(mockCategory.getName(), result.getName());
        Assertions.assertEquals(mockCategory.getDescription(), result.getDescription());
    }

    @Test
    public void getByName_Should_ReturnCategory_When_NameMach() {
        // Arrange
        Mockito.when(service.getByName(Mockito.anyString())).thenReturn(mockCategory);

        // Act
        Category result = service.getByName("Electronics");

        // Assert
        Assertions.assertEquals(mockCategory.getName(), result.getName());
    }

    @Test
    public void create_Should_Throw_When_CategoryWithSameNameExist() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(service.getByName(mockCategory.getName())).thenReturn(mockCategory);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockCategory, mockUser));
    }

    @Test
    public void create_Should_CallRepository_When_CategoryValid() throws Exception {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        service.create(mockCategory, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Category.class));
    }

    @Test
    public void create_Should_Throw_When_InvalidUser() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Customer")));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockCategory, mockUser));

    }

    @Test
    public void update_Should_Throw_When_CategoryWithSameNameExist() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));
        Category existingCategory = createMockCategory();
        existingCategory.setId(mockCategory.getId() + 1);

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenReturn(existingCategory);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCategory, mockUser));
    }


    @Test
    public void update_Should_CallRepository_When_UpdateSameCategory() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCategory);
        // Act
        service.update(mockCategory, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).update(mockCategory);
    }

    @Test
    public void update_Should_Throw_When_InvalidUser() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Customer")));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCategory, mockUser));

    }

    @Test
    public void update_Should_Throw_When_CategoryNotFound() {

        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Category existingCategory = createMockCategory();
        existingCategory.setId(mockCategory.getId() + 1);

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenThrow(new EntityNotFoundException("Category", "name", mockCategory.getName()));

        // Act
        service.update(mockCategory, mockUser);

        // Assert
       Mockito.verify(mockRepository, Mockito.times(1))
       .update(Mockito.any(Category.class));
    }

    @Test
    public void delete_Should_CallRepository_When_ValidUser() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.delete(1,mockUser);

        //Assert
        Mockito.verify(mockRepository,Mockito.times(1)).
                delete(Mockito.anyInt());

    }

}
