package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.ParcelsRepository;
import telerikacademy.services.ParcelsServiceImpl;

import java.util.Optional;
import java.util.Set;

import static telerikacademy.application.TestsHelpers.createMockParcels;
import static telerikacademy.application.TestsHelpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class ParcelsServiceTest {

    @Mock
    ParcelsRepository mockRepository;

    @InjectMocks
    ParcelsServiceImpl service;

    Users mockUser;
    Parcels mockParcel;

    @BeforeEach
    public void setup() {
        mockUser = createMockUser();
        mockParcel = createMockParcels();
    }


    @Test
    public void getAll_Should_ReturnListParcels_When_CallMethod() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.getAll(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_Should_Throw_When_UserIsNotEmployee() {

        //Assert, Act
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getAll(mockUser));
    }


    @Test
    public void getById_Should_ReturnParcel_When_IdMatch() {
        // Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(service.getById(1, mockUser)).thenReturn(mockParcel);

        //Act
        Parcels result = service.getById(1, mockUser);

        //Assert
        Assertions.assertEquals(mockParcel.getId(), result.getId());
        Assertions.assertEquals(mockParcel.getCategoryId(), result.getCategoryId());
        Assertions.assertEquals(mockParcel.getWeight(), result.getWeight());
        Assertions.assertEquals(mockParcel.getUserId(), result.getUserId());
        Assertions.assertEquals(mockParcel.getWarehouseId(), result.getWarehouseId());
    }

    @Test
    public void create_Should_CallRepository_When_CorrectUser() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.create(mockParcel, mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Parcels.class));

    }

    @Test
    public void update_Should_CallRepository_When_IdMath() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockParcel);

        //Act
        service.update(mockParcel, mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(Mockito.any(Parcels.class));
    }

    @Test
    public void update_Should_Throw_When_ParcelsIdNotFound() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException("Parcel", "ID", String.valueOf(mockParcel.getId())));

        //Assert, Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.update(mockParcel, mockUser));
    }

    @Test
    public void delete_Should_CollRepository_When_IdMatch() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockParcel);

        //Act
        service.delete(Mockito.anyInt(), mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).
                delete(Mockito.anyInt());
    }

    @Test
    public void delete_Should_Throw_When_ParcelIdNotFound() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenThrow(new EntityNotFoundException("Parcel", "ID", String.valueOf(mockParcel.getId())));

        //Assert, Act
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.delete(Mockito.anyInt(), mockUser));
    }

    @Test
    public void filterBy_Should_CallRepository_When_CallMethod() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.filterBy(Optional.empty(),
                (Optional.empty()),
                (Optional.empty()),
                (Optional.empty()),
                (Optional.empty()),
                mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filterBy(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()));
    }

    @Test
    public void filterBy_Should_Thrown_When_OrderByParameter_IsNotCorrect() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.filterBy(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.of("ascSomething")),
                        mockUser));
    }

    @Test
    public void SortBy_Should_CallRepository_When_CallMethod() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Act
        service.sortBy(Optional.empty(),
                (Optional.empty()),
                mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .sortBy(Optional.empty(),
                        (Optional.empty()));
    }

    @Test
    public void sortBy_Should_Thrown_When_OrderByWeightParameter_IsNotCorrect() {
        //Arrange
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        //Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> service.sortBy((Optional.of("ascSomething")),
                        Optional.empty(),
                        mockUser));
    }
}
