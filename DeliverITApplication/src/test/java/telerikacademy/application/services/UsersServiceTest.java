package telerikacademy.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.UserRepository;
import telerikacademy.services.UsersServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static telerikacademy.application.TestsHelpers.createMockParcels;
import static telerikacademy.application.TestsHelpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UsersServiceTest {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UsersServiceImpl usersService;

    private Users mockUser;
    private Users actionUser;

    @BeforeEach
    public void before() {
        mockUser = createMockUser();
        actionUser = createMockUser();
    }

    @Test
    public void getAll_Should_ReturnAllUsers_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.getAll(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();

    }

    @Test
    public void getAll_Should_Thrown_When_UserIsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.getAll(mockUser));
    }

    @Test
    public void getByFirstName_Should_Return_CorrectUser() {

        usersService.getByFirstName(mockUser.getFirstName());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByFirsName(mockUser.getFirstName());
    }

    @Test
    public void getByLastName_Should_Return_CorrectUser() {

        usersService.getByLastName(mockUser.getLastName());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByLastName(mockUser.getLastName());
    }

    @Test
    public void getByEmail_Should_Return_CorrectUser() {

        usersService.getByEmail(mockUser.getEmail());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByEmail(mockUser.getEmail());
    }

    @Test
    public void getById_Should_ReturnUser_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.getById(Mockito.anyInt(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_Throw_When_User_IsNotSameCustomerOrEmployee() {
        actionUser.setId(3);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.getById(mockUser.getId(), actionUser));
    }

    @Test
    public void getById_Should_Return_User_When_UserIsCustomer() {

        usersService.getById(mockUser.getId(), actionUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockUser.getId());
    }

    @Test
    public void update_Should_Call_Repository_WhenUserIsEmployee() {

        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getByFirsName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        usersService.update(mockUser, actionUser);

        verify(mockRepository, times(1))
                .update(mockUser);
    }

    @Test
    public void update_Should_Thrown_WhenUserIsNotCreatorOrSameCustomer() {
        actionUser.setId(3);


        Mockito.when(mockRepository.getByFirsName(Mockito.anyString()))
                .thenReturn(mockUser);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.update(mockUser, actionUser));
    }

    @Test
    public void update_Should_Thrown_WhenUserAlreadyExist() {

        actionUser.setId(2);

        Mockito.when(mockRepository.getByFirsName(Mockito.anyString()))
                .thenReturn(actionUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> usersService.update(mockUser, actionUser));
    }

    @Test
    public void create_Should_Register_User_When_UserIsNew() {
        Mockito.when(mockRepository.getByFirsName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        usersService.create(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void create_Should_Thrown_When_UserExist() {
        Mockito.when(mockRepository.getByFirsName(Mockito.anyString()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> usersService.create(mockUser));
    }

    @Test
    public void delete_Should_Thrown_When_ParcelsList_IsNotEmpty() {
        actionUser.setRoles(Set.of(new Roles(2, "Employee")));

        List<Parcels> parcels = List.of(createMockParcels());

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Mockito.when(mockRepository.getCustomerParcels(mockUser.getId()))
                .thenReturn(parcels);

        Assertions.assertThrows(EmptyListException.class,
                () -> usersService.delete(mockUser.getId(), actionUser));
    }

    @Test
    public void delete_Should_Thrown_When_UserIsNotFound() {

        actionUser.setRoles(Set.of(new Roles(2, "Employee")));

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> usersService.delete(mockUser.getId(), actionUser));
    }

    @Test
    public void delete_Should_Thrown_When_UserIsNotEmployee() {
        actionUser.setId(5);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.delete(mockUser.getId(), actionUser));
    }


    @Test
    public void delete_Should_Call_Repository_WhenUserIsEmployee() {
        actionUser.setRoles(Set.of(new Roles(2, "Employee")));

        List<Parcels> parcels = new ArrayList<>();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Mockito.when(mockRepository.getCustomerParcels(mockUser.getId()))
                .thenReturn(parcels);

        usersService.delete(mockUser.getId(), actionUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }

    @Test
    public void delete_Should_Call_Repository_WhenUserISamePerson() {

        List<Parcels> parcels = new ArrayList<>();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        Mockito.when(mockRepository.getCustomerParcels(mockUser.getId()))
                .thenReturn(parcels);

        usersService.delete(mockUser.getId(), actionUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockUser.getId());
    }

    @Test
    public void incomingParcels_Should_Return_ListOfParcels_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.incomingParcels(Mockito.anyInt(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .incomingParcels(Mockito.anyInt());
    }

    @Test
    public void incomingParcels_Should_Thrown_When_UserIsNotAuthorized() {

        actionUser.setId(3);

        Assertions.assertThrows(EmptyListException.class,
                () -> usersService.incomingParcels(mockUser.getId(), actionUser));
    }

    @Test
    public void search_By_Multiple_Should_Thrown_When_UserIsNotAuthorized() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.searchByMultiple(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()),
                        mockUser));
    }

    @Test
    public void search_By_Multiple_Should_ReturnFilter_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.searchByMultiple(Optional.empty(),
                (Optional.empty()),
                (Optional.empty()),
                (Optional.empty()),
                mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .searchByMultiple(Optional.empty(),
                        (Optional.empty()),
                        (Optional.empty()),
                        (Optional.empty()));
    }

    @Test
    public void search_By_OneWord_Should_Thrown_When_UserIsNotAuthorized() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.searchByOneWord(Optional.empty(),
                        mockUser));
    }

    @Test
    public void search_By_OneWord_Should_ReturnFilter_When_UserIsEmployee() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.searchByOneWord(Optional.empty(),
                mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .searchByOneWord(Optional.empty());
    }

    @Test
    public void getCustomerCount_Should_Return_CountOfCustomers() {
        usersService.getCustomerCount();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getCustomerCount();
    }

    @Test
    public void getCustomerParcels_Should_Return_AllParcelsOfSomeCustomer() {
        mockUser.setRoles(Set.of(new Roles(2, "Employee")));

        usersService.getCustomerParcels(Mockito.anyInt(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getCustomerParcels(Mockito.anyInt());
    }

    @Test
    public void getCustomerParcels_Should_Thrown_When_User_IsNotEmployee() {
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> usersService.getCustomerParcels(10, mockUser));
    }

    @Test
    public void getCustomerParcels_Should_ReturnList_When_User_IsOwner() {
        usersService.getCustomerParcels(mockUser.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getCustomerParcels(mockUser.getId());
    }
}
