package telerikacademy;

public class Constants {

    // Error message
    public static final String INVALID_EMAIL = "Invalid email.";
    public static final String ENTITY_NOT_FOUND = "%s with %s %s not found.";
    public static final String PARCEL_ALREADY_IS_DELETED = "Parcel with id %s already is deleted";
    public static final String SHIPMENT_WITHOUT_PARCELS_CANNOT_EXIST = "Shipment without parcels cannot exist!";
    public static final String ERROR_INCOMING_PARCELS = "Error! Incoming parcels belong to a user with id: %s";
    public static final String ERROR_NO_RIGHT = "Error! You have no right to see this parcels";
    public static final String USER_IS_NOT_EMPLOYEE = "User is not employee and there are no rights under the respective action";


    // Standard message
    public static final String RESOURCE_REQUIRES_AUTHENTICATION = "The requested resource requires authentication.";
    public static final String CUSTOMERS_COUNT = "Application have %d customers.";
    public static final String DUPLICATE_ENTITY_ERROR = "%s with %s %s already exists.";
    public static final String CITY_ID_MESSAGE = "City ID should be positive";
    public static final String STREET_NAME_MESSAGE = "Street name can't be null";
    public static final String CATEGORY_NULL_MESSAGE = "Category can't be null";
    public static final String CATEGORY_NAME_MESSAGE = "Category name should be between 2 and 20 symbols";
    public static final String DESCRIPTION_MESSAGE = "Description can't be null";
    public static final String CITY_NULL_MESSAGE = "City can't be null";
    public static final String CITY_NAME_MESSAGE = "City name should be between 2 and 20 symbols";
    public static final String COUNTRY_ID_MESSAGE = "Country ID should be positive";
    public static final String COUNTRY_NULL_MESSAGE = "Country can't be null";
    public static final String COUNTRY_NAME_MESSAGE = "Country should be between 2 and 30 symbols";
    public static final String PARCEL_ID_MESSAGE = "Parcel ID should be positive";
    public static final String USER_ID_MESSAGE = "User ID should be positive";
    public static final String WEIGHT_MESSAGE = "Weight should be positive number";
    public static final String CATEGORY_ID_MESSAGE = "Category ID should be positive";
    public static final String SHIPMENT_DATE_NULL_MESSAGE = "Shipment date can't be null";
    public static final String SHIPMENT_DATE_MESSAGE = "Shipment date should be positive";
    public static final String DEPARTURE_DATE_NULL_MESSAGE = "Departure date can't be null";
    public static final String DEPARTURE_DATE_POSITIVE_MESSAGE = "Departure date should be positive";
    public static final String ARRIVAL_DATE_NULL_MESSAGE = "Arrival date can't be null";
    public static final String ARRIVAL_DATE_POSITIVE_MESSAGE = "Arrival date should be positive";
    public static final String SHIPMENT_STATUS_NULL_MESSAGE = "Shipment status can't be null";
    public static final String SHIPMENT_STATUS_POSITIVE_MESSAGE = "Shipment status ID should be positive";
    public static final String WAREHOUSE_ID_NULL_MESSAGE = "Warehouse_id can't be null";
    public static final String WAREHOUSE_ID_POSITIVE_MESSAGE = "Warehouse_id should be positive";
    public static final String USER_FIRST_NAME_NULL_MESSAGE = "User first name can't be null";
    public static final String USER_NAME_SYMBOLS_MESSAGE = "User name should be between 2 and 15 symbols";
    public static final String USER_LAST_NULL_MESSAGE = "User last name can't be null";
    public static final String EMAIL_CAN_NULL_MESSAGE = "Email can't be null";
    public static final String ADDRESS_ID_POSITIVE_MESSAGE = "Address ID should be positive";
    public static final String ADDRESS_ID_NULL_MESSAGE = "Address Id can't be null";
    public static final String ORDER_TYPE_MESSAGE = "Order type must by ascending or descending";
    public static final String MODIFY_ERROR_MESSAGE = "Only creator or admin can see or modify current Shipment.";
    public static final String USERS_VISIBLE_DATA_MESSAGE = "Users can only see their own data!";
    public static final String MODIFY_USERS_MESSAGE = "Only creator or admin can modify users.";
    public static final String NOT_COMPLETED_PARCELS_MESSAGE = "User with id: %d has parcels which are not completed";
    public static final String USER_IS_NOT_EMPLOYEE_OR_CUSTOMER = "User is not employee or customer.";
    public static final String NOT_COMPLETED_CITY_MESSAGE = "City with id: %d belong to country.";
    public static final String NOT_COMPLETED_COUNTRY_MESSAGE = "Country with id: %d has cities and cannot be deleted.";
    public static final String NOT_COMPLETED_WAREHOUSE_MESSAGE = "Warehouse with id: %d has Parcels and cannot be deleted.";

    // Pages
    public static final String NOT_FOUND_PAGE = "notFoundPage";
    public static final String CREATE_CATEGORY_PAGE = "category-new";
    public static final String UPDATE_CATEGORY_PAGE = "category-update";
    public static final String CREATE_PARCEL_PAGE = "parcel-new";
    public static final String UPDATE_PARCEL_PAGE = "parcel-update";
    public static final String CREATE_WAREHOUSE_PAGE = "warehouse-new";
    public static final String UPDATE_WAREHOUSE_PAGE = "warehouse-update";
    public static final String CREATE_ADDRESS_PAGE = "address-new";
    public static final String UPDATE_USER_PAGE = "user-update";
    public static final String NOT_AUTHORIZED_PAGE = "not-authorized";
    public static final String SHOW_ALL_CATEGORIES_PAGE = "categories";
    public static final String SHOW_ALL_USERS_PAGE = "users";
    public static final String SHOW_CUSTOMER_PROFILE_PAGE = "profile-user-customer";
    public static final String SHOW_EMPLOYEE_PROFILE_PAGE = "profile-user";
    public static final String SHOW_ALL_WAREHOUSES_PAGE = "warehouses";
    public static final String CREATE_CITY_PAGE = "city-new";
    public static final String UPDATE_CITY_PAGE = "city-update";
    public static final String SHOW_ALL_SHIPMENTS_PAGE = "shipments";
    public static final String CREATE_SHIPMENT_PAGE = "shipment-new";
    public static final String CREATE_COUNTRY_PAGE = "country-new";
    public static final String UPDATE_COUNTRY_PAGE = "country-update";
    public static final String SHIPMENT_UPDATE_PAGE = "shipment-update";
    public static final String SHIPMENT_NEW_PAGE = "shipment-new";
    public static final String ERROR_PAGE = "error";
}
