package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Roles;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.ShipmentsRepository;
import telerikacademy.repositories.contract.UserRepository;
import telerikacademy.services.contract.UsersService;

import java.util.List;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    private final UserRepository repository;
    private final ShipmentsRepository shipmentsRepository;

    @Autowired
    public UsersServiceImpl(UserRepository repository, ShipmentsRepository shipmentsRepository) {
        this.repository = repository;
        this.shipmentsRepository = shipmentsRepository;
    }

    @Override
    public List<Users> getAll(Users user) {

        VerifyHelper.verifyIEmployee(user);

        return repository.getAll();
    }

    @Override
    public Users getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public Users getByFirstName(String firstName) {
        return repository.getByFirsName(firstName);
    }

    @Override
    public Users getByLastName(String lastName) {
        return repository.getByLastName(lastName);
    }

    @Override
    public Users getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public Users getById(int id, Users user) {

        VerifyHelper.verifyIsCustomerOrEmployee(user);

        if (id != user.getId() && !user.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.USERS_VISIBLE_DATA_MESSAGE);
        }

        return repository.getById(id);
    }

    @Override
    public void create(Users user) {
        boolean duplicateExists = true;
        try {
            repository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "name", user.getUsername());
        }
        repository.create(user);
    }

    @Override
    public void update(Users user, Users userToUpdate) {

        VerifyHelper.verifyIsCustomerOrEmployee(user);

        boolean duplicateExists = true;

        try {
            Users existingCustomer = repository.getByUsername(user.getUsername());
            if (existingCustomer.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "name", user.getFirstName() + " " + user.getLastName());
        }

        if (userToUpdate.getId() != user.getId() && !user.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.MODIFY_USERS_MESSAGE);
        }

        repository.update(userToUpdate);
    }

    @Override
    public void delete(int id, Users user) {

        VerifyHelper.verifyIsCustomerOrEmployee(user);

        Users userToDelete;
        try {
            userToDelete = repository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }

        if (id != user.getId() && !user.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.MODIFY_USERS_MESSAGE);
        }

        List<Parcels> parcels = repository.getCustomerParcels(id);

        if (!parcels.isEmpty()) {
            throw new EmptyListException(
                    String.format(Constants.NOT_COMPLETED_PARCELS_MESSAGE, userToDelete.getId()));
        }
        repository.delete(id);
    }

    @Override
    public List<Parcels> incomingParcels(int userId, Users user) {

        VerifyHelper.verifyIsCustomerOrEmployee(user);

        if (userId != user.getId() && !user.isEmployee()) {
            throw new EmptyListException(String
                    .format(Constants.ERROR_INCOMING_PARCELS, userId));
        }

        return repository.incomingParcels(userId);
    }

    @Override
    public List<Users> searchByMultiple(Optional<String> firstName,
                                        Optional<String> lastName,
                                        Optional<String> email,
                                        Optional<String> address,
                                        Users user) {

        VerifyHelper.verifyIEmployee(user);

        return repository.searchByMultiple(firstName, lastName, email, address);
    }

    @Override
    public List<Users> searchByOneWord(Optional<String> oneWord, Users user) {

        VerifyHelper.verifyIEmployee(user);

        return repository.searchByOneWord(oneWord);
    }

    @Override
    public Long getCustomerCount() {
        return repository.getCustomerCount();
    }

    @Override
    public List<Parcels> getCustomerParcels(int id, Users user) {
        VerifyHelper.verifyIsCustomerOrEmployee(user);

        if (id != user.getId() && !user.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.ERROR_NO_RIGHT);
        }

        return repository.getCustomerParcels(id);
    }

    @Override
    public Roles addRole(Users user, Roles role) {

        repository.addRole(user, role);
        repository.update(user);

        return role;
    }
}
