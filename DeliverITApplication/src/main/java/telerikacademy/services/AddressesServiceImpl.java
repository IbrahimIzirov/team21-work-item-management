package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.models.Address;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.services.contract.AddressesService;

import java.util.List;

@Service
public class AddressesServiceImpl implements AddressesService {

    private final AddressesRepository repository;

    @Autowired
    public AddressesServiceImpl(AddressesRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Address> getAll() {
        return repository.getAll();
    }

    @Override
    public Address getById(int id) {
        return repository.getById(id);
    }



}
