package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Cities;
import telerikacademy.models.Countries;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.repositories.contract.CountriesRepository;
import telerikacademy.services.contract.CityService;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private final CitiesRepository citiesRepository;
    private final CountriesRepository countriesRepository;

    @Autowired
    public CityServiceImpl(CitiesRepository repository,
                           CountriesRepository countriesRepository) {
        this.citiesRepository = repository;
        this.countriesRepository = countriesRepository;
    }

    @Override
    public List<Cities> getAll() {
        return citiesRepository.getAllCities();
    }

    @Override
    public Cities getById(int id) {
        return citiesRepository.getById(id);
    }

    @Override
    public Cities getByName(String name) {
        return citiesRepository.getByName(name);
    }

    @Override
    public void create(Cities city, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            citiesRepository.getByName(city.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("City", "name", city.getName());
        }

        citiesRepository.create(city);
    }

    @Override
    public void update(Cities city, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            Cities existingCity = citiesRepository.getByName(city.getName());

            if (existingCity.getId() == city.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("City", "name", city.getName());
        }

        citiesRepository.update(city);
    }

    @Override
    public void delete(int id, Users user) {


    }
}


