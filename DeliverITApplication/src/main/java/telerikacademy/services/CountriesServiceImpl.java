package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Cities;
import telerikacademy.models.Countries;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.repositories.contract.CountriesRepository;
import telerikacademy.services.contract.CountriesService;

import java.util.List;

@Service
public class CountriesServiceImpl implements CountriesService {

    private final CountriesRepository countriesRepository;
    private final CitiesRepository citiesRepository;

    @Autowired
    public CountriesServiceImpl(CountriesRepository repository, CitiesRepository citiesRepository) {
        this.countriesRepository = repository;
        this.citiesRepository = citiesRepository;
    }

    @Override
    public List<Countries> getAll() {
        return countriesRepository.getAll();
    }

    @Override
    public Countries getById(int id) {
        return countriesRepository.getById(id);
    }

    @Override
    public Countries getByName(String name) {
        return countriesRepository.getByName(name);
    }

    @Override
    public void create(Countries countries, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            countriesRepository.getByName(countries.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Country", "name", countries.getName());
        }

        countriesRepository.create(countries);
    }

    @Override
    public void update(Countries countries, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            Countries existingCountry = countriesRepository.getByName(countries.getName());

            if (existingCountry.getId() == countries.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Country", "name", countries.getName());
        }

        countriesRepository.update(countries);
    }

    @Override
    public void delete(int id, Users user) {

        VerifyHelper.verifyIEmployee(user);


        List<Cities> cities = citiesRepository.getAllCities();

        if (!cities.isEmpty()) {
            throw new EmptyListException(
                    String.format(Constants.NOT_COMPLETED_COUNTRY_MESSAGE, id));
        }

        countriesRepository.delete(id);
    }
}

