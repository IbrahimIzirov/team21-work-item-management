package telerikacademy.services.contract;

import telerikacademy.models.Parcels;
import telerikacademy.models.Users;

import java.util.List;
import java.util.Optional;

public interface ParcelsService {

    List<Parcels> getAll(Users user);

    Parcels getById(int id, Users user);

    void create(Parcels parcels, Users user);

    void update(Parcels parcels, Users user);

    void delete(int id, Users user);

    List<Parcels> filterBy(Optional<Double> weight,
                           Optional<String> userFilter,
                           Optional<String> warehouse,
                           Optional<String> category,
                           Optional<String> orderBy,
                           Users user);

    List<Parcels> sortBy(Optional<String> weight,
                         Optional<String> arrivalDate,
                         Users user);
}
