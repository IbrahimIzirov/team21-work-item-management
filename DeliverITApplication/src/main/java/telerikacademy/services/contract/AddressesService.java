package telerikacademy.services.contract;

import telerikacademy.models.Address;

import java.util.List;

public interface AddressesService {

    List<Address> getAll();

    Address getById(int id);
}
