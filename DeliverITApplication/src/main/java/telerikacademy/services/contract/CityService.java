package telerikacademy.services.contract;

import telerikacademy.models.Cities;
import telerikacademy.models.Users;

import java.util.List;

public interface CityService {

    List<Cities> getAll();

    //List<Cities> getAll(Optional<String> name, Optional<Integer> countryId);

    Cities getById(int id);

    Cities getByName(String name);

    void create(Cities city, Users user);

    void update(Cities city, Users user);

    void delete(int id, Users user);

}
