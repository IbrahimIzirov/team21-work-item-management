package telerikacademy.services.contract;

import telerikacademy.models.Parcels;
import telerikacademy.models.ShipmentStatus;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;

import java.util.List;
import java.util.Optional;

public interface ShipmentsService {

    List<Shipments> getAll(Users user);

    List<ShipmentStatus> getAllStatuses(Users user);

    Shipments getById(int id, Users user);

    List<Shipments> getByStatus(Optional<String> status, Users user);

    void create(Shipments shipments, Users user);

    void update(Shipments shipments, Users user);

    void delete(int id, Users user);

    List<Shipments> filterByWarehouse(Optional<String> warehouse, Users user);

    List<Shipments> filterByCustomer(int customerId, Users user);

    Parcels addToShipments(Shipments shipment, Parcels parcel);

    Parcels deleteToShipments(Shipments shipment, Parcels parcel);
}
