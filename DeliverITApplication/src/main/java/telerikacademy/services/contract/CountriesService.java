package telerikacademy.services.contract;

import telerikacademy.models.Countries;
import telerikacademy.models.Users;

import java.util.List;

public interface CountriesService {

    List<Countries> getAll();

    Countries getById(int id);

    Countries getByName(String name);

    void create(Countries countries, Users user);

    void update(Countries countries, Users user);

    void delete(int id, Users user);
}
