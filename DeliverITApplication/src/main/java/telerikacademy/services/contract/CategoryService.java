package telerikacademy.services.contract;

import telerikacademy.models.Category;
import telerikacademy.models.Users;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);

    void create(Category category, Users user);

    void update(Category category, Users user);

    void delete(int id, Users user);
}
