package telerikacademy.services.contract;

import telerikacademy.models.Roles;

import java.util.List;

public interface RolesService {

    List<Roles> getAll();

    Roles getByName(String name);
}
