package telerikacademy.services.contract;

import telerikacademy.models.Users;
import telerikacademy.models.Warehouses;

import java.util.List;

public interface WarehouseService {

    List<Warehouses> getAll();

    Warehouses getById(int id);

    void create(Warehouses warehouses, Users user);

    void update(Warehouses warehouses, Users user);

    void delete(int id, Users user);
}
