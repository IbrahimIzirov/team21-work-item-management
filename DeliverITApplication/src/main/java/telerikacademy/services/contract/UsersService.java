package telerikacademy.services.contract;

import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.models.Parcels;

import java.util.List;
import java.util.Optional;

public interface UsersService {

    Users getById(int id, Users user);

    void create(Users users);

    void update(Users users, Users userToUpdate);

    void delete(int id, Users users);

    List<Parcels> incomingParcels(int userId, Users user);

    List<Users> searchByMultiple(Optional<String> firstName,
                                 Optional<String> lastName,
                                 Optional<String> email,
                                 Optional<String> address,
                                 Users user);

    List<Users> getAll(Users user);

    Users getByUsername(String username);

    Users getByFirstName(String firstName);

    Users getByLastName(String lastName);

    Users getByEmail(String email);

    List<Users> searchByOneWord(Optional<String> oneWord,
                                Users user);

    Long getCustomerCount();

    List<Parcels> getCustomerParcels(int id, Users user);

    Roles addRole(Users user, Roles role);
}

