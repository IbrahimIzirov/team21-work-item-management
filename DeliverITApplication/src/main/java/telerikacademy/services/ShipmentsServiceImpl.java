package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.ShipmentStatus;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.ParcelsRepository;
import telerikacademy.repositories.contract.ShipmentsRepository;
import telerikacademy.services.contract.ShipmentsService;

import java.util.List;
import java.util.Optional;

@Service
public class ShipmentsServiceImpl implements ShipmentsService {

    private final ShipmentsRepository repository;
    private final ParcelsRepository parcelsRepository;

    @Autowired
    public ShipmentsServiceImpl(ShipmentsRepository repository, ParcelsRepository parcelsRepository) {
        this.repository = repository;
        this.parcelsRepository = parcelsRepository;
    }

    @Override
    public List<Shipments> getAll(Users user) {
        VerifyHelper.verifyIEmployee(user);
        return repository.getAll();
    }

    @Override
    public List<ShipmentStatus> getAllStatuses(Users user) {
        VerifyHelper.verifyIEmployee(user);
        return repository.getAllStatuses();
    }

    @Override
    public Shipments getById(int id, Users user) {
        VerifyHelper.verifyIEmployee(user);
        return repository.getById(id);
    }

    @Override
    public List<Shipments> getByStatus(Optional<String> status, Users user) {
        VerifyHelper.verifyIEmployee(user);
        return repository.getByStatus(status);
    }

    @Override
    public void create(Shipments shipments, Users user) {
        VerifyHelper.verifyIEmployee(user);
        repository.create(shipments);
    }

    @Override
    public void update(Shipments shipments, Users user) {
        VerifyHelper.verifyIEmployee(user);
        repository.update(shipments);
    }

    @Override
    public void delete(int id, Users user) {
        VerifyHelper.verifyIEmployee(user);
        repository.delete(id);
    }

    @Override
    public List<Shipments> filterByWarehouse(Optional<String> warehouse, Users user) {
        VerifyHelper.verifyIEmployee(user);
        return repository.filterByWarehouse(warehouse);
    }

    @Override
    public List<Shipments> filterByCustomer(int customerId, Users user) {
        VerifyHelper.verifyIsCustomerOrEmployee(user);
        if (customerId != user.getId() && !user.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.MODIFY_ERROR_MESSAGE);
        }
        return repository.filterByCustomer(customerId);
    }

    @Override
    public Parcels addToShipments(Shipments shipment, Parcels parcel) {

        var newList = shipment.getParcelsSet();

        parcelsRepository.create(parcel);

        newList.add(parcel);
        shipment.setParcelsSet(newList);

        repository.update(shipment);

        return parcel;
    }

    @Override
    public Parcels deleteToShipments(Shipments shipment, Parcels parcel) {


        var newList = shipment.getParcelsSet();

        newList.remove(parcel);

        if (shipment.getParcelsSet().size() > 0) {
            shipment.setParcelsSet(newList);
            repository.update(shipment);
        } else {
            repository.delete(shipment.getId());
        }

        return parcel;
    }
}
