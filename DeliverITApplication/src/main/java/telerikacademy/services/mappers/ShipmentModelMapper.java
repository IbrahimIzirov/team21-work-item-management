package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Shipments;
import telerikacademy.models.dto.ShipmentsDto;
import telerikacademy.repositories.contract.ShipmentsRepository;
import telerikacademy.repositories.contract.WarehouseRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class ShipmentModelMapper {

    private final ShipmentsRepository shipmentsRepository;
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public ShipmentModelMapper(ShipmentsRepository shipmentsRepository, WarehouseRepository warehouseRepository) {
        this.shipmentsRepository = shipmentsRepository;
        this.warehouseRepository = warehouseRepository;
    }

    public Shipments fromDto(ShipmentsDto shipmentsDto) {
        Shipments shipment = new Shipments();
        dtoToObject(shipmentsDto, shipment);
        return shipment;
    }


    public Shipments fromDto(ShipmentsDto shipmentsDto, int id) {
        Shipments shipment = shipmentsRepository.getById(id);
        dtoToObject(shipmentsDto, shipment);
        return shipment;
    }

    private void dtoToObject(ShipmentsDto shipmentsDto, Shipments shipments) {

        shipments.setShipmentDate(LocalDate.now());
        shipments.setArrivalDate((shipmentsDto.getArrivalDate()));
        shipments.setDepartureDate(shipmentsDto.getDepartureDate());
        shipments.setShipmentStatus(shipmentsRepository.getStatusById(shipmentsDto.getShipmentStatusId()));
        shipments.setWarehouses(warehouseRepository.getById(shipmentsDto.getWarehouseId()));
    }

    public ShipmentsDto toDto(Shipments shipment) {

        ShipmentsDto shipmentDto = new ShipmentsDto();
        shipmentDto.setShipmentDate(LocalDate.now());
        shipmentDto.setDepartureDate(shipment.getDepartureDate());
        shipmentDto.setArrivalDate(shipment.getArrivalDate());
        shipmentDto.setShipmentStatusId(shipment.getShipmentStatus().getId());
        shipmentDto.setWarehouseId(shipment.getWarehouses().getId());
        return shipmentDto;
    }

}
