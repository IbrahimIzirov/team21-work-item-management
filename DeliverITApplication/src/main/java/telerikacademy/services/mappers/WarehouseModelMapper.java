package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Address;
import telerikacademy.models.Warehouses;
import telerikacademy.models.dto.WarehousesDto;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.repositories.contract.WarehouseRepository;

@Component
public class WarehouseModelMapper {

    private final WarehouseRepository warehouseRepository;
    private final AddressesRepository addressesRepository;
    private final CitiesRepository citiesRepository;

    @Autowired
    public WarehouseModelMapper(WarehouseRepository warehouseRepository,
                                AddressesRepository addressesRepository,
                                CitiesRepository citiesRepository) {
        this.warehouseRepository = warehouseRepository;
        this.addressesRepository = addressesRepository;

        this.citiesRepository = citiesRepository;
    }

    public Warehouses fromDto(WarehousesDto warehousesDto) {
        Warehouses warehouses = new Warehouses();
        dtoToObject(warehousesDto, warehouses);
        return warehouses;
    }

    public Warehouses fromDto(WarehousesDto warehousesDto, int id) {
        Warehouses warehouses = warehouseRepository.getById(id);
        dtoToObject(warehousesDto, warehouses);
        return warehouses;
    }

    public WarehousesDto toDto(Warehouses warehouse) {
        WarehousesDto warehousesDto = new WarehousesDto();
        warehousesDto.setCityId(warehouse.getAddressId().getCityId().getId());
        warehousesDto.setStreetName(warehouse.getAddressId().getStreetName());
        return warehousesDto;
    }

    private void dtoToObject(WarehousesDto warehousesDto, Warehouses warehouses) {
        warehouses.setAddressId(addressesRepository.create(new Address(1, citiesRepository.getById(warehousesDto.getCityId()),
                warehousesDto.getStreetName())));
    }
}
