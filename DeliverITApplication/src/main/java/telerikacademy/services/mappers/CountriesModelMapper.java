package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Countries;
import telerikacademy.models.Parcels;
import telerikacademy.models.dto.CountriesDto;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.repositories.contract.CountriesRepository;

@Component
public class CountriesModelMapper {

    private final CountriesRepository countriesRepository;

    @Autowired
    public CountriesModelMapper(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;

    }

    public Countries fromDto(CountriesDto countriesDto) {
        Countries country = new Countries();
        dtoToObject(countriesDto, country);
        return country;
    }


    public Countries fromDto(CountriesDto countriesDto, int id) {
        Countries country = countriesRepository.getById(id);
        dtoToObject(countriesDto, country);
        return country;
    }

    private void dtoToObject(CountriesDto countriesDto, Countries countries) {
        countries.setName(countriesDto.getName());
    }

    public CountriesDto toDto(Countries countries) {
        CountriesDto countriesDto = new CountriesDto();
        countriesDto.setName(countries.getName());
        return countriesDto;
    }
}
