package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Address;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.models.dto.RegisterDto;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.repositories.contract.RolesRepository;
import telerikacademy.repositories.contract.UserRepository;

import java.util.Set;

@Component
public class RegisterMapper {

    private final String DEFAULT_ROLE = "Customer";

    private final UserRepository customerRepository;
    private final AddressesRepository addressesRepository;
    private final CitiesRepository citiesRepository;
    private final RolesRepository roleRepository;

    @Autowired
    public RegisterMapper(UserRepository customerRepository,
                          AddressesRepository addressesRepository,
                          CitiesRepository citiesRepository,
                          RolesRepository roleRepository) {
        this.customerRepository = customerRepository;
        this.addressesRepository = addressesRepository;
        this.citiesRepository = citiesRepository;
        this.roleRepository = roleRepository;
    }

    public Users fromDto(RegisterDto registerDto, Users user) {
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setAddressId(addressesRepository.create(new Address(1,
                citiesRepository.getById(registerDto.getCityId()),
                registerDto.getStreetName())));
//        user.setRoles(Set.of(roleRepository.getByName(DEFAULT_ROLE)));
        return user;
    }

    public Users updateMethod(RegisterDto registerDto, Users user) {
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setAddressId(addressesRepository.create(new Address(1,
                citiesRepository.getById(registerDto.getCityId()),
                registerDto.getStreetName())));
        user.setRoles(Set.of(roleRepository.getById(registerDto.getRoleId())));

        return user;
    }
}
