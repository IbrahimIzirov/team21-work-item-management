package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Address;
import telerikacademy.models.Parcels;
import telerikacademy.models.Users;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.models.dto.UserDto;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.repositories.contract.UserRepository;

@Component
public class UserModelMapper {

    private final UserRepository customerRepository;
    private final AddressesRepository addressesRepository;

    @Autowired
    public UserModelMapper(UserRepository customerRepository, AddressesRepository addressesRepository) {
        this.customerRepository = customerRepository;
        this.addressesRepository = addressesRepository;
    }


    public Users fromDto(UserDto userDto) {
        Users customer = new Users();
        dtoToObject(userDto, customer);
        return customer;
    }


    public Users fromDto(UserDto userDto, int id) {
        Users customer = customerRepository.getById(id);
        dtoToObject(userDto, customer);
        return customer;
    }

    public UserDto toDto(Users user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setAddressId(user.getAddressId().getId());
        return userDto;
    }

    public Users inversion(Users inputUser, Users updateUser) {
        inputUser.setUsername(updateUser.getUsername());
        inputUser.setPassword(updateUser.getPassword());
        inputUser.setFirstName(updateUser.getFirstName());
        inputUser.setLastName(updateUser.getLastName());
        inputUser.setEmail(updateUser.getEmail());
        inputUser.setAddressId(updateUser.getAddressId());
        inputUser.setRoles(updateUser.getRoles());

        return inputUser;
    }

    private void dtoToObject(UserDto userDto, Users user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setAddressId(addressesRepository.getById(userDto.getAddressId()));
    }
}
