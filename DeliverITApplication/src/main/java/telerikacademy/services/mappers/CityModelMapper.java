package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Category;
import telerikacademy.models.Cities;
import telerikacademy.models.dto.CategoryDto;
import telerikacademy.models.dto.CitiesDto;
import telerikacademy.repositories.contract.CitiesRepository;
import telerikacademy.repositories.contract.CountriesRepository;

@Component
public class CityModelMapper {

    private final CitiesRepository citiesRepository;
    private final CountriesRepository countriesRepository;

    @Autowired
    public CityModelMapper(CitiesRepository citiesRepository, CountriesRepository countriesRepository) {
        this.citiesRepository = citiesRepository;
        this.countriesRepository = countriesRepository;

    }

    public Cities fromDto(CitiesDto citiesDto) {
        Cities city = new Cities();
        dtoToObject(citiesDto, city);
        return city;
    }


    public Cities fromDto(CitiesDto citiesDto, int id) {
        Cities city = citiesRepository.getById(id);
        dtoToObject(citiesDto, city);
        return city;
    }

    private void dtoToObject(CitiesDto citiesDto, Cities city) {
        city.setName(citiesDto.getName());
        city.setCountryId(countriesRepository.getById(citiesDto.getCountryId()));
    }

    public CitiesDto toDto(Cities city) {
        CitiesDto cityDto = new CitiesDto();
        cityDto.setName(city.getName());
        cityDto.setCountryId(city.getCountryId().getId());
        return cityDto;
    }
}
