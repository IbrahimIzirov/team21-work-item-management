package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Category;
import telerikacademy.models.Parcels;
import telerikacademy.models.dto.CategoryDto;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.repositories.contract.*;

@Component
public class ParcelModelMapper {

    private final ParcelsRepository parcelsRepository;
    private final WarehouseRepository warehouseRepository;
    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;

    @Autowired
    public ParcelModelMapper(ParcelsRepository parcelsRepository,
                             WarehouseRepository warehouseRepository,
                             CategoryRepository categoryRepository,
                             UserRepository userRepository) {
        this.parcelsRepository = parcelsRepository;
        this.warehouseRepository = warehouseRepository;
        this.categoryRepository = categoryRepository;
        this.userRepository = userRepository;
    }

    public Parcels fromDto(ParcelDto parcelDto) {
        Parcels parcels = new Parcels();
        dtoToObject(parcelDto, parcels);
        return parcels;
    }

    public Parcels fromDto(ParcelDto parcelDto, int id) {
        Parcels parcels = parcelsRepository.getById(id);
        dtoToObject(parcelDto, parcels);
        return parcels;
    }

    public ParcelDto toDto(Parcels parcel) {
        ParcelDto parcelDto = new ParcelDto();
        parcelDto.setUserId(parcel.getUserId().getId());
        parcelDto.setWarehouseId(parcel.getWarehouseId().getId());
        parcelDto.setWeight(parcel.getWeight());
        parcelDto.setCategoryId(parcel.getCategoryId().getId());
        return parcelDto;
    }


    private void dtoToObject(ParcelDto parcelDto, Parcels parcels) {
        parcels.setWarehouseId(warehouseRepository.getById(parcelDto.getWarehouseId()));
        parcels.setUserId(userRepository.getById(parcelDto.getUserId()));
        parcels.setWeight(parcelDto.getWeight());
        parcels.setCategoryId(categoryRepository.getById(parcelDto.getCategoryId()));
    }
}
