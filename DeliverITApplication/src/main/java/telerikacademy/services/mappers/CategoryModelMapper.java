package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Category;
import telerikacademy.models.dto.CategoryDto;
import telerikacademy.repositories.contract.CategoryRepository;

@Component
public class CategoryModelMapper {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryModelMapper(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;

    }

    public Category fromDto(CategoryDto categoryDto) {
        Category category = new Category();
        dtoToObject(categoryDto, category);
        return category;
    }

    public Category fromDto(CategoryDto categoryDto, int id) {
        Category category = categoryRepository.getById(id);
        dtoToObject(categoryDto, category);
        return category;
    }

    public CategoryDto toDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(category.getName());
        categoryDto.setDescription(category.getDescription());
        return categoryDto;
    }

    private void dtoToObject(CategoryDto categoryDto, Category category) {
        category.setName(categoryDto.getName());
        category.setDescription(categoryDto.getDescription());
    }
}
