package telerikacademy.services.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import telerikacademy.models.Address;
import telerikacademy.models.dto.AddressDto;
import telerikacademy.repositories.contract.AddressesRepository;
import telerikacademy.repositories.contract.CitiesRepository;

@Component
public class AddressModelMapper {

    private final AddressesRepository addressesRepository;
    private final CitiesRepository citiesRepository;

    @Autowired
    public AddressModelMapper(AddressesRepository addressesRepository, CitiesRepository citiesRepository) {
        this.addressesRepository = addressesRepository;
        this.citiesRepository = citiesRepository;

    }

    public Address fromDto(AddressDto addressDto) {
        Address address = new Address();
        dtoToObject(addressDto, address);
        return address;
    }

    public Address fromDto(AddressDto addressDto, int id) {
        Address address = addressesRepository.getById(id);
        dtoToObject(addressDto, address);
        return address;
    }

    private void dtoToObject(AddressDto addressDt, Address address) {
        address.setCityId(citiesRepository.getById(addressDt.getCityId()));
        address.setStreetName(addressDt.getStreetName());
    }
}


