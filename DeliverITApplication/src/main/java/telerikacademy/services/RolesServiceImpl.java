package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.models.Roles;
import telerikacademy.repositories.contract.RolesRepository;
import telerikacademy.services.contract.RolesService;

import java.util.List;

@Service
public class RolesServiceImpl implements RolesService {

    private final RolesRepository roleRepository;

    @Autowired
    public RolesServiceImpl(RolesRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Roles> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Roles getByName(String name) {
        return roleRepository.getByName(name);
    }
}
