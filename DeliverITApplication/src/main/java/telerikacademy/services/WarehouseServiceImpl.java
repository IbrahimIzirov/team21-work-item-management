package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Address;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.models.Warehouses;
import telerikacademy.repositories.contract.ShipmentsRepository;
import telerikacademy.repositories.contract.WarehouseRepository;
import telerikacademy.services.contract.WarehouseService;

import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;
    private final ShipmentsRepository shipmentsRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository repository, ShipmentsRepository shipmentsRepository) {
        this.warehouseRepository = repository;
        this.shipmentsRepository = shipmentsRepository;
    }

    @Override
    public List<Warehouses> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouses getById(int id) {
        return warehouseRepository.getById(id);
    }

    @Override
    public void create(Warehouses warehouses, Users user) {
        VerifyHelper.verifyIEmployee(user);
        warehouseRepository.create(warehouses);
    }

    @Override
    public void update(Warehouses warehouses, Users user) {
        VerifyHelper.verifyIEmployee(user);

        try{
            Warehouses existWarehouse = warehouseRepository.getById(warehouses.getId());
        }catch (EntityNotFoundException e){
            throw new EntityNotFoundException("Warehouse", "ID", String.valueOf(warehouses.getId()));
        }

        warehouseRepository.update(warehouses);
    }

    @Override
    public void delete(int id, Users user) {
        VerifyHelper.verifyIEmployee(user);

        List<Shipments> shipments = shipmentsRepository.getAll();

        for (Shipments shipment : shipments) {
            if (shipment.getWarehouses().getId() == id){
                throw new EmptyListException(
                        String.format(Constants.NOT_COMPLETED_WAREHOUSE_MESSAGE, id));
            }
        }
        warehouseRepository.delete(id);
    }
}
