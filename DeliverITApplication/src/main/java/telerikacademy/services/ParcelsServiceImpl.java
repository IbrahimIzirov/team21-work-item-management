package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.Constants;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.ParcelsRepository;
import telerikacademy.services.contract.ParcelsService;

import java.util.List;
import java.util.Optional;

@Service
public class ParcelsServiceImpl implements ParcelsService {

    private final ParcelsRepository parcelsRepository;

    @Autowired
    public ParcelsServiceImpl(ParcelsRepository parcelsRepository) {
        this.parcelsRepository = parcelsRepository;
    }


    public List<Parcels> getAll(Users user) {
        VerifyHelper.verifyIEmployee(user);

        return parcelsRepository.getAll();
    }

    @Override
    public Parcels getById(int id, Users user) {

        VerifyHelper.verifyIEmployee(user);

        return parcelsRepository.getById(id);
    }

    @Override
    public void create(Parcels parcels, Users user) {

        VerifyHelper.verifyIEmployee(user);

        parcelsRepository.create(parcels);
    }

    @Override
    public void update(Parcels parcels, Users user) {

        VerifyHelper.verifyIEmployee(user);

        try {
            parcelsRepository.getById(parcels.getId());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Parcel", parcels.getId());
        }

        parcelsRepository.update(parcels);
    }

    @Override
    public void delete(int id, Users user) {

        VerifyHelper.verifyIEmployee(user);

        try {
            parcelsRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Parcel", id);
        }

        parcelsRepository.delete(id);
    }

    @Override
    public List<Parcels> filterBy(Optional<Double> weight,
                                  Optional<String> userFilted,
                                  Optional<String> warehouse,
                                  Optional<String> category,
                                  Optional<String> orderBy,
                                  Users user) {

        VerifyHelper.verifyIsCustomerOrEmployee(user);

        if (orderBy.isPresent() && !orderBy.get().equals("asc") && !orderBy.get().equals("desc")) {
            throw new IllegalArgumentException(Constants.ORDER_TYPE_MESSAGE);
        }

        return parcelsRepository.filterBy(weight, userFilted, warehouse, category, orderBy);
    }

    @Override
    public List<Parcels> sortBy(Optional<String> weight,
                                Optional<String> arrivalDate,
                                Users user) {

        VerifyHelper.verifyIEmployee(user);

        if (weight.isPresent() && !weight.get().equals("asc") && !weight.get().equals("desc")) {
            throw new IllegalArgumentException(Constants.ORDER_TYPE_MESSAGE);
        }

        if (arrivalDate.isPresent() && !arrivalDate.get().equals("asc") && !arrivalDate.get().equals("desc")) {
            throw new IllegalArgumentException(Constants.ORDER_TYPE_MESSAGE);
        }

        return parcelsRepository.sortBy(weight, arrivalDate);
    }
}
