package telerikacademy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Category;
import telerikacademy.models.Users;
import telerikacademy.repositories.contract.CategoryRepository;
import telerikacademy.services.contract.CategoryService;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> getAll() {
        return repository.getAll();
    }

    @Override
    public Category getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public void create(Category category, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        repository.create(category);
    }

    @Override
    public void update(Category category, Users user) {

        VerifyHelper.verifyIEmployee(user);

        boolean duplicateExists = true;

        try {
            Category existingCategory = repository.getByName(category.getName());

            if (existingCategory.getId() == category.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        repository.update(category);
    }

    @Override
    public void delete(int id, Users user) {

        VerifyHelper.verifyIEmployee(user);

        repository.delete(id);
    }
}
