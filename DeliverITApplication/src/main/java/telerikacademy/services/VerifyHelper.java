package telerikacademy.services;

import telerikacademy.Constants;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Users;

public class VerifyHelper {

    public static void verifyIsCustomerOrEmployee(Users users) {
        if (!users.isEmployee() && !users.isCustomer()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_EMPLOYEE_OR_CUSTOMER);
        }
    }

    public static void verifyIEmployee(Users users) {
        if (!users.isEmployee()) {
            throw new UnauthorizedOperationException(Constants.USER_IS_NOT_EMPLOYEE);
        }
    }
}
