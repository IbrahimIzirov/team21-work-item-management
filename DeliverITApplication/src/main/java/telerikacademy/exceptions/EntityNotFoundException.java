package telerikacademy.exceptions;

import telerikacademy.Constants;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format(Constants.ENTITY_NOT_FOUND, type, attribute, value));
    }
}
