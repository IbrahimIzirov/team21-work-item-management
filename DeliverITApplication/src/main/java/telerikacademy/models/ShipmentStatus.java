package telerikacademy.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "shipment_status")
public class ShipmentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_status_id")
    private int id;

    @Column(name = "description")
    private String description;

    public ShipmentStatus() {
    }

    public ShipmentStatus(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        ShipmentStatus status = (ShipmentStatus) object;
        return id == status.id &&
                description.equals(status.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description);
    }
}
