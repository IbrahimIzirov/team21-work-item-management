package telerikacademy.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "countries")
public class Countries {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private int id;

    @Column(name = "country_name")
    private String name;

    public Countries() {
    }

    public Countries(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Countries countries = (Countries) object;
        return id == countries.id &&
                name.equals(countries.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
