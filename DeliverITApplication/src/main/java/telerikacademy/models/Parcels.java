package telerikacademy.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "parcels")
public class Parcels {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parcel_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouses warehouseId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users userId;

    @Column(name = "weight")
    private double weight;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category categoryId;

    public Parcels() {
    }

    public Parcels(int id, Warehouses warehouseId,
                   Users userId, double weight,
                   Category categoryId) {
        this.id = id;
        this.warehouseId = warehouseId;
        this.userId = userId;
        this.weight = weight;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Warehouses getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Warehouses warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Parcels parcels = (Parcels) object;
        return id == parcels.id &&
                Double.compare(parcels.weight, weight) == 0 &&
                warehouseId.equals(parcels.warehouseId) &&
                userId.equals(parcels.userId) &&
                categoryId.equals(parcels.categoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, warehouseId, userId, weight, categoryId);
    }
}
