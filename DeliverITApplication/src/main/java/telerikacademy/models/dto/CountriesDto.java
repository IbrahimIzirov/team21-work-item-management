package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CountriesDto {

    @NotNull(message = Constants.COUNTRY_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.COUNTRY_NAME_MESSAGE)
    private String name;

    public CountriesDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
