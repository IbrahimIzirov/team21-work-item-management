package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDto {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotNull(message = Constants.USER_FIRST_NAME_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.USER_NAME_SYMBOLS_MESSAGE)
    private String firstName;

    @NotNull(message = Constants.USER_LAST_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.USER_NAME_SYMBOLS_MESSAGE)
    private String lastName;

    @NotNull(message = Constants.EMAIL_CAN_NULL_MESSAGE)
    private String email;

    @Positive(message = Constants.ADDRESS_ID_POSITIVE_MESSAGE)
    private int addressId;

    @NotEmpty
    private String streetName;

    @NotEmpty
    private int cityId;

    public UserDto(String firstName, String lastName, String email, int addressId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.addressId = addressId;
    }

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
