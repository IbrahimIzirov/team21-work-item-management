package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CitiesDto {

    @NotNull(message = Constants.CITY_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.CITY_NAME_MESSAGE)
    private String name;

    @Positive(message = Constants.COUNTRY_ID_MESSAGE)
    private int countryId;

    public CitiesDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
