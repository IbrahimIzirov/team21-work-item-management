package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class AddressDto {

    @Positive(message = Constants.CITY_ID_MESSAGE)
    private int cityId;

    @NotNull(message = Constants.STREET_NAME_MESSAGE)
    private String streetName;

    public AddressDto() {
    }

    public AddressDto(int cityId, String streetName) {
        this.cityId = cityId;
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
}
