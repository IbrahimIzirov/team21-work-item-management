package telerikacademy.models.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import telerikacademy.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

public class ShipmentsDto {

    @NotNull(message = Constants.SHIPMENT_DATE_NULL_MESSAGE)
    @Positive(message = Constants.SHIPMENT_DATE_MESSAGE)
    private LocalDate shipmentDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = Constants.DEPARTURE_DATE_NULL_MESSAGE)
    @Positive(message = Constants.DEPARTURE_DATE_POSITIVE_MESSAGE)
    private LocalDate departureDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = Constants.ARRIVAL_DATE_NULL_MESSAGE)
    @Positive(message = Constants.ARRIVAL_DATE_POSITIVE_MESSAGE)
    private LocalDate arrivalDate;

    @NotNull(message = Constants.SHIPMENT_STATUS_NULL_MESSAGE)
    @Positive(message = Constants.SHIPMENT_STATUS_POSITIVE_MESSAGE)
    private int shipmentStatusId;

    @NotNull(message = Constants.WAREHOUSE_ID_NULL_MESSAGE)
    @Positive(message = Constants.WAREHOUSE_ID_POSITIVE_MESSAGE)
    private int warehouseId;

    public ShipmentsDto() {
    }

    public ShipmentsDto(LocalDate shipmentDate, LocalDate departureDate,
                        LocalDate arrivalDate, int shipmentStatus, int warehouse_id) {
        this.shipmentDate = shipmentDate;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.shipmentStatusId = shipmentStatus;
        this.warehouseId = warehouse_id;
    }

    public LocalDate getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(LocalDate shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getShipmentStatusId() {
        return shipmentStatusId;
    }

    public void setShipmentStatusId(int shipmentStatusId) {
        this.shipmentStatusId = shipmentStatusId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

}
