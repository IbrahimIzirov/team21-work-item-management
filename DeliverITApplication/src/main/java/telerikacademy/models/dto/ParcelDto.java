package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.Positive;

public class ParcelDto {

    @Positive(message = Constants.PARCEL_ID_MESSAGE)
    private int warehouseId;

    @Positive(message = Constants.USER_ID_MESSAGE)
    private int userId;

    @Positive(message = Constants.WEIGHT_MESSAGE)
    private double weight;

    @Positive(message = Constants.CATEGORY_ID_MESSAGE)
    private int categoryId;

    public ParcelDto() {
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}

