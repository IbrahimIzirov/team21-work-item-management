package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDto {

    @NotNull(message = Constants.CATEGORY_NULL_MESSAGE)
    @Size(min = 2, max = 20, message = Constants.CATEGORY_NAME_MESSAGE)
    private String name;

    @NotNull(message = Constants.DESCRIPTION_MESSAGE)
    private String description;

    public CategoryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
