package telerikacademy.models.dto;

import telerikacademy.Constants;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class WarehousesDto {

    @NotNull(message = Constants.ADDRESS_ID_NULL_MESSAGE)
    @Positive(message = Constants.ADDRESS_ID_POSITIVE_MESSAGE)
    private int cityId;

    @NotEmpty
    private String streetName;

    public WarehousesDto() {
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
}
