package telerikacademy.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cities")
public class Cities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private int id;

    @Column(name = "city_name")
    private String name;

    @ManyToOne()
    @JoinColumn(name = "country_id")
    private Countries countryId;

    public Cities() {
    }

    public Cities(int id, String name, Countries countryId) {
        this.id = id;
        this.name = name;
        this.countryId = countryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Countries getCountryId() {
        return countryId;
    }

    public void setCountryId(Countries countryId) {
        this.countryId = countryId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Cities cities = (Cities) object;
        return id == cities.id &&
                name.equals(cities.name) &&
                countryId.equals(cities.countryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, countryId);
    }
}
