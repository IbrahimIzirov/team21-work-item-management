package telerikacademy.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import net.bytebuddy.asm.Advice;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "shipments")
public class Shipments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private int id;

    @Column(name = "shipment_date")
    private LocalDate shipmentDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "departure_date")
    private LocalDate departureDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "arrival_date")
    private LocalDate arrivalDate;

    @ManyToOne
    @JoinColumn(name = "shipment_status")
    private ShipmentStatus shipmentStatus;

    @ManyToOne
    @JoinColumn(name = "warehouse_id")
    private Warehouses warehouses;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "shipments_details",
            joinColumns = @JoinColumn(name = "shipment_id"),
            inverseJoinColumns = @JoinColumn(name = "parcel_id")
    )
    private Set<Parcels> parcelsSet;


    public Shipments() {
    }

    public Shipments(int id, LocalDate shipmentDate, LocalDate departureDate,
                     LocalDate arrivalDate, ShipmentStatus shipmentStatus, Warehouses warehouses) {
        this.id = id;
        this.shipmentDate = shipmentDate;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.shipmentStatus = shipmentStatus;
        this.warehouses = warehouses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(LocalDate shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public ShipmentStatus getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(ShipmentStatus shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public Set<Parcels> getParcelsSet() {
        return parcelsSet;
    }

    public void setParcelsSet(Set<Parcels> parcelsSet) {
        this.parcelsSet = parcelsSet;
    }

    public Warehouses getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(Warehouses warehouses) {
        this.warehouses = warehouses;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Shipments shipments = (Shipments) object;
        return id == shipments.id &&
                shipmentDate.equals(shipments.shipmentDate) &&
                departureDate.equals(shipments.departureDate) &&
                arrivalDate.equals(shipments.arrivalDate) &&
                shipmentStatus.equals(shipments.shipmentStatus) &&
                warehouses.equals(shipments.warehouses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shipmentDate, departureDate, arrivalDate, shipmentStatus, warehouses);
    }
}
