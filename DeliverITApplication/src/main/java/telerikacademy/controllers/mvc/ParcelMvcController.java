package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.*;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.*;
import telerikacademy.services.mappers.ParcelModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final ParcelsService parcelsService;
    private final CategoryService categoryService;
    private final WarehouseService warehouseService;
    private final ShipmentsService shipmentsService;
    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;
    private final ParcelModelMapper parcelModelMapper;


    @Autowired
    public ParcelMvcController(ParcelsService parcelsService,
                               CategoryService categoryService,
                               WarehouseService warehouseService,
                               ShipmentsService shipmentsService, UsersService usersService,
                               AuthenticationHelper authenticationHelper,
                               ParcelModelMapper parcelModelMapper) {
        this.parcelsService = parcelsService;
        this.categoryService = categoryService;
        this.warehouseService = warehouseService;
        this.shipmentsService = shipmentsService;
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
        this.parcelModelMapper = parcelModelMapper;
    }

    @GetMapping
    public String showAllParcels(Model model, HttpSession session) {

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            if (!currentUser.isEmployee()) {
                model.addAttribute("customerParcels", parcelsService.filterBy(Optional.empty(),
                        Optional.of(currentUser.getUsername()),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.of("asc"),
                        currentUser));
                return "customerOnlyParcels";
            }

            model.addAttribute("parcelsAll", parcelsService.getAll(currentUser));

            return "parcels";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{id}")
    public String showSingleParcel(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            Parcels parcels = parcelsService.getById(id, currentUser);
            model.addAttribute("parcel", parcels);

            return "parcel";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{parcelId}/shipments/{shipmentId}")
    public String addParcelToShipment(@PathVariable int parcelId, @PathVariable int shipmentId, HttpSession session) {

        Users currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            Shipments shipment = shipmentsService.getById(shipmentId, currentUser);

            Parcels parcel = parcelsService.getById(parcelId, currentUser);

            shipmentsService.addToShipments(shipment, parcel);

            return "redirect:/shipments/{shipmentId}";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            model.addAttribute("parcel", new ParcelDto());

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
        return Constants.CREATE_PARCEL_PAGE;
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {

        if (errors.hasErrors()) {
            return Constants.CREATE_PARCEL_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            Parcels newParcel = parcelModelMapper.fromDto(parcelDto);
            parcelsService.create(newParcel, currentUser);
            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "parcel-exist", e.getMessage());
            return Constants.CREATE_PARCEL_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/update")
    public String showEditParcelPage(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {

            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Parcels parcel = parcelsService.getById(id, currentUser);
            ParcelDto parcelDto = parcelModelMapper.toDto(parcel);
            model.addAttribute("parcelId", id);
            model.addAttribute("parcel", parcelDto);

            return Constants.UPDATE_PARCEL_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }


    @PostMapping("/{id}/update")
    public String updateParcel(@PathVariable int id,
                               @Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               Model model, HttpSession session) {

        if (errors.hasErrors()) {
            return Constants.UPDATE_PARCEL_PAGE;
        }
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Parcels newParcel = parcelModelMapper.fromDto(parcelDto, id);

            parcelsService.update(newParcel, currentUser);
            return "redirect:/parcels";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_parcel", e.getMessage());
            return Constants.UPDATE_PARCEL_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            parcelsService.delete(id, currentUser);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @ModelAttribute("categories")
    public List<Category> getAllCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("warehouses")
    public List<Warehouses> getAllWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("shipments")
    public List<Shipments> getAllShipments() {
        return shipmentsService.getAll(getUser());
    }

    @ModelAttribute("users")
    public List<Users> getAllUsers() {
        Users user = new Users(1,
                "ivan",
                "lolev",
                "asdada@asd.bg",
                new Address(1, new Cities(1, "sdadsa", new Countries(1, "JSAdkasdk")), "asdadasd"));
        user.setRoles(Set.of(new Roles(2, "Employee")));
        return usersService.getAll(user);
    }

    private Users getUser() {
        Users user = new Users(1, "ivan", "lolev", "asdada@asd.bg",
                new Address(1,
                        new Cities(1, "Minsk",
                                new Countries(1, "Russia")), "asdadasd"));

        user.setRoles(Set.of(new Roles(2, "Employee")));
        return user;
    }
}
