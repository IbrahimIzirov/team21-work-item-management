package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.*;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.models.dto.ShipmentsDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.ParcelsService;
import telerikacademy.services.contract.ShipmentsService;
import telerikacademy.services.contract.UsersService;
import telerikacademy.services.contract.WarehouseService;
import telerikacademy.services.mappers.ShipmentModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentsService shipmentsService;
    private final WarehouseService warehouseService;
    private final ShipmentModelMapper modelMapper;
    private final UsersService usersService;
    private final ParcelsService parcelsService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentMvcController(ShipmentsService shipmentsService,
                                 WarehouseService warehouseService,
                                 ShipmentModelMapper modelMapper,
                                 UsersService usersService,
                                 ParcelsService parcelsService, AuthenticationHelper authenticationHelper) {
        this.shipmentsService = shipmentsService;
        this.warehouseService = warehouseService;
        this.modelMapper = modelMapper;
        this.usersService = usersService;
        this.parcelsService = parcelsService;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public String showAllShipment(Model model, HttpSession session) {

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            if (!currentUser.isEmployee()) {
                model.addAttribute("customerShipments",
                        shipmentsService.filterByCustomer(currentUser.getId(), currentUser));

                return "customerOnlyShipments";
            }

            model.addAttribute("shipments", shipmentsService.getAll(currentUser));
            model.addAttribute("warehouses", warehouseService.getAll());

            return Constants.SHOW_ALL_SHIPMENTS_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{id}")
    public String showShipment(@PathVariable int id, Model model, HttpSession session) {

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            Shipments shipment = shipmentsService.getById(id, currentUser);
            List<Parcels> parcels = new ArrayList<>(shipmentsService.getById(id, currentUser).getParcelsSet());
            model.addAttribute("parcels", parcels);
            model.addAttribute("shipment", shipment);

            return "shipment";

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{id}/update")
    public String showEditShipmentPage(@PathVariable int id, Model model, HttpSession session) {

        Users currentUser;

        try {

            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Shipments shipment = shipmentsService.getById(id, currentUser);
            ShipmentsDto shipmentsDto = modelMapper.toDto(shipment);
            model.addAttribute("shipmentId", id);
            model.addAttribute("shipmentDto", shipmentsDto);

            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String updateShipment(@PathVariable int id,
                                 @Valid @ModelAttribute ShipmentsDto shipmentsDto,
                                 BindingResult error,
                                 Model model, HttpSession session) {

        if (error.hasErrors()) {
            return "shipment-update";
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Shipments newShipment = modelMapper.fromDto(shipmentsDto, id);
            shipmentsService.update(newShipment, currentUser);

            return "redirect:/shipments";
        } catch (DuplicateEntityException e) {
            error.rejectValue("name", "shipment.exist", e.getMessage());
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }


    @GetMapping("/new")
    public String showShipmentCreatePage(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            model.addAttribute("shipmentDto", new ShipmentsDto());

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
        return Constants.CREATE_SHIPMENT_PAGE;
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipmentDto") ShipmentsDto shipmentsDto,
                                 BindingResult errors,
                                 HttpSession session) {

        if (errors.hasErrors()) {
            return Constants.CREATE_SHIPMENT_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            Shipments newShipment = modelMapper.fromDto(shipmentsDto);
            shipmentsService.create(newShipment, currentUser);
            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "shipment-exist", e.getMessage());
            return Constants.CREATE_SHIPMENT_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }


    @GetMapping("/{id}/delete")
    public String deleteShipment(@PathVariable int id, Model model, HttpSession session) {


        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            shipmentsService.delete(id, currentUser);
            return "redirect:/shipments";

        } catch (EntityNotFoundException | EmptyListException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{shipmentId}/deleteParcel/{parcelId}")
    public String DeleteParcelFromShipment(@PathVariable int shipmentId,
                                           @PathVariable int parcelId,
                                           Model model,
                                           HttpSession session) {

        Shipments currentShipment;
        Parcels currentParcel;
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            VerifyHelper.verifyIEmployee(currentUser);

            currentShipment = shipmentsService.getById(shipmentId, currentUser);
            currentParcel = parcelsService.getById(parcelId, currentUser);

            model.addAttribute("currentParcel", currentParcel);
            model.addAttribute("currentShipment", currentShipment);

            shipmentsService.deleteToShipments(currentShipment, currentParcel);

            shipmentsService.getById(currentShipment.getId(), currentUser);

            return "redirect:/shipments/{shipmentId}";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return "redirect:/shipments";
        }
    }

    private Users getUser() {
        Users user = new Users(1, "ivan", "lolev", "asdada@asd.bg",
                new Address(1,
                        new Cities(1, "Minsk",
                                new Countries(1, "Russia")), "asdadasd"));
        user.setRoles(Set.of(new Roles(2, "Employee")));
        return user;
    }


    @ModelAttribute("status")
    public List<ShipmentStatus> populateStatus() {
        return shipmentsService.getAllStatuses(getUser());
    }

    @ModelAttribute("warehouses")
    public List<Warehouses> populateWarehouse() {
        return warehouseService.getAll();
    }

    @ModelAttribute("users")
    public List<Users> populateUsers() {
        return usersService.getAll(getUser());
    }

    @ModelAttribute("parcels")
    public List<Parcels> populateParcels() {
        return parcelsService.getAll(getUser());
    }
}
