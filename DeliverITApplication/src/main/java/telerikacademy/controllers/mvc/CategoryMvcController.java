package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.*;
import telerikacademy.models.dto.CategoryDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.CategoryService;
import telerikacademy.services.contract.UsersService;
import telerikacademy.services.mappers.CategoryModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/categories")
public class CategoryMvcController {

    private final CategoryService categoryService;
    private final CategoryModelMapper modelMapper;
    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryMvcController(CategoryService categoryService,
                                 CategoryModelMapper modelMapper,
                                 UsersService usersService,
                                 AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.modelMapper = modelMapper;
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllCategories(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("categoriesAll", categoryService.getAll());

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
            model.addAttribute("categoriesAll", categoryService.getAll());

        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }

        return Constants.SHOW_ALL_CATEGORIES_PAGE;
    }

    @GetMapping("/{id}")
    public String showSingleCategory(@PathVariable int id, Model model) {
        try {
            Category category = categoryService.getById(id);
            model.addAttribute("category", category);
            return "category";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/new")
    public String showNewCategoryPage(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("category", new CategoryDto());
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
        return Constants.CREATE_CATEGORY_PAGE;
    }

    @PostMapping("/new")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDto category,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.CREATE_CATEGORY_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            Category newCategory = modelMapper.fromDto(category);
            categoryService.create(newCategory, currentUser);
            return "redirect:/categories";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "category-exist", e.getMessage());
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
        return Constants.CREATE_CATEGORY_PAGE;
    }

    @GetMapping("/{id}/update")
    public String showEditCategoryPage(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {

            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Category category = categoryService.getById(id);
            CategoryDto categoryDto = modelMapper.toDto(category);
            model.addAttribute("categoryId", id);
            model.addAttribute("category", categoryDto);

            return Constants.UPDATE_CATEGORY_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String updateCategory(@PathVariable int id,
                                 @Valid @ModelAttribute("category") CategoryDto category,
                                 BindingResult errors,
                                 Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.UPDATE_CATEGORY_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Category newCategory = modelMapper.fromDto(category, id);
            categoryService.update(newCategory, currentUser);
            return "redirect:/categories";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_category", e.getMessage());
            return Constants.UPDATE_CATEGORY_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            categoryService.delete(id, currentUser);

            return "redirect:/categories";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }
}
