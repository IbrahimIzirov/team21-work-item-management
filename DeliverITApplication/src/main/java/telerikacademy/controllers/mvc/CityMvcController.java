package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Category;
import telerikacademy.models.Cities;
import telerikacademy.models.Countries;
import telerikacademy.models.Users;
import telerikacademy.models.dto.CategoryDto;
import telerikacademy.models.dto.CitiesDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.CityService;
import telerikacademy.services.contract.CountriesService;
import telerikacademy.services.mappers.CityModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cities")
public class CityMvcController {

    private final CityService cityService;
    private final AuthenticationHelper authenticationHelper;
    private final CityModelMapper modelMapper;
    private final CountriesService countriesService;

    @Autowired
    public CityMvcController(CityService cityService,
                             AuthenticationHelper authenticationHelper,
                             CityModelMapper modelMapper,
                             CountriesService countriesService) {
        this.cityService = cityService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.countriesService = countriesService;

    }

    @GetMapping
    public String showAllCities(Model model, HttpSession session) {
        model.addAttribute("citiesAll", cityService.getAll());
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e){
            model.addAttribute("currentUser", null);
        }
        return "cities";
    }

    @GetMapping("/{id}")
    public String showSingleCity(@PathVariable int id, Model model, HttpSession session) {

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            Cities city = cityService.getById(id);
            model.addAttribute("city", city);
            model.addAttribute("currentUser", currentUser);
            return "city";
        } catch (EntityNotFoundException e) {
            return "notFoundPage";
        } catch (UnauthorizedOperationException e) {
            return "not-authorized";
        }
    }

    @GetMapping("/new")
    public String createCity(Model model, HttpSession session) {

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("cityDto", new CitiesDto());
            model.addAttribute("currentUser", currentUser);
            return "city-new";
        } catch (UnauthorizedOperationException e) {
            return "not-authorized";
        }

    }

    @PostMapping("/new")
    public String createCity(@Valid @ModelAttribute("cityDto") CitiesDto cityDto, HttpSession session, BindingResult errors) {

        if (errors.hasErrors()) {
            return Constants.CREATE_CITY_PAGE;
        }
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);

            Cities newCity = modelMapper.fromDto(cityDto);
            cityService.create(newCity, currentUser);

            return "redirect:/cities";
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "city-exist", e.getMessage());
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
        return "city-new";
    }


    @GetMapping("/{id}/update")
    public String showUpdateCity(@PathVariable int id, Model model, HttpSession session) {

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);
            Cities city = cityService.getById(id);
            CitiesDto citiesDto = modelMapper.toDto(city);
            model.addAttribute("cityId", id);
            model.addAttribute("cityDto", citiesDto);

            return Constants.UPDATE_CITY_PAGE;
        } catch (UnauthorizedOperationException e) {
            return "not-authorized";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }

    }

    @PostMapping("/{id}/update")
    public String showUpdateCity(@PathVariable int id,
                                 @Valid @ModelAttribute("cityDto") CitiesDto citiesDto,
                                 BindingResult errors,
                                 Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.UPDATE_CITY_PAGE;
        }

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Cities newCity = modelMapper.fromDto(citiesDto, id);
            cityService.update(newCity, currentUser);
            return "redirect:/cities";

        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_city", e.getMessage());
            return Constants.UPDATE_CITY_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);
            cityService.delete(id, currentUser);

            return "redirect:/cities";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EmptyListException e){
            model.addAttribute("emptyList", e.getMessage());
            return Constants.ERROR_PAGE;
        }

    }


    @ModelAttribute("countries")
    public List<Countries> populateCountries() {
        return countriesService.getAll();
    }


}
