package telerikacademy.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.*;
import telerikacademy.models.dto.AddressDto;
import telerikacademy.models.dto.WarehousesDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.AddressesService;
import telerikacademy.services.contract.CityService;
import telerikacademy.services.contract.CountriesService;
import telerikacademy.services.contract.WarehouseService;
import telerikacademy.services.mappers.WarehouseModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {

    private final WarehouseService warehouseService;
    private final WarehouseModelMapper warehouseModelMapper;
    private final CityService cityService;
    private final CountriesService countriesService;
    private final AddressesService addressesService;
    private final AuthenticationHelper authenticationHelper;

    public WarehouseMvcController(WarehouseService warehouseService,
                                  WarehouseModelMapper warehouseModelMapper,
                                  CityService cityService,
                                  CountriesService countriesService,
                                  AddressesService addressesService, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseModelMapper = warehouseModelMapper;
        this.cityService = cityService;
        this.countriesService = countriesService;
        this.addressesService = addressesService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllWarehouses(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("warehousesAll", warehouseService.getAll());

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
            model.addAttribute("warehousesAll", warehouseService.getAll());

        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }

        return Constants.SHOW_ALL_WAREHOUSES_PAGE;
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model) {
        try {
            Warehouses warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/new")
    public String showNewWarehousePage(Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("warehouse", new WarehousesDto());

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
        return Constants.CREATE_WAREHOUSE_PAGE;
    }

    @PostMapping("/new")
    public String createWarehouse(@Valid @ModelAttribute("warehouse") WarehousesDto warehousesDto,
                                  BindingResult errors,
                                  HttpSession session,
                                  Model model) {

        if (errors.hasErrors()) {
            return Constants.CREATE_WAREHOUSE_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Warehouses newWarehouse = warehouseModelMapper.fromDto(warehousesDto);
            warehouseService.create(newWarehouse, currentUser);
            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "warehouse-exist", e.getMessage());
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
        return Constants.CREATE_WAREHOUSE_PAGE;
    }

    @GetMapping("/{id}/update")
    public String showEditWarehousePage(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {

            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Warehouses warehouse = warehouseService.getById(id);
            WarehousesDto warehousesDto = warehouseModelMapper.toDto(warehouse);
            model.addAttribute("warehouseId", id);
            model.addAttribute("warehouse", warehousesDto);
            return Constants.UPDATE_WAREHOUSE_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String updateWarehouse(@PathVariable int id,
                                  @Valid @ModelAttribute("warehouse") WarehousesDto warehousesDto,
                                  BindingResult errors,
                                  Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.UPDATE_WAREHOUSE_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            Warehouses newWarehouse = warehouseModelMapper.fromDto(warehousesDto, id);
            warehouseService.update(newWarehouse, currentUser);

            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_warehouse", e.getMessage());
            return Constants.UPDATE_WAREHOUSE_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteWarehouse(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            warehouseService.delete(id, currentUser);

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EmptyListException e) {
            model.addAttribute("emptyList", e.getMessage());
            return Constants.ERROR_PAGE;
        }
    }


    @ModelAttribute("countries")
    public List<Countries> getAllCountries() {
        return countriesService.getAll();
    }

    @ModelAttribute("cities")
    public List<Cities> getAllCities() {
        return cityService.getAll();
    }
}
