package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Cities;
import telerikacademy.models.Countries;
import telerikacademy.models.Parcels;
import telerikacademy.models.Users;
import telerikacademy.models.dto.CountriesDto;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.repositories.contract.CountriesRepository;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.CountriesService;
import telerikacademy.services.mappers.CountriesModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/countries")
public class CountriesMvcController {

    private final CountriesService countriesService;
    private final AuthenticationHelper authenticationHelper;
    private final CountriesModelMapper countriesModelMapper;

    @Autowired
    public CountriesMvcController(CountriesService countriesService,
                                  AuthenticationHelper authenticationHelper,
                                  CountriesModelMapper countriesModelMapper) {
        this.countriesService = countriesService;
        this.authenticationHelper = authenticationHelper;
        this.countriesModelMapper = countriesModelMapper;
    }


    @GetMapping
    public String showAllCountriesс(Model model, HttpSession session) {
            model.addAttribute("countries", countriesService.getAll());
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        }catch (UnauthorizedOperationException e){
            model.addAttribute("currentUser", null);
        }
        return "countries";
    }


    @GetMapping("/new")
    public String showNewCountryPage(Model model, HttpSession session) {
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);
            model.addAttribute("countryDto", new CountriesDto());

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }

        return Constants.CREATE_COUNTRY_PAGE;
    }

    @PostMapping("/new")
    public String createCountry(@Valid @ModelAttribute("countryDto") CountriesDto countriesDto,
                                BindingResult errors,
                                HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.CREATE_COUNTRY_PAGE;
        }
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);
            Countries newCountry = countriesModelMapper.fromDto(countriesDto);
            countriesService.create(newCountry, currentUser);
            return "redirect:/countries";

        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "country-exist", e.getMessage());
            return Constants.CREATE_COUNTRY_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }


    @GetMapping("/{id}/update")
    public String showEditCountryPage(@PathVariable int id, Model model, HttpSession session) {

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            VerifyHelper.verifyIEmployee(currentUser);

            Countries countries = countriesService.getById(id);
            CountriesDto countryDto = countriesModelMapper.toDto(countries);
            model.addAttribute("countryId", id);
            model.addAttribute("countryDto", countryDto);

            return Constants.UPDATE_COUNTRY_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String updateCountry(@PathVariable int id,
                                @Valid @ModelAttribute("countryDto") CountriesDto countriesDto,
                                BindingResult errors,
                                Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return Constants.UPDATE_COUNTRY_PAGE;
        }

        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            VerifyHelper.verifyIEmployee(currentUser);

            Countries newCountry = countriesModelMapper.fromDto(countriesDto, id);
            countriesService.update(newCountry, currentUser);
            return "redirect:/countries";

        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_country", e.getMessage());
            return Constants.UPDATE_COUNTRY_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCountry(@PathVariable int id, Model model, HttpSession session) {

        try {
            Users currentUser  = authenticationHelper.tryGetUser(session);
            VerifyHelper.verifyIEmployee(currentUser);
            countriesService.delete(id, currentUser);

            return "redirect:/countries";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EmptyListException e){
            model.addAttribute("emptyList", e.getMessage());
            return Constants.ERROR_PAGE;
        }
    }


}
