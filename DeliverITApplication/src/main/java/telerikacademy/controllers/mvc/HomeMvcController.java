package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Users;
import telerikacademy.services.contract.UsersService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UsersService usersService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, UsersService usersService) {
        this.authenticationHelper = authenticationHelper;
        this.usersService = usersService;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        return "index";
    }

    @GetMapping("/about")
    public String showAboutUsPage(Model model, HttpSession session) {
        try {
            Users currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }
        return "about-us";
    }


    @ModelAttribute("countUser")
    public long getUserCount() {
        return usersService.getCustomerCount();
    }


}
