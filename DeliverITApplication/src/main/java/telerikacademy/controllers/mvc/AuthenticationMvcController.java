package telerikacademy.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.AuthenticationFailureException;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.models.Cities;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.models.dto.LoginDto;
import telerikacademy.models.dto.RegisterDto;
import telerikacademy.services.contract.CityService;
import telerikacademy.services.contract.UsersService;
import telerikacademy.services.mappers.RegisterMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping
public class AuthenticationMvcController {

    private final UsersService usersService;
    private final CityService cityService;
    private final AuthenticationHelper authenticationHelper;
    private final RegisterMapper registerMapper;

    public AuthenticationMvcController(UsersService usersService,
                                       CityService cityService,
                                       AuthenticationHelper authenticationHelper,
                                       RegisterMapper registerMapper) {
        this.usersService = usersService;
        this.cityService = cityService;
        this.authenticationHelper = authenticationHelper;
        this.registerMapper = registerMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("loginDto", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("loginDto") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUsername", loginDto.getUsername());
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUsername");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("registerDto") RegisterDto registerDto, BindingResult bindingResult) {

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password_error", "Password don't match");
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            Users userToRegister = registerMapper.fromDto(registerDto, new Users());
            usersService.create(userToRegister);
            usersService.addRole(userToRegister, new Roles(1, "Customer"));
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
        }
        return "redirect:/login";
    }

    @ModelAttribute("cities")
    public List<Cities> getAllCities() {
        return cityService.getAll();
    }


}
