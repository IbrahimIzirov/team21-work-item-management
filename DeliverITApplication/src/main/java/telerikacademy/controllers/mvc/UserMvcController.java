package telerikacademy.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Cities;
import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.models.dto.RegisterDto;
import telerikacademy.services.VerifyHelper;
import telerikacademy.services.contract.CityService;
import telerikacademy.services.contract.RolesService;
import telerikacademy.services.contract.UsersService;
import telerikacademy.services.mappers.RegisterMapper;
import telerikacademy.services.mappers.UserModelMapper;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final CityService cityService;
    private final RegisterMapper registerMapper;
    private final RolesService rolesService;

    @Autowired
    public UserMvcController(UsersService usersService,
                             AuthenticationHelper authenticationHelper,
                             UserModelMapper userModelMapper,
                             CityService cityService,
                             RegisterMapper registerMapper, RolesService rolesService) {
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.cityService = cityService;
        this.registerMapper = registerMapper;
        this.rolesService = rolesService;
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            VerifyHelper.verifyIEmployee(currentUser);

            model.addAttribute("usersAll", usersService.getAll(currentUser));

            return Constants.SHOW_ALL_USERS_PAGE;

        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            Users user = usersService.getById(id, currentUser);
            model.addAttribute("userView", user);

            if (currentUser.isEmployee()) {
                return Constants.SHOW_CUSTOMER_PROFILE_PAGE;
            }

            return Constants.SHOW_EMPLOYEE_PROFILE_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EntityNotFoundException e) {
            return Constants.NOT_FOUND_PAGE;
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdatePage(@PathVariable int id, Model model, HttpSession session) {

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("updateDto", new RegisterDto());

            Users user = usersService.getById(id, currentUser);
            return Constants.UPDATE_USER_PAGE;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }
    }

    @PostMapping("/{id}/update")
    public String handleUpdate(@PathVariable int id, Model model,
                                 @Valid @ModelAttribute("updateDto") RegisterDto registerDto,
                                 BindingResult bindingResult, HttpSession session) {

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            bindingResult.rejectValue("password", "password_error", "Password don't match");
        }

        if (bindingResult.hasErrors()) {
            return Constants.UPDATE_USER_PAGE;
        }

        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);

            Users inputUser = usersService.getById(id, currentUser);
            Users userToUpdate = registerMapper.updateMethod(registerDto, new Users());
            userToUpdate.setUsername(inputUser.getUsername());
            userToUpdate.setId(inputUser.getId());
            Users outputUser = userModelMapper.inversion(inputUser, userToUpdate);


            usersService.update(currentUser, outputUser);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        }

        return "redirect:/users/{id}";
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        Users currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);

            VerifyHelper.verifyIEmployee(currentUser);

            usersService.delete(id, currentUser);

            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return Constants.NOT_FOUND_PAGE;
        } catch (UnauthorizedOperationException e) {
            return Constants.NOT_AUTHORIZED_PAGE;
        } catch (EmptyListException e){
            model.addAttribute("emptyList", e.getMessage());
            return Constants.ERROR_PAGE;
        }
    }

    @ModelAttribute("cities")
    public List<Cities> getAllCities() {
        return cityService.getAll();
    }

    @ModelAttribute("roles")
    public List<Roles> getAllRoles() {
        return rolesService.getAll();
    }
}
