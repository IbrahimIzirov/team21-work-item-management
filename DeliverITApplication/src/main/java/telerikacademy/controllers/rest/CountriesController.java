package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Countries;
import telerikacademy.models.Users;
import telerikacademy.models.dto.CountriesDto;
import telerikacademy.services.contract.CountriesService;
import telerikacademy.services.mappers.CountriesModelMapper;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountriesController {

    private final CountriesService service;
    private final CountriesModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CountriesController(CountriesService service, CountriesModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Countries> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Countries getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Countries getByName(@RequestParam String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Countries create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CountriesDto countryDto) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            Countries country = modelMapper.fromDto(countryDto);
            service.create(country, user);
            return country;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Countries update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CountriesDto countryDto) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            Countries country = modelMapper.fromDto(countryDto, id);
            service.update(country, user);
            return country;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }
}
