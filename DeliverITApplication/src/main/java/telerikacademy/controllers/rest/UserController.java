package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Users;
import telerikacademy.models.dto.UserDto;
import telerikacademy.services.contract.UsersService;
import telerikacademy.services.mappers.UserModelMapper;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/users")
public class UserController {

    private final UsersService service;
    private final UserModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UsersService service, UserModelMapper mapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping("/count")
    public String getCustomerCount() {

        return String.format(Constants.CUSTOMERS_COUNT, service.getCustomerCount());
    }

    @GetMapping
    public List<Users> getAll(@RequestHeader HttpHeaders headers) {

        try {
            Users users = authenticationHelper.tryGetUser(headers);
            return service.getAll(users);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public Users getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return service.getById(id, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping
    public Users create(@RequestHeader HttpHeaders headers, @Valid @RequestBody UserDto userDto) {
        try {

            Users userToCreate = mapper.fromDto(userDto);
            service.create(userToCreate);
            return userToCreate;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Users update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody UserDto userDto) {

        Users user = authenticationHelper.tryGetUser(headers);


        try {
            Users userToUpdate = mapper.fromDto(userDto, id);

            service.update(user, userToUpdate);

            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException d) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, d.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (EmptyListException iar) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, iar.getMessage());
        }
    }

    @GetMapping("/{id}/in-coming-parcels")
    public List<Parcels> incomingParcels(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            return service.incomingParcels(id, user);
        } catch (UnauthorizedOperationException | EmptyListException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/search")
    public List<Users> searchByMultiple(@RequestParam(required = false) Optional<String> firstName,
                                        @RequestParam(required = false) Optional<String> lastName,
                                        @RequestParam(required = false) Optional<String> email,
                                        @RequestParam(required = false) Optional<String> address,
                                        @RequestHeader HttpHeaders headers) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return service.searchByMultiple(firstName, lastName, email, address, user);

        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/word")
    public List<Users> searchByOneWord(@RequestHeader HttpHeaders headers,
                                       @RequestParam(required = false) Optional<String> search) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return service.searchByOneWord(search, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/parcels")
    public List<Parcels> getCustomerParcels(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return service.getCustomerParcels(id, user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
