package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.Constants;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EmptyListException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.models.dto.ShipmentsDto;
import telerikacademy.services.contract.ParcelsService;
import telerikacademy.services.contract.ShipmentsService;
import telerikacademy.services.mappers.ShipmentModelMapper;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/shipments")
public class ShipmentController {

    private final ShipmentsService service;
    private final ParcelsService parcelsService;
    private final ShipmentModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ShipmentController(ShipmentsService service, ParcelsService parcelsService,
                              ShipmentModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.parcelsService = parcelsService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Shipments> getAll(@RequestHeader HttpHeaders headers) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return service.getAll(user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Shipments getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return service.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/status")
    public List<Shipments> getByStatus(@RequestHeader HttpHeaders headers, @RequestParam Optional<String> status) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            return service.getByStatus(status, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping
    public Shipments create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ShipmentsDto shipmentsDto) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            Shipments shipment = modelMapper.fromDto(shipmentsDto);
            service.create(shipment, user);
            return shipment;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Shipments update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ShipmentsDto shipmentsDto) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            Shipments shipments = modelMapper.fromDto(shipmentsDto, id);
            service.update(shipments, user);
            return shipments;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("{id}/parcels")
    public List<Parcels> ParcelsList(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return new ArrayList<>(service.getById(id, user).getParcelsSet());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/filterByWarehouse")
    public List<Shipments> filterByWarehouse(@RequestParam(required = false) Optional<String> streetName,
                                             @RequestHeader HttpHeaders headers) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return service.filterByWarehouse(streetName, user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/customers/{customerId}")
    public List<Shipments> filterByCustomer(@PathVariable int customerId,
                                            @RequestHeader HttpHeaders headers) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return service.filterByCustomer(customerId, user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping("/{shipmentId}/addParcel")
    public Parcels addToShipments(@RequestHeader HttpHeaders headers,
                                  @PathVariable int shipmentId,
                                  @RequestBody Map<String, Object> body) {

        var shipment = getById(headers, shipmentId);
        var parcelId = (int) body.get("parcelId");
        Parcels parcel;

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            parcel = parcelsService.getById(parcelId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return service.addToShipments(shipment, parcel);

    }

    @PutMapping("/{shipmentId}/deleteParcel")
    public Parcels deleteToShipments(@RequestHeader HttpHeaders headers,
                                     @PathVariable int shipmentId,
                                     @RequestBody Map<String, Object> body) {

        var shipment = getById(headers, shipmentId);
        var parcelId = (int) body.get("parcelId");

        Parcels parcel;

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            parcel = parcelsService.getById(parcelId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        try {
            if (!shipment.getParcelsSet().contains(parcel)) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format(Constants.PARCEL_ALREADY_IS_DELETED, parcel.getId()));
            }
        } catch (EmptyListException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return service.deleteToShipments(shipment, parcel);
    }
}

