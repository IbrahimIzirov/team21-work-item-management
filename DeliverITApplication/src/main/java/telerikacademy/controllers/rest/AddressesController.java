package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Address;
import telerikacademy.services.contract.AddressesService;
import telerikacademy.services.mappers.AddressModelMapper;

import java.util.List;

@RestController
@RequestMapping("/api/address")
public class AddressesController {

    private final AddressesService service;
    private final AddressModelMapper modelMapper;

    @Autowired
    public AddressesController(AddressesService service, AddressModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Address> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Address getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}