package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Parcels;
import telerikacademy.models.Users;
import telerikacademy.models.dto.ParcelDto;
import telerikacademy.services.contract.ParcelsService;
import telerikacademy.services.mappers.ParcelModelMapper;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/parcels")
public class ParcelsController {

    private final ParcelsService parcelsService;
    private final ParcelModelMapper parcelModelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelsController(ParcelsService parcelsService, ParcelModelMapper parcelModelMapper, AuthenticationHelper authenticationHelper) {
        this.parcelsService = parcelsService;
        this.parcelModelMapper = parcelModelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Parcels> getAll(@RequestHeader HttpHeaders headers) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            return parcelsService.getAll(user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Parcels getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {

            Users user = authenticationHelper.tryGetUser(headers);

            return parcelsService.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PostMapping
    public Parcels create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ParcelDto parcelDto) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            Parcels parcel = parcelModelMapper.fromDto(parcelDto);
            parcelsService.create(parcel, user);
            return parcel;
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers, @Validated @PathVariable int id,
                       @RequestBody ParcelDto parcelDto) {

        try {

            Users user = authenticationHelper.tryGetUser(headers);

            Parcels parcel = parcelModelMapper.fromDto(parcelDto, id);
            parcel.setId(id);
            parcelsService.update(parcel, user);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            parcelsService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Parcels> filterBy(@RequestParam(required = false) Optional<Double> weight,
                                  @RequestParam(required = false) Optional<String> customer,
                                  @RequestParam(required = false) Optional<String> warehouse,
                                  @RequestParam(required = false) Optional<String> category,
                                  @RequestParam(required = false) Optional<String> orderBy,
                                  @RequestHeader HttpHeaders headers) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return parcelsService.filterBy(weight,
                    customer,
                    warehouse,
                    category,
                    orderBy,
                    user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (IllegalArgumentException iae) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, iae.getMessage());
        }
    }

    @GetMapping("/sort")
    public List<Parcels> sortBy(@RequestParam(required = false) Optional<String> weight,
                                @RequestParam(required = false) Optional<String> arrivalDate,
                                @RequestHeader HttpHeaders headers) {

        try {
            Users user = authenticationHelper.tryGetUser(headers);
            return parcelsService.sortBy(weight,
                    arrivalDate,
                    user);
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        } catch (IllegalArgumentException iae) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, iae.getMessage());
        }
    }
}
