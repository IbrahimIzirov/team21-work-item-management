package telerikacademy.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.controllers.AuthenticationHelper;
import telerikacademy.exceptions.DuplicateEntityException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Users;
import telerikacademy.models.Warehouses;
import telerikacademy.models.dto.WarehousesDto;
import telerikacademy.services.contract.WarehouseService;
import telerikacademy.services.mappers.WarehouseModelMapper;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/warehouses")
public class WarehouseController {

    private final WarehouseService service;
    private final WarehouseModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseController(WarehouseService service, WarehouseModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Warehouses> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Warehouses getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Warehouses create(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody WarehousesDto warehousesDto) {
        try {
            Users user = authenticationHelper.tryGetUser(headers);

            Warehouses warehouse = modelMapper.fromDto(warehousesDto);
            service.create(warehouse, user);
            return warehouse;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Warehouses update(@RequestHeader HttpHeaders headers,
                             @PathVariable int id,
                             @Valid @RequestBody WarehousesDto warehousesDto) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            Warehouses warehouse = modelMapper.fromDto(warehousesDto, id);
            service.update(warehouse, user);
            return warehouse;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {

            Users user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException u) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, u.getMessage());
        }
    }
}
