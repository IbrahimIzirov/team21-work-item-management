package telerikacademy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import telerikacademy.Constants;
import telerikacademy.exceptions.AuthenticationFailureException;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.exceptions.UnauthorizedOperationException;
import telerikacademy.models.Users;
import telerikacademy.services.contract.UsersService;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authentication";

    private final UsersService userService;

    @Autowired
    public AuthenticationHelper(UsersService userService) {
        this.userService = userService;
    }

    public Users tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constants.RESOURCE_REQUIRES_AUTHENTICATION);
        }

        try {
            String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, Constants.INVALID_EMAIL);
        }
    }

    public Users verifyAuthentication(String username, String password) {
        try {
            Users user = userService.getByUsername(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException("Wrong username or password.");
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("Wrong username or password.");
        }
    }

    public Users tryGetUser(HttpSession session) {
        String currentUsername = (String) session.getAttribute("currentUsername");

        if (currentUsername == null) {
            throw new UnauthorizedOperationException("No logged in user");
        }

        try {
            return userService.getByUsername(currentUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No logged in user");
        }
    }
}
