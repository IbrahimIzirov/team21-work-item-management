package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.models.Address;
import telerikacademy.models.Roles;
import telerikacademy.repositories.contract.RolesRepository;

import java.util.List;

@Repository
public class RolesRepositoryImpl implements RolesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RolesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Roles> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Roles", Roles.class)
                    .list();
        }
    }

    @Override
    public Roles getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Roles  where value = :name", Roles.class)
                    .setParameter("name", name)
                    .uniqueResult();
        }
    }

    @Override
    public Roles getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Roles  where id = :id", Roles.class)
                    .setParameter("id", id)
                    .uniqueResult();
        }
    }

    @Override
    public Roles create(Roles role) {
        try (Session session = sessionFactory.openSession()) {
            session.save(role);
        }
        return role;
    }
}