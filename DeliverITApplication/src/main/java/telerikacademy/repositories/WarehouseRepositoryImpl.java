package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Warehouses;
import telerikacademy.repositories.contract.WarehouseRepository;

import java.util.List;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WarehouseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Warehouses> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Warehouses ", Warehouses.class)
                    .list();
        }
    }

    @Override
    public Warehouses getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Warehouses warehouses = session.get(Warehouses.class, id);
            if (warehouses == null) {
                throw new EntityNotFoundException("Warehouse", id);
            }
            return warehouses;
        }
    }

    @Override
    public void create(Warehouses warehouses) {
        try (Session session = sessionFactory.openSession()) {
            session.save(warehouses);
        }
    }

    @Override
    public void update(Warehouses warehouses) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(warehouses);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Warehouses warehouse = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(warehouse);
            session.getTransaction().commit();
        }
    }
}
