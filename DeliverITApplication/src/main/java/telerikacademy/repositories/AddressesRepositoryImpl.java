package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Address;
import telerikacademy.models.Category;
import telerikacademy.repositories.contract.AddressesRepository;

import java.util.List;

@Repository
public class AddressesRepositoryImpl implements AddressesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddressesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Address> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Address ", Address.class)
                    .list();
        }
    }

    @Override
    public Address getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Address address = session.get(Address.class, id);
            if (address == null) {
                throw new EntityNotFoundException("Address", id);
            }
            return address;
        }
    }

    @Override
    public Address create(Address address) {
        try (Session session = sessionFactory.openSession()) {
            session.save(address);
        }
        return address;
    }
}
