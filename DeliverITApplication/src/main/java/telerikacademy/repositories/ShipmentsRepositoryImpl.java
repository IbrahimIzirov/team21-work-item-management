package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.*;
import telerikacademy.repositories.contract.ShipmentsRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class ShipmentsRepositoryImpl implements ShipmentsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ShipmentsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipments> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Shipments ", Shipments.class)
                    .getResultList();
        }
    }

    @Override
    public List<ShipmentStatus> getAllStatuses() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from ShipmentStatus ", ShipmentStatus.class)
                    .getResultList();
        }
    }

    @Override
    public Shipments getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipments shipment = session.get(Shipments.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    @Override
    public ShipmentStatus getStatusById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ShipmentStatus status = session.get(ShipmentStatus.class, id);
            if (status == null) {
                throw new EntityNotFoundException("Shipment status", id);
            }
            return status;
        }
    }

    @Override
    public List<Shipments> getByStatus(Optional<String> status) {
        try (Session session = sessionFactory.openSession()) {

            var query = session.createQuery("from Shipments p " +
                    "where p.shipmentStatus.description like concat('%', :status ,'%')", Shipments.class);

            query.setParameter("status", status
                    .orElseThrow(() -> new EntityNotFoundException("Shipment", "status", status.get())));

            return query.list();
        }
    }

    @Override
    public void create(Shipments shipments) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipments);
        }
    }

    @Override
    public void update(Shipments shipments) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipments);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Shipments shipment = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Shipments> filterByWarehouse(Optional<String> warehouses) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Shipments p " +
                    "where p.warehouses.addressId.streetName like concat('%',:warehouse,'%') ", Shipments.class);

            query.setParameter("warehouse", warehouses.orElse(""));

            return query.list();
        }
    }

    @Override
    public List<Shipments> filterByCustomer(int userId) {
        try (Session session = sessionFactory.openSession()) {

            var query = session.createQuery("select distinct s " +
                    "from Shipments s " +
                    "join fetch s.parcelsSet as parcels " +
                    "where parcels.userId.id = :userId", Shipments.class);

            query.setParameter("userId", userId);

            return query.list();
        }
    }
}
