package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Countries;
import telerikacademy.repositories.contract.CountriesRepository;

import java.util.List;

@Repository
public class CountriesRepositoryImpl implements CountriesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CountriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Countries> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Countries", Countries.class)
                    .list();
        }
    }

    @Override
    public Countries getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Countries country = session.get(Countries.class, id);
            if (country == null) {
                throw new EntityNotFoundException("Country", id);
            }
            return country;
        }
    }

    @Override
    public Countries getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Countries> query = session.createQuery("from Countries where name = :name", Countries.class);
            query.setParameter("name", name);

            List<Countries> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Country", "name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(Countries country) {
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
        }
    }

    @Override
    public void update(Countries country) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Countries country = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(country);
            session.getTransaction().commit();
        }
    }
}
