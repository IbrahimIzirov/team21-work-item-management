package telerikacademy.repositories;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Parcels;
import telerikacademy.repositories.contract.ParcelsRepository;

import java.util.List;
import java.util.Optional;

@Repository
public class ParcelsRepositoryImpl implements ParcelsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public List<Parcels> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Parcels", Parcels.class).list();
        }
    }

    @Override
    public Parcels getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Parcels parcel = session.get(Parcels.class, id);

            if (parcel == null) {
                throw new EntityNotFoundException("Parcel", id);
            }
            return parcel;
        }
    }

    @Override
    public void create(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcels);
        }
    }

    @Override
    public void update(Parcels parcels) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcels);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Parcels parcel = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Parcels> filterBy(Optional<Double> weight,
                                  Optional<String> user,
                                  Optional<String> warehouse,
                                  Optional<String> category,
                                  Optional<String> orderBy) {

        String parameter = " order by ";

        parameter = checkIfOptionalIsPresent(weight, user, warehouse, category, orderBy, parameter);

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Parcels p where p.weight >= :weight and" +
                    " p.userId.username like concat('%', :user, '%')  and" +
                    " p.warehouseId.addressId.streetName like concat ('%', :warehouse, '%')  and" +
                    " p.categoryId.name like concat('%', :category, '%')" + parameter, Parcels.class);

            query.setParameter("weight", weight.orElse(0.0));
            query.setParameter("user", user.orElse(""));
            query.setParameter("warehouse", warehouse.orElse(""));
            query.setParameter("category", category.orElse(""));

            return query.list();
        }
    }

    @Override
    public List<Parcels> sortBy(Optional<String> weight,
                                Optional<String> arrivalDate) {

        String parameter = "select p from Shipments s join s.parcelsSet as p order by ";

        if (weight.isPresent()) {
            parameter += "p.weight " + weight.get();
        }
        if (weight.isPresent() && arrivalDate.isPresent()) {
            parameter += " , s.arrivalDate " + arrivalDate.get();
        }
        if (weight.isEmpty() && arrivalDate.isPresent()) {
            parameter += " s.arrivalDate " + arrivalDate.get();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Parcels> query = session.createQuery(parameter, Parcels.class);
            return query.list();
        }
    }

    private String checkIfOptionalIsPresent(Optional<Double> weight,
                                            Optional<String> user,
                                            Optional<String> warehouse,
                                            Optional<String> category,
                                            Optional<String> orderBy,
                                            String parameter) {
        if (weight.isPresent()) {
            parameter += "p.weight, ";
        }

        if (user.isPresent()) {
            parameter += "p.userId.firstName, ";
        }

        if (warehouse.isPresent()) {
            parameter += "p.warehouseId.addressId.streetName, ";
        }

        if (category.isPresent()) {
            parameter += "p.categoryId.name, ";
        }

        parameter = parameter.substring(0, parameter.lastIndexOf(", ")) + " ";
            parameter += orderBy.get();
        return parameter;
    }
}
