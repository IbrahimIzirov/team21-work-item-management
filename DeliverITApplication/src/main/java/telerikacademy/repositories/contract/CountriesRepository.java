package telerikacademy.repositories.contract;

import telerikacademy.models.Countries;

import java.util.List;

public interface CountriesRepository {

    List<Countries> getAll();

    Countries getById(int id);

    Countries getByName(String name);

    void create(Countries country);

    void update(Countries country);

    void delete(int id);
}
