package telerikacademy.repositories.contract;

import telerikacademy.models.Parcels;

import java.util.List;
import java.util.Optional;

public interface ParcelsRepository {

    List<Parcels> getAll();

    Parcels getById(int id);

    void create(Parcels parcels);

    void update(Parcels parcels);

    void delete(int id);

    List<Parcels> filterBy(Optional<Double> weight,
                           Optional<String> user,
                           Optional<String> warehouse,
                           Optional<String> category,
                           Optional<String> orderBy);

    List<Parcels> sortBy(Optional<String> weight,
                         Optional<String> arrivalDate);
}
