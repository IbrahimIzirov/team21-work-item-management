package telerikacademy.repositories.contract;

import telerikacademy.models.Roles;

import java.util.List;

public interface RolesRepository {

    List<Roles> getAll();

    Roles getByName(String name);

    Roles getById(int id);

    Roles create(Roles role);
}
