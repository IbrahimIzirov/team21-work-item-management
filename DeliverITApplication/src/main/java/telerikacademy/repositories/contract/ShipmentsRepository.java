package telerikacademy.repositories.contract;

import telerikacademy.models.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ShipmentsRepository {

    List<Shipments> getAll();

    List<ShipmentStatus> getAllStatuses();

    Shipments getById(int id);

    ShipmentStatus getStatusById(int id);

    List<Shipments> getByStatus(Optional<String> status);

    void create(Shipments shipments);

    void update(Shipments shipments);

    void delete(int id);

    List<Shipments> filterByWarehouse(Optional<String> warehouse);

    List<Shipments> filterByCustomer(int userId);
}
