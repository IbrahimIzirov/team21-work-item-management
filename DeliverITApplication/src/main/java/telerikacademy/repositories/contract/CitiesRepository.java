package telerikacademy.repositories.contract;

import telerikacademy.models.Cities;

import java.util.List;

public interface CitiesRepository {

    List<Cities> getAllCities();

    Cities getById(int id);

    Cities getByName(String name);

    void create(Cities city);

    void update(Cities city);

    void delete(int id);
}
