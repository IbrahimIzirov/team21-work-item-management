package telerikacademy.repositories.contract;

import telerikacademy.models.Warehouses;

import java.util.List;

public interface WarehouseRepository {

    List<Warehouses> getAll();

    Warehouses getById(int id);

    void create(Warehouses warehouses);

    void update(Warehouses warehouses);

    void delete(int id);
}
