package telerikacademy.repositories.contract;

import telerikacademy.models.Roles;
import telerikacademy.models.Users;
import telerikacademy.models.Parcels;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<Users> getAll();

    Users getById(int id);

    Users getByUsername(String username);

    void create(Users user);

    void update(Users user);

    void delete(int id);

    List<Parcels> incomingParcels(int userId);

    List<Users> searchByMultiple(Optional<String> firstName,
                                 Optional<String> lastName,
                                 Optional<String> email,
                                 Optional<String> address);

    Users getByFirsName(String firstName);

    Users getByLastName(String lastName);

    Users getByEmail(String email);

    List<Users> searchByOneWord(Optional<String> oneWord);

    Long getCustomerCount();

    List<Parcels> getCustomerParcels(int id);

    Users addRole(Users user, Roles role);
}
