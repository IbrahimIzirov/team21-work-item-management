package telerikacademy.repositories.contract;

import telerikacademy.models.Address;

import java.util.List;

public interface AddressesRepository {

    List<Address> getAll();

    Address getById(int id);

    Address create(Address address);
}
