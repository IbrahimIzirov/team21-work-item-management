package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Roles;
import telerikacademy.models.Shipments;
import telerikacademy.models.Users;
import telerikacademy.models.Parcels;
import telerikacademy.repositories.contract.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Users> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Users ", Users.class);

            return query.list();
        }
    }

    @Override
    public Users getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Users user = session.get(Users.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public Users getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where username = :username", Users.class);
            query.setParameter("username", username);
            List<Users> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return users.get(0);
        }
    }

    @Override
    public void create(Users user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
    }

    @Override
    public void update(Users user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Users user = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Parcels> incomingParcels(int userId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Parcels> query = session.createQuery("SELECT p from Shipments s " +
                    "join s.parcelsSet as p " +
                    "where p.userId.id = :userId " +
                    "and s.shipmentStatus.description = 'On the way' ", Parcels.class);

            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public List<Users> searchByMultiple(Optional<String> firstName,
                                        Optional<String> lastName,
                                        Optional<String> email,
                                        Optional<String> address) {

        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Users c where c.firstName like concat('%', :firstName, '%')  and" +
                    " c.lastName like concat('%', :lastName, '%')  and" +
                    " c.email like concat('%', :email, '%')  and" +
                    " c.addressId.streetName like concat('%', :address, '%')", Users.class);

            query.setParameter("firstName", firstName.orElse(""));
            query.setParameter("lastName", lastName.orElse(""));
            query.setParameter("email", email.orElse(""));
            query.setParameter("address", address.orElse(""));

            return query.list();
        }
    }

    @Override
    public Users getByFirsName(String firstName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where firstName = :firstName", Users.class);
            query.setParameter("firstName", firstName);
            List<Users> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", firstName);
            }
            return users.get(0);
        }
    }

    @Override
    public Users getByLastName(String lastName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where lastName = :lastName", Users.class);
            query.setParameter("lastName", lastName);
            List<Users> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", lastName);
            }
            return users.get(0);
        }
    }

    @Override
    public Users getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where email = :email", Users.class);
            query.setParameter("email", email);
            List<Users> users = query.list();

            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "name", email);
            }
            return users.get(0);
        }
    }

    @Override
    public List<Users> searchByOneWord(Optional<String> oneWord) {
        try (Session session = sessionFactory.openSession()) {
            var query = session
                    .createQuery("from Users c where c.firstName like concat('%', :firstName, '%')  or" +
                            " c.lastName like concat('%', :lastName, '%')  or" +
                            " c.email like concat('%', :email, '%')  or" +
                            " c.addressId.streetName like concat('%', :address, '%')", Users.class);

            query.setParameter("firstName", oneWord.orElse(""));
            query.setParameter("lastName", oneWord.orElse(""));
            query.setParameter("email", oneWord.orElse(""));
            query.setParameter("address", oneWord.orElse(""));

            return query.list();
        }
    }

    @Override
    public Long getCustomerCount() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("select count(*) from Users ", Long.class);

            return query.uniqueResult();
        }
    }

    @Override
    public List<Parcels> getCustomerParcels(int id) {
        try (Session session = sessionFactory.openSession()) {

            Query<Parcels> query = session.createQuery("SELECT p from Shipments s " +
                    "join s.parcelsSet as p " +
                    "where p.userId.id = :id ", Parcels.class);

            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public Users addRole(Users user, Roles role) {

        Set<Roles> newList = user.getRoles();

        newList.add(role);
        user.setRoles(newList);

        return user;
    }
}