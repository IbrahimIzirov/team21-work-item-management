package telerikacademy.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerikacademy.exceptions.EntityNotFoundException;
import telerikacademy.models.Cities;
import telerikacademy.repositories.contract.CitiesRepository;

import java.util.List;

@Repository
public class CitiesRepositoryImpl implements CitiesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CitiesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Cities> getAllCities() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Cities ", Cities.class)
                    .list();
        }
    }

    @Override
    public Cities getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Cities city = session.get(Cities.class, id);
            if (city == null) {
                throw new EntityNotFoundException("City", id);
            }
            return city;
        }
    }

    @Override
    public Cities getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Cities> query = session.createQuery("from Cities where name = :name", Cities.class);
            query.setParameter("name", name);

            List<Cities> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("City", "name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(Cities city) {
        try (Session session = sessionFactory.openSession()) {
            session.save(city);
        }
    }

    @Override
    public void update(Cities city) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(city);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Cities city = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(city);
            session.getTransaction().commit();
        }
    }
}
