package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;

import java.util.List;

public interface Base {

    String getName();

    List<WorkItems> getWorkItem();

    List<EventLogImpl> getHistory();

    void addWorkItems(WorkItems workItem);

    void removeWorkItem(WorkItems workItem);
}
