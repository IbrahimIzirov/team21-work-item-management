package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

public class BoardImpl extends BaseImpl implements Board {


    public BoardImpl(String name) {
        super(name);
        addHistory(new EventLogImpl(String.format(Constants
                .NEW_BOARD_WAS_CREATED_MESSAGE, name)));
    }

    @Override
    public void addWorkItems(WorkItems workItem) {
        addHistory(new EventLogImpl(String.format(Constants
                        .WORKITEM_WAS_ADDED_TO_BOARD_MESSAGE,
                workItem.getID(),
                this.getName())));
        super.addWorkItems(workItem);
    }

    @Override
    protected void addHistory(EventLogImpl eventLogImpl) {
        super.addHistory(eventLogImpl);
    }

    @Override
    public void removeWorkItem(WorkItems workItem) {
        addHistory(new EventLogImpl(String.format(Constants
                        .WORKITEM_WAS_REMOVE_FROM_BOARD_MESSAGE,
                workItem.getID())));
        super.removeWorkItem(workItem);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
