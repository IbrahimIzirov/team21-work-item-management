package com.telerikacademy.oop.workItemManagement.models.contracts;

public interface EventLog {

    String viewInfo();
}
