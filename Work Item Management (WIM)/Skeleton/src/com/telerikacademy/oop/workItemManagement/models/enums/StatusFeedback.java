package com.telerikacademy.oop.workItemManagement.models.enums;

public enum StatusFeedback {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException("Status must be New, Unscheduled, Scheduled or Done");
        }
    }
}
