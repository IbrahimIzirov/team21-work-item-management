package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.enums.Priority;

public interface Assignable extends WorkItems {

    Member getAssignable();

    Priority getPriority();

}
