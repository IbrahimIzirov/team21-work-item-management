package com.telerikacademy.oop.workItemManagement.models.contracts;

public interface Logger {

    void log(String value);
}
