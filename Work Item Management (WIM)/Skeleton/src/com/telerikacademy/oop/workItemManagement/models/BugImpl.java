package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusBug;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemsImpl implements Bug {

    private Priority priority;
    private List<String> stepsToReproduce;
    private Severity severity;
    private Member assignee;
    private StatusBug statusBug;

    public BugImpl(String ID, String title, String description, Priority priority, Severity severity, Member assignee) {
        super(ID, title, description);

        addHistory(new EventLogImpl(String.format(Constants
                .NEW_BUG_WAS_CREATED_MESSAGE, getID())));

        setPriority(priority);
        stepsToReproduce = new ArrayList<>();
        this.severity = severity;
        setAssignee(assignee);
        this.statusBug = StatusBug.ACTIVE;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public void addStepsToReproduce(String steps) {
        addHistory(new EventLogImpl(String.format(Constants
                .STEPS_TO_REPRODUCE_WAS_ADDED_MESSAGE, getID())));
        stepsToReproduce.add(steps);
    }

    @Override
    public Severity getSeverity() {
        return severity;
    }

    @Override
    public String getStatus() {
        return statusBug.toString();
    }

    @Override
    public Member getAssignable() {
        return this.assignee;
    }

    @Override
    public void changePriority(Priority priority) {
        addHistory(new EventLogImpl(String.format(Constants
                        .PRIORITY_OF_BUG_WAS_CHANGED_MESSAGE,
                getID(),
                this.priority,
                priority)));
        this.priority = priority;
    }

    @Override
    public void changeSeverity(Severity severity) {
        addHistory(new EventLogImpl(String.format(Constants
                        .SEVERITY_OF_BUG_WAS_CHANGED_MESSAGE,
                getID(),
                this.severity,
                severity)));
        this.severity = severity;
    }

    private void setPriority(Priority priority) {
        ValidationHelpers.checkNull(priority);
        this.priority = priority;
    }

    private void setAssignee(Member assignee) {
        ValidationHelpers.checkNull(assignee);
        addHistory(new EventLogImpl(String.format(Constants
                        .ASSIGNEE_BUG_TO_MEMBER_MESSAGE,
                getID(),
                assignee.getName())));
        this.assignee = assignee;
    }

    @Override
    public void changeStatus(StatusBug statusBug) {
        if (this.statusBug.equals(statusBug)) {
            throw new IllegalArgumentException(String.format(Constants
                    .STATUS_OF_BUG_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .STATUS_OF_BUG_WAS_CHANGED_MESSAGE,
                getID(),
                this.statusBug,
                statusBug)));
        this.statusBug = statusBug;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

