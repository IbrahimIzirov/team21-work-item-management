package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final int MIN_TEAM_NAME_LENGTH = 5;
    private static final int MAX_TEAM_NAME_LENGTH = 15;
    private static final String ERROR_MESSAGE_TEAM = "Name must be between 5 and 15 symbols";

    private String name;
    private List<Member> lisOfMembers;
    private List<Board> listOfBoards;
    private List<EventLogImpl> history;

    public TeamImpl(String name) {
        setName(name);
        lisOfMembers = new ArrayList<>();
        listOfBoards = new ArrayList<>();
        history = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(lisOfMembers);
    }

    @Override
    public List<Board> getAllBoards() {
        return new ArrayList<>(listOfBoards);
    }

    @Override
    public void addMember(Member member) {
        addHistory(new EventLogImpl(String.format(Constants
                .MEMBER_ADDED_TO_TEAM_MESSAGE, member.getName(), this.getName())));
        lisOfMembers.add(member);
    }

    @Override
    public void addBoard(Board board) {
        addHistory(new EventLogImpl(String.format(Constants
                .BOARD_ADDED_TO_TEAM_MESSAGE, board.getName(), this.getName())));
        listOfBoards.add(board);
    }

    private void setName(String name) {
        ValidationHelpers.checkStringLength(name,
                MIN_TEAM_NAME_LENGTH,
                MAX_TEAM_NAME_LENGTH,
                ERROR_MESSAGE_TEAM);
        this.name = name;
    }

    public String catalogBoards() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format(Constants.TEAM_BOARD_CATALOG_MESSAGE, name));

        int count = 1;

        for (Board board : listOfBoards) {
            strBuilder.append(String.format("%d. %s%n", count++, board.getName()));
        }

        return strBuilder.toString().trim();
    }

    public String catalogMembers() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format(Constants.TEAM_MEMBER_CATALOG_MESSAGE, name));

        int count = 1;

        for (Member member : lisOfMembers) {
            strBuilder.append(String.format("%d. %s%n", count++, member.getName()));
        }

        return strBuilder.toString().trim();
    }

    @Override
    public List<EventLogImpl> getHistory() {
        return new ArrayList<>(history);
    }

    protected void addHistory(EventLogImpl eventLogImpl) {
        this.history.add(eventLogImpl);
    }

}
