package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusFeedback;

public class FeedbackImpl extends WorkItemsImpl implements Feedback {

    private int rating;
    private StatusFeedback statusFeedback;

    public FeedbackImpl(String ID, String title, String description, int rating) {
        super(ID, title, description);

        addHistory(new EventLogImpl(String.format(Constants
                .NEW_FEEDBACK_WAS_CREATED_MESSAGE, getID())));

        changeRating(rating);
        this.statusFeedback = StatusFeedback.NEW;
    }


    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public String getStatus() {
        return statusFeedback.toString();
    }

    @Override
    public void changeRating(int rating) {
        ValidationHelpers.CheckNegativeNumber(rating);
        if (this.rating == rating) {
            throw new IllegalArgumentException(String.format(Constants
                    .RATING_OF_FEEDBACK_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .RATING_OF_FEEDBACK_WAS_CHANGED_MESSAGE,
                getID(),
                this.rating,
                rating)));
        this.rating = rating;
    }

    @Override
    public void changeStatus(StatusFeedback statusFeedback) {
        if (this.statusFeedback.equals(statusFeedback)) {
            throw new IllegalArgumentException(String.format(Constants
                    .STATUS_OF_FEEDBACK_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .STATUS_OF_FEEDBACK_WAS_CHANGED_MESSAGE,
                getID(),
                this.statusFeedback,
                statusFeedback)));
        this.statusFeedback = statusFeedback;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
