package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.Base;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseImpl implements Base {

    private static final String NAME_LENGTH_ERROR_MESSAGE = "Name must be between 5 and 15 symbols!";
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;

    private String name;
    private List<WorkItems> workItems;
    private List<EventLogImpl> activityHistory;

    public BaseImpl(String name) {
        setName(name);
        this.workItems = new ArrayList<>();
        this.activityHistory = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItems> getWorkItem() {
        return new ArrayList<>(workItems);
    }


    @Override
    public List<EventLogImpl> getHistory() {
        return new ArrayList<>(activityHistory);
    }


    protected void addHistory(EventLogImpl eventLogImpl) {
        this.activityHistory.add(eventLogImpl);
    }

    public void addWorkItems(WorkItems workItem) {
        this.workItems.add(workItem);
    }

    private void setName(String name) {
        ValidationHelpers.checkStringLength(name,
                NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                NAME_LENGTH_ERROR_MESSAGE);
        this.name = name;
    }

    public void removeWorkItem(WorkItems workItem) {
        this.workItems.remove(workItem);
    }

    @Override
    public String toString() {
        return String.format("%s", getName());
    }
}
