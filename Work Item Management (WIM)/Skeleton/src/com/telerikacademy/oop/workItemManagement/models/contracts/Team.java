package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;

import java.util.List;

public interface Team {

    String getName();

    List<Member> getMembers();

    List<Board> getAllBoards();

    void addMember(Member member);

    void addBoard(Board board);

    String catalogBoards();

    String catalogMembers();

    List<EventLogImpl> getHistory();
}
