package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusFeedback;

public interface Feedback extends WorkItems {

    int getRating();

    void changeRating(int rating);

    void changeStatus(StatusFeedback statusFeedback);

}
