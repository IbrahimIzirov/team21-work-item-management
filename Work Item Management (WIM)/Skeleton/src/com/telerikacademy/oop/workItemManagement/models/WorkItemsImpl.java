package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemsImpl implements WorkItems {


    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MIN_TITLE_LENGTH = MIN_DESCRIPTION_LENGTH;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final String TITLE_ERROR_MESSAGE = "Title must be between 10 and 50 symbols.";
    private static final int MAX_DESCRIPTION_LENGTH = 500;
    private static final String DESCRIPTION_ERROR_MESSAGE = "Description must be between 10 and 500 symbols.";

    private String ID;
    private String title;
    private String description;
    private List<EventLogImpl> history;
    private List<String> comments;

    public WorkItemsImpl(String ID, String title, String description) {
        setID(ID);
        setTitle(title);
        setDescription(description);
        this.history = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<EventLogImpl> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public List<String> getComment() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<String> addComment(String comment) {
        comments.add(comment);
        return new ArrayList<>(comments);
    }

    private void setID(String ID) {
        ValidationHelpers.checkID(ID);
        this.ID = ID;
    }

    private void setTitle(String title) {
        ValidationHelpers.checkStringLength(title,
                MIN_TITLE_LENGTH,
                MAX_TITLE_LENGTH,
                TITLE_ERROR_MESSAGE);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelpers.checkStringLength(description,
                MIN_DESCRIPTION_LENGTH,
                MAX_DESCRIPTION_LENGTH,
                DESCRIPTION_ERROR_MESSAGE);
        this.description = description;
    }

    protected void addHistory(EventLogImpl eventLogsImpl) {
        this.history.add(eventLogsImpl);
    }

    @Override
    public String toString() {
        return String.format("%s", getID());
    }
}
