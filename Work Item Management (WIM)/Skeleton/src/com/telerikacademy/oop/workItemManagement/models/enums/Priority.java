package com.telerikacademy.oop.workItemManagement.models.enums;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW;

    public boolean isValid(String value) {
        for (Priority val : Priority.values()) {
            if (val.toString().equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        switch (this) {
            case LOW:
                return "Low";
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            default:
                throw new IllegalArgumentException("Priority must be Low, Medium or High");
        }
    }
}
