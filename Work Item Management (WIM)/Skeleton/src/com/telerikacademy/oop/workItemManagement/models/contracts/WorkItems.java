package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;

import java.util.List;

public interface WorkItems {

    String getID();

    String getTitle();

    String getStatus();

    String getDescription();

    List<String> getComment();

    List<EventLogImpl> getHistory();

    List<String> addComment(String comment);
}
