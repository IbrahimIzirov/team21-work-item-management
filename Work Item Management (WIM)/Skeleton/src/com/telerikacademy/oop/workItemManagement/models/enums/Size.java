package com.telerikacademy.oop.workItemManagement.models.enums;

public enum Size {
    LARGE,
    MEDIUM,
    SMALL;

    @Override
    public String toString() {
        switch (this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new IllegalArgumentException("Size must be Large, Medium or Small");
        }
    }
}
