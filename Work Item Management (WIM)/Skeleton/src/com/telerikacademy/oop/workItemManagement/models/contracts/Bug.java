package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusBug;

import java.util.List;

public interface Bug extends WorkItems, Assignable {


    List<String> getStepsToReproduce();

    void addStepsToReproduce(String steps);

    Severity getSeverity();

    void changePriority(Priority priority);

    void changeSeverity(Severity severity);

    void changeStatus(StatusBug statusBug);

}
