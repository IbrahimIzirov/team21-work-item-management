package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

public class MemberImpl extends BaseImpl implements Member {


    public MemberImpl(String name) {
        super(name);
        addHistory(new EventLogImpl(String.format(Constants
                .NEW_MEMBER_WAS_CREATED_MESSAGE, name)));
    }

    @Override
    public void addWorkItems(WorkItems workItem) {
        addHistory(new EventLogImpl(String.format(Constants
                        .WORKITEM_WAS_ADDED_TO_MEMBER_MESSAGE,
                workItem.getID(),
                getName())));
        super.addWorkItems(workItem);
    }

    @Override
    protected void addHistory(EventLogImpl eventLogImpl) {
        super.addHistory(eventLogImpl);
    }

    public void removeWorkItem(WorkItems workItem) {
        addHistory(new EventLogImpl(String.format(Constants
                        .WORKITEM_WAS_REMOVE_FROM_MEMBER_MESSAGE,
                workItem.getID(),
                getName())));
        super.removeWorkItem(workItem);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
