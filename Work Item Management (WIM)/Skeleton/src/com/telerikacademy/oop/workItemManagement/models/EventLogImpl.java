package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.EventLog;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLogImpl implements EventLog {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm");

    private final String description;
    private final LocalDateTime timestamp;


    public EventLogImpl(String description) {
        ValidationHelpers.checkNull(description);
        this.description = description;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public String viewInfo() {
        return String.format("[%s] %s", timestamp.format(formatter), description);
    }

}
