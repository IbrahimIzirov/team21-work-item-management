package com.telerikacademy.oop.workItemManagement.models.enums;

public enum StatusStory {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "Not done";
            case INPROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException("Status must be Not done, In progress or Done");
        }
    }
}
