package com.telerikacademy.oop.workItemManagement.models.contracts;

import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusStory;

public interface Story extends WorkItems, Assignable {

    Size getSize();

    void changeStatus(StatusStory statusStory);

    void changePriority(Priority priority);

    void changeSize(Size size);
}
