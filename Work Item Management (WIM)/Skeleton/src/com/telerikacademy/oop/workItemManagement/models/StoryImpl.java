package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusStory;

public class StoryImpl extends WorkItemsImpl implements Story {

    private Size size;
    private Priority priority;
    private Member assignee;
    private StatusStory statusStory;

    public StoryImpl(String ID, String title, String description, Priority priority, Size size, Member assignee) {
        super(ID, title, description);

        addHistory(new EventLogImpl(String.format(Constants
                .NEW_STORY_WAS_CREATED_MESSAGE, getID())));

        this.size = size;
        this.priority = priority;
        setAssignee(assignee);
        this.statusStory = StatusStory.NOTDONE;
    }


    @Override
    public String getStatus() {
        return statusStory.toString();
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public Member getAssignable() {
        return this.assignee;
    }

    @Override
    public void changeStatus(StatusStory statusStory) {
        if (this.statusStory.equals(statusStory)) {
            throw new IllegalArgumentException(String.format(Constants
                    .STATUS_OF_STORY_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .STATUS_OF_STORY_WAS_CHANGED_MESSAGE,
                getID(),
                this.statusStory,
                statusStory)));
        this.statusStory = statusStory;
    }

    @Override
    public void changePriority(Priority priority) {
        if (this.priority.equals(priority)) {
            throw new IllegalArgumentException(String.format(Constants
                    .PRIORITY_OF_STORY_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .PRIORITY_OF_STORY_WAS_CHANGED_MESSAGE,
                getID(),
                this.priority,
                priority)));
        this.priority = priority;
    }

    private void setAssignee(Member assignee) {
        ValidationHelpers.checkNull(assignee);
        addHistory(new EventLogImpl(String.format(Constants.
                        ASSIGNEE_STORY_TO_MEMBER_MESSAGE,
                getID(),
                assignee.getName())));
        this.assignee = assignee;
    }

    @Override
    public void changeSize(Size size) {
        if (this.size.equals(size)) {
            throw new IllegalArgumentException(String.format(Constants
                    .SIZE_OF_STORY_IS_SAME_MESSAGE, getID()));
        }
        addHistory(new EventLogImpl(String.format(Constants
                        .SIZE_OF_STORY_WAS_CHANGED_MESSAGE,
                getID(),
                size)));
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
