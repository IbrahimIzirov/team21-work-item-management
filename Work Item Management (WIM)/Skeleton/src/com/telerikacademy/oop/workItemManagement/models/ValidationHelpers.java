package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.enums.*;

import java.util.List;

public class ValidationHelpers {

    private static final String NULL_INPUT = "Input cannot be null!";
    private static final String NEGATIVE_NUMBER = "Number cannot be negative";
    private static final String STATUS_OF_BUG_CORRECT = "Status must be Active or Fixed.";
    private static final String SIZE_OF_STORY_CORRECT = "Size must be Large, Medium or Small";
    private static final String STATUS_OF_STORY_CORRECT = "Status must be Not done, In progress or Done";
    private static final String STATUS_OF_FEEDBACK_CORRECT = "Status must be New, Unscheduled, Scheduled or Done";
    private static final String PRIORITY_CORRECT = "Priority must be Low, Medium or High";

    public static void checkID(String ID) {
        checkNull(ID);
    }

    public static void checkStringLength(String str, int min, int max, String message) {
        checkNull(str);
        if (str.trim().length() < min || str.trim().length() > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void checkNull(Object obj) {
        if (obj == null) {
            throw new NullPointerException(NULL_INPUT);
        }
    }

    public static void CheckNegativeNumber(int number) {
        if (number < 0) {
            throw new IllegalArgumentException(NEGATIVE_NUMBER);
        }
    }

    public static void checkIfStatusOfBugIsCorrect(List<StatusBug> statusBugList, String parameterBugStatus) {
        int counter = 0;
        for (StatusBug status : statusBugList) {
            if (!(parameterBugStatus.toUpperCase().equalsIgnoreCase(String.valueOf(status)))) {
                counter++;
            }
        }
        if (counter == 2) {
            throw new IllegalArgumentException(STATUS_OF_BUG_CORRECT);
        }
    }

    public static void checkIfSizeOfStoryIsCorrect(List<Size> sizeStoryList, String parameterSize) {
        int counter = 0;
        for (Size size : sizeStoryList) {
            if (!(parameterSize.toUpperCase().equalsIgnoreCase(String.valueOf(size)))) {
                counter++;
            }
        }
        if (counter == 3) {
            throw new IllegalArgumentException(SIZE_OF_STORY_CORRECT);
        }
    }

    public static void checkIfStatusOfStoryIsCorrect(List<StatusStory> statusStories, String parameterStoryStatus) {
        int counter = 0;
        for (StatusStory status : statusStories) {
            if (!(parameterStoryStatus.toUpperCase()
                    .equalsIgnoreCase(String.valueOf(status).replace(" ", "")))) {
                counter++;
            }
        }
        if (counter == 3) {
            throw new IllegalArgumentException(STATUS_OF_STORY_CORRECT);
        }
    }

    public static void checkIfStatusOfFeedbackIsCorrect(List<StatusFeedback> statusFeedbacks, String parameterFeedbackStatus) {
        int counter = 0;
        for (StatusFeedback status : statusFeedbacks) {
            if (!(parameterFeedbackStatus.toUpperCase().equalsIgnoreCase(String.valueOf(status)))) {
                counter++;
            }
        }
        if (counter == 4) {
            throw new IllegalArgumentException(STATUS_OF_FEEDBACK_CORRECT);
        }
    }

    public static void checkIfPriorityIsCorrect(List<Priority> priorities, String parameter) {
        int counter = 0;
        for (Priority priority : priorities) {
            if (!(parameter.toUpperCase().equalsIgnoreCase(String.valueOf(priority)))) {
                counter++;
            }
        }
        if (counter == 3) {
            throw new IllegalArgumentException(PRIORITY_CORRECT);
        }
    }
}
