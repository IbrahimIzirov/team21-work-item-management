package com.telerikacademy.oop.workItemManagement.core.contracts;

public interface Engine {

    void start();
}
