package com.telerikacademy.oop.workItemManagement.core.contracts;

import com.telerikacademy.oop.workItemManagement.models.contracts.*;

import java.util.List;
import java.util.Map;

public interface ManagementRepository {

    Map<String, Team> getTeams();

    Map<String, Member> getMembers();

    Map<String, Board> getBoards();

    List<WorkItems> getAllWorkItems();

    List<Bug> getAllBugs();

    List<Story> getAllStories();

    List<Feedback> getAllFeedbacks();

    List<Assignable> getAssignable();

    void addTeam(String name, Team team);

    void addMember(String name, Member member);

    void addBoard(String name, Board board);

    void addBugs(Bug bug);

    void addStory(Story story);

    void addFeedback(Feedback feedback);

}
