package com.telerikacademy.oop.workItemManagement.core.contracts;

public interface Reader {

    String readLine();

}
