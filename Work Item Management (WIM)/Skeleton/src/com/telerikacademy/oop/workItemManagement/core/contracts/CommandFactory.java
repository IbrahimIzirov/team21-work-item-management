package com.telerikacademy.oop.workItemManagement.core.contracts;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;

public interface CommandFactory {

    Command createCommand(String commandTypeAsString, ManagementFactory factory, ManagementRepository agencyRepository);

}
