package com.telerikacademy.oop.workItemManagement.core.contracts;

import com.telerikacademy.oop.workItemManagement.models.contracts.*;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;

public interface ManagementFactory {

    Team createTeam(String name);

    Member createMember(String name);

    Board createBoard(String name);

    Bug createBugs(String ID, String title, String description, String priority, String severity, Member assignee);

    Story createStory(String ID, String title, String description, String priority, String size, Member assignee);

    Feedback createFeedback(String ID, String title, String description, int rating);


}
