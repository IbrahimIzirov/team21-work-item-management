package com.telerikacademy.oop.workItemManagement.core.contracts;

public interface Writer {

    void write(String message);

    void writeLine(String message);
}
