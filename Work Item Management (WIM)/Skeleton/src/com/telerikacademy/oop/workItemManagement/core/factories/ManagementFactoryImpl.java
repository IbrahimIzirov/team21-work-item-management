package com.telerikacademy.oop.workItemManagement.core.factories;

import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.models.*;
import com.telerikacademy.oop.workItemManagement.models.contracts.*;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;

import java.util.List;

public class ManagementFactoryImpl implements ManagementFactory {

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBugs(String ID, String title, String description, String priority, String severity, Member assignee) {
        return new BugImpl(ID, title, description, getPriority(priority), getSeverity(severity), assignee);
    }

    @Override
    public Story createStory(String ID, String title, String description, String priority, String size, Member assignee) {
        return new StoryImpl(ID, title, description, getPriority(priority), getSize(size), assignee);
    }

    @Override
    public Feedback createFeedback(String ID, String title, String description, int rating) {
        return new FeedbackImpl(ID, title, description, rating);
    }

    private Priority getPriority(String priority) {
        return Priority.valueOf(priority.toUpperCase());
    }

    private Severity getSeverity(String severity) {
        return Severity.valueOf(severity.toUpperCase());
    }

    private Size getSize(String size) {
        return Size.valueOf(size.toUpperCase());
    }
}
