package com.telerikacademy.oop.workItemManagement.core;

import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagementRepositoryImpl implements ManagementRepository {

    private Map<String, Team> teams;
    private Map<String, Member> members;
    private Map<String, Board> boards;
    private List<Bug> bugList;
    private List<Story> storyList;
    private List<Feedback> feedbackList;

    public ManagementRepositoryImpl() {
        this.teams = new HashMap<>();
        this.members = new HashMap<>();
        this.boards = new HashMap<>();
        this.bugList = new ArrayList<>();
        this.storyList = new ArrayList<>();
        this.feedbackList = new ArrayList<>();
    }

    @Override
    public Map<String, Team> getTeams() {
        return new HashMap<>(teams);
    }

    @Override
    public Map<String, Member> getMembers() {
        return new HashMap<>(members);
    }

    @Override
    public Map<String, Board> getBoards() {
        return new HashMap<>(boards);
    }

    @Override
    public List<WorkItems> getAllWorkItems() {

        List<WorkItems> listAllWorkItems = new ArrayList<>();

        listAllWorkItems.addAll(bugList);
        listAllWorkItems.addAll(storyList);
        listAllWorkItems.addAll(feedbackList);

        return new ArrayList<>(listAllWorkItems);
    }

    @Override
    public List<Bug> getAllBugs() {
        return new ArrayList<>(bugList);
    }

    @Override
    public List<Story> getAllStories() {
        return new ArrayList<>(storyList);
    }

    @Override
    public List<Feedback> getAllFeedbacks() {
        return new ArrayList<>(feedbackList);
    }

    @Override
    public List<Assignable> getAssignable() {

        List<Assignable> listAssignable = new ArrayList<>();

        listAssignable.addAll(bugList);
        listAssignable.addAll(storyList);


        return new ArrayList<>(listAssignable);
    }

    @Override
    public void addTeam(String name, Team team) {
        this.teams.put(name, team);
    }

    @Override
    public void addMember(String name, Member member) {
        this.members.put(name, member);
    }

    @Override
    public void addBoard(String name, Board board) {
        this.boards.put(name, board);
    }

    @Override
    public void addBugs(Bug bug) {
        this.bugList.add(bug);
    }

    @Override
    public void addStory(Story story) {
        this.storyList.add(story);
    }

    @Override
    public void addFeedback(Feedback feedback) {
        this.feedbackList.add(feedback);
    }
}
