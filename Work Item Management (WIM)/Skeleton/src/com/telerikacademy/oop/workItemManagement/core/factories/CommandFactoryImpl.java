package com.telerikacademy.oop.workItemManagement.core.factories;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.*;
import com.telerikacademy.oop.workItemManagement.commands.enums.CommandType;
import com.telerikacademy.oop.workItemManagement.core.contracts.CommandFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command createCommand(String commandTypeAsString, ManagementFactory managementFactory, ManagementRepository managementRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case CREATEPERSON:
                return new CreatePerson(managementFactory, managementRepository);
            case SHOWALLPEOPLE:
                return new ShowPeopleList(managementRepository);
            case SHOWPERSONSACTIVITY:
                return new ShowPersonsActivity(managementRepository);
            case SHOWWORKITEMACTIVITY:
                return new ShowWorkItemsActivity(managementRepository);
            case CREATETEAM:
                return new CreateTeam(managementFactory, managementRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeams(managementRepository);
            case SHOWTEAMSACTIVITY:
                return new ShowTeamsActivity(managementRepository);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(managementRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(managementRepository);
            case CREATEBOARD:
                return new CreateBoard(managementRepository, managementFactory);
            case SHOWALLBOARDS:
                return new ShowAllBoards(managementRepository);
            case SHOWBOARDSACTIVITY:
                return new ShowBoardsActivity(managementRepository);
            case CREATEBUGINBOARD:
                return new CreateBugs(managementRepository, managementFactory);
            case CREATESTORYINBOARD:
                return new CreateStory(managementRepository, managementFactory);
            case CREATEFEEDBACKINBOARD:
                return new CreateFeedback(managementRepository, managementFactory);
            case CHANGEPRIORITYOFBUG:
                return new ChangePriorityOfBug(managementRepository);
            case CHANGESEVERITYOFBUG:
                return new ChangeSeverityOfBug(managementRepository);
            case CHANGESTATUSOFBUG:
                return new ChangeStatusOfBug(managementRepository);
            case CHANGEPRIORITYOFSTORY:
                return new ChangePriorityOfStory(managementRepository);
            case CHANGESIZEOFSTORY:
                return new ChangeSizeOfStory(managementRepository);
            case CHANGESTATUSOFSTORY:
                return new ChangeStatusOfStory(managementRepository);
            case CHANGERATINGOFFEEDBACK:
                return new ChangeRatingOfFeedback(managementRepository);
            case CHANGESTATUSOFFEEDBACK:
                return new ChangeStatusOfFeedback(managementRepository);
            case ASSIGNEEWORKITEM:
                return new AssigneeWorkItem(managementRepository);
            case UNASSIGNEDWORKITEM:
                return new UnassignedWorkItem(managementRepository);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(managementRepository);
            case ADDSTEPSTOREPRODUCE:
                return new AddStepsToReproduce(managementRepository);
            case GETSTEPSTOREPRODUCE:
                return new GetStepsToReproduce(managementRepository);
            case LISTSORT:
                return new ListSort(managementRepository);
            case LISTFILTER:
                return new ListFilter(managementRepository);
            case LISTONLY:
                return new ListOnlyElements(managementRepository);
        }
        throw new IllegalArgumentException(String.format(Constants.INVALID_COMMAND_ERROR_MESSAGE, commandTypeAsString));
    }
}
