package com.telerikacademy.oop.workItemManagement;

import com.telerikacademy.oop.workItemManagement.core.ManagementEngineImpl;

public class Startup {
    public static void main(String[] args) {
        ManagementEngineImpl engine = new ManagementEngineImpl();
        engine.start();
    }
}
