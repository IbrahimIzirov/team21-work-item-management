package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;

import java.util.List;

public class ShowPersonsActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
    private final ManagementRepository managementRepository;

    public ShowPersonsActivity(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;

    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(Constants.INVALID_NUMBER_OF_ARGUMENTS);
        }

        if (managementRepository.getMembers().size() == 0) {
            throw new IllegalArgumentException("There is no registered person in this list.");
        }

        StringBuilder builder = new StringBuilder("---List All Person Activity---\n");

        for (Member member : managementRepository.getMembers().values()) {
            for (EventLogImpl eventLogImpl : member.getHistory()) {
                builder.append(String.format("%s%n",
                        eventLogImpl.viewInfo()));
            }
        }
        return builder.toString();
    }
}
