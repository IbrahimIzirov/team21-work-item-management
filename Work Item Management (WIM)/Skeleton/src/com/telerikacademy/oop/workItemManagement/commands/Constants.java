package com.telerikacademy.oop.workItemManagement.commands;

public class Constants {

    // Command Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    public static final String JOIN_DELIMITER = "####################";
    public static final String INVALID_COMMAND_ERROR_MESSAGE = "Invalid command name: %s";
    public static final String PERSON_EXISTS_ERROR_MESSAGE = "Person %s already exists!";
    public static final String PERSON_NOT_FOUND_ERROR_MESSAGE = "Member %s not found!";
    public static final String BOARD_NOT_FOUND_ERROR_MESSAGE = "Board %s not found!";
    public static final String BUG_ALREADY_EXIST_MESSAGE = "Bug %s already exist!";
    public static final String BOARD_EXISTS_ERROR_MESSAGE = "Board %s already exists in team: %s!";
    public static final String TEAM_EXISTS_ERROR_MESSAGE = "Team %s already exists!";
    public static final String TEAM_NOT_FOUND_ERROR_MESSAGE = "Team %s not found!";
    public static final String BUG_NOT_FOUND_ERROR_MESSAGE = "Bug with ID: %s not found!";
    public static final String FEEDBACK_NOT_FOUND_ERROR_MESSAGE = "Feedback with ID: %s not found!";
    public static final String STORY_NOT_FOUND_ERROR_MESSAGE = "Story with ID: %s not found!";
    public static final String WORK_ITEM_NOT_FOUND_ERROR_MESSAGE = "WorkItem %s not found!";

    // Command Success messages
    public static final String PERSON_CREATED_SUCCESS_MESSAGE = "Person with name %s was created";
    public static final String STEPS_TO_REPRODUCE_ADD_SUCCESS = "Steps to reproduce add to bug with name %s.";
    public static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team with name %s was created";
    public static final String MEMBER_ADDED_SUCCESS_MESSAGE = "Member %s added to Team %s.";
    public static final String BOARD_ADDED_SUCCESS_MESSAGE = "Board %s added to Team %s.";
    public static final String BUG_ADDED_SUCCESS_MESSAGE = "Bug %s added to Board %s.";
    public static final String STORY_ADDED_SUCCESS_MESSAGE = "Story %s added to Board %s.";
    public static final String FEEDBACK_ADDED_SUCCESS_MESSAGE = "Feedback %s added to Board %s.";
    public static final String PRIORITY_CHANGED_SUCCESS_MESSAGE = "%s with ID: %s, Priority was changed from %s to %s.";
    public static final String RATING_CHANGED_SUCCESS_MESSAGE = "%s with ID: %s, Rating was changed from: %s to: %s";
    public static final String SIZE_CHANGED_SUCCESS_MESSAGE = "%s with ID: %s, Size was changed from: %s to: %s";
    public static final String SEVERITY_CHANGED_SUCCESS_MESSAGE = "%s with ID: %s, Severity was changed from: %s to: %s";
    public static final String STATUS_CHANGED_SUCCESS_MESSAGE = "%s with ID: %s, Status was changed from: %s to: %s";
    public static final String COMMENT_ADDED_SUCCESS_MESSAGE = "Comment %s added to WorkItem %s.";
    public static final String WORK_ITEM_ASSIGNEE_SUCCESS_MESSAGE = "Work Item with ID: %s, Was assignee to: %s.";
    public static final String WORK_ITEM_UNASSIGNED_SUCCESS_MESSAGE = "Work Item with ID: %s, Was unassigned from: %s.";

    // TeamImpl messages
    public static final String MEMBER_ADDED_TO_TEAM_MESSAGE = "Member: %s was added to Team: %s.";
    public static final String BOARD_ADDED_TO_TEAM_MESSAGE = "Board: %s was added to Team: %s.";
    public static final String TEAM_BOARD_CATALOG_MESSAGE = "%s Team Boards:%n";
    public static final String TEAM_MEMBER_CATALOG_MESSAGE = "%s Team Members:%n";

    // BoardImpl messages
    public static final String NEW_BOARD_WAS_CREATED_MESSAGE = "A new Board %s was created.";
    public static final String WORKITEM_WAS_ADDED_TO_BOARD_MESSAGE = "WorkItem %s was added to Board %s.";
    public static final String WORKITEM_WAS_REMOVE_FROM_BOARD_MESSAGE = "WorkItem %s was removed.";

    // MemberImpl messages
    public static final String NEW_MEMBER_WAS_CREATED_MESSAGE = "A new Member %s was created.";
    public static final String WORKITEM_WAS_ADDED_TO_MEMBER_MESSAGE = "WorkItem %s was added to %s.";
    public static final String WORKITEM_WAS_REMOVE_FROM_MEMBER_MESSAGE = "WorkItem %s was removed from %s.";

    // BugImpl messages
    public static final String NEW_BUG_WAS_CREATED_MESSAGE = "A new Bug %s was created.";
    public static final String STEPS_TO_REPRODUCE_WAS_ADDED_MESSAGE = "Steps to reproduce added success of Bug with ID: %s";
    public static final String PRIORITY_OF_BUG_WAS_CHANGED_MESSAGE = "Priority of Bug %s  was changed from %s to %s.";
    public static final String SEVERITY_OF_BUG_WAS_CHANGED_MESSAGE = "Severity of Bug %s was changed from %s to %s.";
    public static final String ASSIGNEE_BUG_TO_MEMBER_MESSAGE = "Bug %s was assignee to %s.";
    public static final String STATUS_OF_BUG_IS_SAME_MESSAGE = "Status of Bug %s is the same";
    public static final String STATUS_OF_BUG_WAS_CHANGED_MESSAGE = "Status of Bug %s was changed from %s to %s.";

    // FeedbackImpl messages
    public static final String NEW_FEEDBACK_WAS_CREATED_MESSAGE = "A new Feedback %s was created.";
    public static final String RATING_OF_FEEDBACK_IS_SAME_MESSAGE = "Rating of Feedback %s is the same";
    public static final String RATING_OF_FEEDBACK_WAS_CHANGED_MESSAGE = "Rating of Feedback %s was changed from %d to %d.";
    public static final String STATUS_OF_FEEDBACK_IS_SAME_MESSAGE = "Status of Feedback %s is the same";
    public static final String STATUS_OF_FEEDBACK_WAS_CHANGED_MESSAGE = "Status of Feedback %s was changed from %s to %s.";

    // StoryImpl messages
    public static final String NEW_STORY_WAS_CREATED_MESSAGE = "A new Story %s was created.";
    public static final String SIZE_OF_STORY_WAS_CHANGED_MESSAGE = "Size of Story %s was changed to %s";
    public static final String ASSIGNEE_STORY_TO_MEMBER_MESSAGE = "Story %s was assignee to %s.";
    public static final String STATUS_OF_STORY_IS_SAME_MESSAGE = "Status of Story %s  is the same";
    public static final String SIZE_OF_STORY_IS_SAME_MESSAGE = "Size of Story %s is the same";
    public static final String PRIORITY_OF_STORY_IS_SAME_MESSAGE = "Priority of Story %s  is the same";
    public static final String STATUS_OF_STORY_WAS_CHANGED_MESSAGE = "Status of Story %s was changed from %s to %s.";
    public static final String PRIORITY_OF_STORY_WAS_CHANGED_MESSAGE = "Priority of Story %s was changed from %s to %s.";

}
