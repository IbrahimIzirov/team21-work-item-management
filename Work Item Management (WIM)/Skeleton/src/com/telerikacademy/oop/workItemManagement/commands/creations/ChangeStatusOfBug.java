package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.ValidationHelpers;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusBug;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeStatusOfBug implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangeStatusOfBug(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String bugID = parameters.get(0);
        String bugStatus = parameters.get(1);

        Bug bug = managementRepository.getAllBugs()
                .stream()
                .filter(o -> o.getID().equals(bugID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.BUG_NOT_FOUND_ERROR_MESSAGE, bugID)));

        String currentStatus = String.valueOf(bug.getStatus());
        List<StatusBug> listStatusOfBugs = new ArrayList<>(Arrays.asList(StatusBug.values()));

        ValidationHelpers.checkIfStatusOfBugIsCorrect(listStatusOfBugs, bugStatus);

        bug.changeStatus(StatusBug.valueOf(bugStatus.toUpperCase()));

        return String.format(Constants.STATUS_CHANGED_SUCCESS_MESSAGE,
                bug.getClass().getSimpleName().replace("Impl", ""),
                bugID,
                currentStatus.toUpperCase(),
                bugStatus.toUpperCase());
    }
}
