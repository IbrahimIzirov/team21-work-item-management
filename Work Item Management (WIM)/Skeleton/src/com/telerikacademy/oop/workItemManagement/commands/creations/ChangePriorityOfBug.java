package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.ValidationHelpers;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangePriorityOfBug implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangePriorityOfBug(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String bugID = parameters.get(0);
        String bugPriority = parameters.get(1);

        Bug bug = managementRepository.getAllBugs()
                .stream()
                .filter(o -> o.getID().equals(bugID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.BUG_NOT_FOUND_ERROR_MESSAGE, bugID)));


        String currentPriority = String.valueOf(bug.getPriority());
        List<Priority> priorities = new ArrayList<>(Arrays.asList(Priority.values()));

        ValidationHelpers.checkIfPriorityIsCorrect(priorities, bugPriority);
        bug.changePriority(Priority.valueOf(bugPriority.toUpperCase()));

        return String.format(Constants.PRIORITY_CHANGED_SUCCESS_MESSAGE,
                bug.getClass().getSimpleName().replace("Impl", ""),
                bugID,
                currentPriority.toUpperCase(),
                bugPriority.toUpperCase());
    }
}
