package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;

import java.util.List;

public class ShowAllTeams implements Command {

    private final ManagementRepository managementRepository;

    public ShowAllTeams(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (managementRepository.getTeams().size() == 0) {
            throw new IllegalArgumentException("There are no registered teams in list.");
        }

        return showAllTeams();
    }

    private String showAllTeams() {

        StringBuilder builder = new StringBuilder();
        builder.append("--Teams--").append(System.lineSeparator());
        int counter = 1;

        for (String team : managementRepository.getTeams().keySet()) {
            builder.append(String.format("%d. %s", counter, team))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }
}
