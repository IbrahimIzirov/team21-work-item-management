package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListSort implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementRepository managementRepository;

    public ListSort(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String commandType = parameters.get(0);

        return WorkItemsSort(commandType);
    }

    private String WorkItemsSort(String command) {
        switch (command.toLowerCase()) {
            case "size":
                return ListSortBySize();
            case "rating":
                return ListSortByRating();
            case "priority":
                return ListSortByPriority();
            case "severity":
                return ListSortBySeverity();
            case "title":
                return ListSortByTitle();
            default:
                throw new IllegalArgumentException("Invalid Argument. Try again!");
        }
    }

    private String ListSortBySize() {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("List of work items is empty");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Work Item Sort By Size---").append(System.lineSeparator());

        Collections.sort(managementRepository.getAllStories(), Comparator.comparing(Story::getSize));

        for (Story item : managementRepository.getAllStories()) {
            result.append(String.format("Work Item: %s, Size: %s%n",
                    item.getClass().getSimpleName().replace("Impl", ""),
                    item.getSize().toString()));
        }
        return result.toString();
    }

    private String ListSortByRating() {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("List of work items is empty");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Work Item Sort By Rating---").append(System.lineSeparator());


        managementRepository.getAllFeedbacks().sort(Comparator.comparing(Feedback::getRating));

        for (Feedback item : managementRepository.getAllFeedbacks()) {
            result.append(String.format("Work Item: %s, Feedback: %s%n",
                    item.getClass().getSimpleName().replace("Impl", ""),
                    item.getRating()));
        }

        return result.toString();
    }

    private String ListSortByPriority() {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("List of work items is empty");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Work Item Sort By Priority---").append(System.lineSeparator());

        managementRepository.getAssignable().stream().sorted(Comparator.comparing(Assignable::getPriority))
                .forEach(item -> {
                    result.append(String.format("Work Item: %s, Priority: %s%n",
                            item.getClass().getSimpleName().replace("Impl", ""),
                            item.getPriority().toString()));
                });


        return result.toString();
    }

    private String ListSortBySeverity() {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("List of work items is empty");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Work Item Sort By Severity---").append(System.lineSeparator());

        managementRepository.getAllBugs().sort(Comparator.comparing(Bug::getSeverity));

        for (Bug item : managementRepository.getAllBugs()) {
            result.append(String.format("Work Item: %s, Severity: %s%n",
                    item.getClass().getSimpleName().replace("Impl", ""),
                    item.getSeverity().toString()));
        }
        return result.toString();
    }

    private String ListSortByTitle() {
        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("List of work items is empty");
        }

        StringBuilder result = new StringBuilder();

        result.append("---List Work Item Sort By Title---").append(System.lineSeparator());

        List<WorkItems> items = new ArrayList<>(managementRepository.getAllWorkItems());

        Collections.sort(items, Comparator.comparing(WorkItems::getTitle));

        for (WorkItems item : items) {
            result.append(String.format("Work Item:%s, Title:%s%n",
                    item.getClass().getSimpleName().replace("Impl", ""),
                    item.getTitle()));
        }
        return result.toString();
    }
}
