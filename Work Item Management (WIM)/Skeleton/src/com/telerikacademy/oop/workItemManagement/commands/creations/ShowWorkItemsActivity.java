package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.List;

public class ShowWorkItemsActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementRepository managementRepository;

    public ShowWorkItemsActivity(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String workItemID = parameters.get(0);

        if (!managementRepository.getAllWorkItems().toString().contains(workItemID)) {
            throw new IllegalArgumentException(String.format(
                    Constants.WORK_ITEM_NOT_FOUND_ERROR_MESSAGE, workItemID));
        }

        StringBuilder builder = new StringBuilder(String.format("---History of work item with ID: %s---%n", workItemID));

        for (WorkItems items : managementRepository.getAllWorkItems()) {
            if (items.getID().equals(workItemID)) {
                for (EventLogImpl eventLogImpl : items.getHistory()) {
                    builder.append(String.format("%s%n",
                            eventLogImpl.viewInfo()));
                }
            }
        }
        return builder.toString();
    }
}
