package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;

import java.util.List;

public class ShowAllBoards implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementRepository managementRepository;

    public ShowAllBoards(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String teamToShowCatalog = parameters.get(0);
        return showBoardsCatalog(teamToShowCatalog);
    }


    private String showBoardsCatalog(String teamName) {
        if (!managementRepository.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(
                    String.format(Constants.TEAM_NOT_FOUND_ERROR_MESSAGE, teamName));
        }
        return managementRepository.getTeams().get(teamName).catalogBoards();
    }
}
