package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;

import java.util.List;

public class CreateTeam implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementFactory managementFactory;
    private final ManagementRepository managementRepository;

    public CreateTeam(ManagementFactory managementFactory, ManagementRepository managementRepository) {
        this.managementFactory = managementFactory;
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        String teamName = parameters.get(0);
        return createTeam(teamName);
    }

    private String createTeam(String name) {
        if (managementRepository.getTeams().containsKey(name)) {
            throw new IllegalArgumentException(String.format(Constants.TEAM_EXISTS_ERROR_MESSAGE, name));
        }
        Team team = managementFactory.createTeam(name);
        managementRepository.addTeam(name, team);

        return String.format(Constants.TEAM_CREATED_SUCCESS_MESSAGE, name);
    }

}
