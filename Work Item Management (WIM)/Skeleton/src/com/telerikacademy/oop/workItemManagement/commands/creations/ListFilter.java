package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Assignable;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.List;

public class ListFilter implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ListFilter(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String commandType = parameters.get(0);
        String commandParameter = parameters.get(1);

        return WorkItemsFilter(commandType, commandParameter);
    }

    private String WorkItemsFilter(String command, String parameter) {
        switch (command.toLowerCase()) {
            case "assignee":
                return ListFilterByAssignee(parameter);
            case "status":
                return ListFilterByStatus(parameter);
            case "statusandassignee":
                return ListFilterByAssigneeAndStatus(parameter);
            default:
                throw new IllegalArgumentException("Invalid Argument. Try again!");
        }
    }

    private String ListFilterByAssignee(String assignee) {
        if (managementRepository.getAssignable().size() == 0) {
            throw new IllegalArgumentException("There are no registered assignable items in list.");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Filter by Assignee---")
                .append(System.lineSeparator());


        for (Assignable assignable : managementRepository.getAssignable()) {
            if (assignable.getAssignable().getName().equals(assignee)) {
                result.append(String.format("WorkItem: %s, Assigned to: %s%n",
                        assignable.getClass().getSimpleName().replace("Impl", ""),
                        assignable.getAssignable().getName()));
            }
        }

        return result.toString();
    }

    private String ListFilterByStatus(String status) {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("There are no registered work items in list.");
        }

        StringBuilder result = new StringBuilder();
        result.append(String.format("---List Filter By Status %S---", status)).append(System.lineSeparator());

        for (WorkItems workItem : managementRepository.getAllWorkItems()) {
            if (workItem.getStatus().equalsIgnoreCase(status)) {
                result.append(String.format("WorkItem: %s, ID: %s, Status %s.%n",
                        workItem.getClass().getSimpleName().replace("Impl", ""),
                        workItem.getID(),
                        workItem.getStatus()));
            }
        }
        return result.toString();
    }

    private String ListFilterByAssigneeAndStatus(String status) {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("There are no registered work items in list.");
        }

        StringBuilder result = new StringBuilder();
        result.append("---List Filter By Status And Assignee---").append(System.lineSeparator());

        for (Assignable assignable1 : managementRepository.getAssignable()) {
            if (assignable1.getStatus().equalsIgnoreCase(status)) {

                result.append(String.format("WorkItem: %s, Assigned to: %s, Status: %s%n",
                        assignable1.getClass().getSimpleName().replace("Impl", ""),
                        assignable1.getAssignable().getName(),
                        assignable1.getStatus()));
            }
        }
        return result.toString();
    }
}
