package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;

import java.util.List;

public class ChangeSeverityOfBug implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangeSeverityOfBug(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String bugID = parameters.get(0);
        String bugSeverity = parameters.get(1);


        Bug bug = managementRepository.getAllBugs()
                .stream()
                .filter(o -> o.getID().equals(bugID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.BUG_NOT_FOUND_ERROR_MESSAGE, bugID)));


        String currentSeverity = String.valueOf(bug.getSeverity());
        bug.changeSeverity(Severity.valueOf(bugSeverity.toUpperCase()));

        return String.format(Constants.SEVERITY_CHANGED_SUCCESS_MESSAGE,
                bug.getClass().getSimpleName().replace("Impl", ""),
                bugID,
                currentSeverity.toUpperCase(),
                bugSeverity.toUpperCase());
    }
}
