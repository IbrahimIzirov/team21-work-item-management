package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.ValidationHelpers;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangePriorityOfStory implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangePriorityOfStory(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String storyID = parameters.get(0);
        String storyPriority = parameters.get(1);


        Story story = managementRepository.getAllStories()
                .stream()
                .filter(o -> o.getID().equals(storyID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.STORY_NOT_FOUND_ERROR_MESSAGE, storyID)));


        String currentPriority = String.valueOf(story.getPriority());
        List<Priority> priorities = new ArrayList<>(Arrays.asList(Priority.values()));

        ValidationHelpers.checkIfPriorityIsCorrect(priorities, storyPriority);
        story.changePriority(Priority.valueOf(storyPriority.toUpperCase()));

        return String.format(Constants.PRIORITY_CHANGED_SUCCESS_MESSAGE,
                story.getClass().getSimpleName().replace("Impl", ""),
                storyID,
                currentPriority.toUpperCase(),
                storyPriority.toUpperCase());
    }

}
