package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.ValidationHelpers;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusFeedback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeStatusOfFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangeStatusOfFeedback(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String feedbackID = parameters.get(0);
        String feedbackStatus = parameters.get(1);


        Feedback feedback = managementRepository.getAllFeedbacks()
                .stream()
                .filter(o -> o.getID().equals(feedbackID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.FEEDBACK_NOT_FOUND_ERROR_MESSAGE, feedbackID)));


        String currentStatus = String.valueOf(feedback.getStatus());
        List<StatusFeedback> listStatusOfFeedback = new ArrayList<>(Arrays.asList(StatusFeedback.values()));

        ValidationHelpers.checkIfStatusOfFeedbackIsCorrect(listStatusOfFeedback, feedbackStatus);
        feedback.changeStatus(StatusFeedback.valueOf(feedbackStatus.toUpperCase()));

        return String.format(Constants.STATUS_CHANGED_SUCCESS_MESSAGE,
                feedback.getClass().getSimpleName().replace("Impl", ""),
                feedbackID,
                currentStatus.toUpperCase(),
                feedbackStatus.toUpperCase());
    }
}
