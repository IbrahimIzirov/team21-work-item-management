package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;

import java.util.List;

public class AddPersonToTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;


    private final ManagementRepository managementRepository;

    public AddPersonToTeam(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String teamToAddToName = parameters.get(0);
        String memberToBeAddedName = parameters.get(1);

        if (!managementRepository.getTeams().containsKey(teamToAddToName)) {
            throw new IllegalArgumentException(String.format(Constants.TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddToName));
        }

        if (!managementRepository.getMembers().containsKey(memberToBeAddedName)) {
            throw new IllegalArgumentException(String.format(Constants.PERSON_NOT_FOUND_ERROR_MESSAGE, memberToBeAddedName));
        }

        Team team = managementRepository.getTeams().get(teamToAddToName);
        Member member = managementRepository.getMembers().get(memberToBeAddedName);
        team.addMember(member);

        return String.format(Constants.MEMBER_ADDED_SUCCESS_MESSAGE, memberToBeAddedName, teamToAddToName);
    }
}
