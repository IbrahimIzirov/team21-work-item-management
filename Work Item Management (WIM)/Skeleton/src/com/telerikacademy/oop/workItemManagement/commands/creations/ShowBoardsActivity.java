package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;

import java.util.List;

public class ShowBoardsActivity implements Command {

    private final ManagementRepository managementRepository;

    public ShowBoardsActivity(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (managementRepository.getBoards().size() == 0) {
            throw new IllegalArgumentException("There is no registered board in this list");
        }

        StringBuilder builder = new StringBuilder("---List All Boards Activity---\n");

        for (Board board : managementRepository.getBoards().values()) {
            for (EventLogImpl eventLogImpl : board.getHistory()) {
                builder.append(String.format("%s%n",
                        eventLogImpl.viewInfo()));
            }
        }
        return builder.toString();
    }
}
