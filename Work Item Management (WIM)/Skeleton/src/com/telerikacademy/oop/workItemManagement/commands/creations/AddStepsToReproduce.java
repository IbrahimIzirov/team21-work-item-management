package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;

import java.util.List;

public class AddStepsToReproduce implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public AddStepsToReproduce(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String bugID = parameters.get(0);
        String steps = parameters.get(1);

        Bug item = managementRepository.getAllBugs()
                .stream()
                .filter(o -> o.getID().equals(bugID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(Constants.BUG_NOT_FOUND_ERROR_MESSAGE, bugID)));

        item.addStepsToReproduce(steps);

        return String.format(Constants.STEPS_TO_REPRODUCE_ADD_SUCCESS, bugID);
    }
}
