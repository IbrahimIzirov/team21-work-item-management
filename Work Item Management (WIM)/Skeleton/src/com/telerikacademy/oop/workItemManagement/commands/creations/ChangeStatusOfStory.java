package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.ValidationHelpers;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusStory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeStatusOfStory implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;


    public ChangeStatusOfStory(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String storyID = parameters.get(0);
        String storyStatus = parameters.get(1);


        Story story = managementRepository.getAllStories()
                .stream()
                .filter(o -> o.getID().equals(storyID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.STORY_NOT_FOUND_ERROR_MESSAGE, storyID)));


        String currentStatus = String.valueOf(story.getStatus());
        List<StatusStory> listStatusOfStory = new ArrayList<>(Arrays.asList(StatusStory.values()));

        ValidationHelpers.checkIfStatusOfStoryIsCorrect(listStatusOfStory, storyStatus);
        story.changeStatus(StatusStory.valueOf(storyStatus.toUpperCase()));

        return String.format(Constants.STATUS_CHANGED_SUCCESS_MESSAGE,
                story.getClass().getSimpleName().replace("Impl", ""),
                storyID,
                currentStatus.toUpperCase(),
                storyStatus.toUpperCase());
    }
}
