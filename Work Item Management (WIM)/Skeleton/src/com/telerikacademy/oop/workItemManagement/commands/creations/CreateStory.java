package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;

import java.util.List;

public class CreateStory implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 7;


    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    public CreateStory(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String boardToAddToName = parameters.get(0);
        String storyID = parameters.get(1);
        String storyTitle = parameters.get(2);
        String storyDescription = parameters.get(3);
        String storyPriority = parameters.get(4);
        String storySize = parameters.get(5);
        Member storyAssignee = managementRepository.getMembers().get(parameters.get(6));

        if (!managementRepository.getBoards().containsKey(boardToAddToName)) {
            throw new IllegalArgumentException(String.format(Constants.BOARD_NOT_FOUND_ERROR_MESSAGE, boardToAddToName));
        }

        Board board = managementRepository.getBoards().get(boardToAddToName);
        Story story = managementFactory.createStory(storyID, storyTitle,
                storyDescription, storyPriority, storySize, storyAssignee);

        managementRepository.addStory(story);

        board.addWorkItems(story);

        return String.format(Constants.STORY_ADDED_SUCCESS_MESSAGE, storyID, boardToAddToName);
    }
}
