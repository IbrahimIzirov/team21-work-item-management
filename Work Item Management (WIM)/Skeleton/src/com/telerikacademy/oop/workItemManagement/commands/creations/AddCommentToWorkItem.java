package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.List;

public class AddCommentToWorkItem implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public AddCommentToWorkItem(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String commentToWorkItem = parameters.get(0);
        String commentToBeAdded = parameters.get(1);


        WorkItems item = managementRepository.getAllWorkItems()
                .stream()
                .filter(o -> o.getID().equals(commentToWorkItem))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(Constants.WORK_ITEM_NOT_FOUND_ERROR_MESSAGE, commentToWorkItem)));

        item.addComment(commentToBeAdded);

        return String.format(Constants.COMMENT_ADDED_SUCCESS_MESSAGE, commentToBeAdded, commentToWorkItem);
    }
}
