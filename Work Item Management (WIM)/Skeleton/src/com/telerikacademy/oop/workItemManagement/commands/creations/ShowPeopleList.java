package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;

import java.util.List;

public class ShowPeopleList implements Command {

    private final ManagementRepository managementRepository;

    public ShowPeopleList(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (managementRepository.getMembers().size() == 0) {
            throw new IllegalArgumentException("There are no registered person in list.");
        }
        return showAllUsers();
    }

    private String showAllUsers() {

        StringBuilder builder = new StringBuilder();
        builder.append("--Members--").append(System.lineSeparator());
        int counter = 1;
        for (String member : managementRepository.getMembers().keySet()) {
            builder.append(String.format("%d. %s", counter, member))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }
}
