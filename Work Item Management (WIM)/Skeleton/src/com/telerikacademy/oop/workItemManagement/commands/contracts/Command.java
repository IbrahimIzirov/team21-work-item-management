package com.telerikacademy.oop.workItemManagement.commands.contracts;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);

}
