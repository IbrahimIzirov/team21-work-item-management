package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;

import java.util.List;

public class CreatePerson implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementFactory managementFactory;
    private final ManagementRepository managementRepository;

    public CreatePerson(ManagementFactory managementFactory, ManagementRepository managementRepository) {
        this.managementFactory = managementFactory;
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        String memberName = parameters.get(0);
        return createMember(memberName);
    }

    private String createMember(String name) {
        if (managementRepository.getMembers().containsKey(name)) {
            throw new IllegalArgumentException(String.format(Constants.PERSON_EXISTS_ERROR_MESSAGE, name));
        }
        Member member = managementFactory.createMember(name);
        managementRepository.addMember(name, member);

        return String.format(Constants.PERSON_CREATED_SUCCESS_MESSAGE, name);
    }
}
