package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;

import java.util.List;

public class ChangeRatingOfFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public ChangeRatingOfFeedback(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String feedbackID = parameters.get(0);
        int feedbackRating = Integer.parseInt(parameters.get(1));

        if (feedbackRating < 0) {
            throw new IllegalArgumentException("Rating cannot be negative");
        }


        Feedback feedback = managementRepository.getAllFeedbacks()
                .stream()
                .filter(o -> o.getID().equals(feedbackID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.FEEDBACK_NOT_FOUND_ERROR_MESSAGE, feedbackID)));

        int currentRating = feedback.getRating();
        feedback.changeRating(feedbackRating);

        return String.format(Constants.RATING_CHANGED_SUCCESS_MESSAGE,
                feedback.getClass().getSimpleName().replace("Impl", ""),
                feedbackID,
                currentRating,
                feedbackRating);
    }

}
