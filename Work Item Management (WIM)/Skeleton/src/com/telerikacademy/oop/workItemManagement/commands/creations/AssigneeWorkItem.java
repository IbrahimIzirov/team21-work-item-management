package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.List;

public class AssigneeWorkItem implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    public AssigneeWorkItem(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String memberToBeAssignee = parameters.get(0);
        String workItemAssigneeToPerson = parameters.get(1);


        if (managementRepository.getMembers().values()
                .stream()
                .noneMatch(o -> o.getName().equals(memberToBeAssignee))) {
            throw new IllegalArgumentException(String.format(
                    Constants.PERSON_NOT_FOUND_ERROR_MESSAGE, memberToBeAssignee));
        }


        WorkItems item = managementRepository.getAllWorkItems()
                .stream()
                .filter(o -> o.getID().equals(workItemAssigneeToPerson))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        Constants.WORK_ITEM_NOT_FOUND_ERROR_MESSAGE, workItemAssigneeToPerson)));

        Member member = managementRepository.getMembers().get(memberToBeAssignee);

        member.addWorkItems(item);

        return String.format(Constants.WORK_ITEM_ASSIGNEE_SUCCESS_MESSAGE, workItemAssigneeToPerson, memberToBeAssignee);
    }


}
