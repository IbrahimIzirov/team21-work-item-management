package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;

import java.util.List;

public class CreateBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;


    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    public CreateBoard(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String teamToAddToName = parameters.get(0);
        String boardToBeAddedName = parameters.get(1);

        if (!managementRepository.getTeams().containsKey(teamToAddToName)) {
            throw new IllegalArgumentException(String.format(
                    Constants.TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddToName));
        }

        if (managementRepository.getTeams().get(teamToAddToName).getAllBoards().toString().contains(boardToBeAddedName)) {
            throw new IllegalArgumentException(String.format(
                    Constants.BOARD_EXISTS_ERROR_MESSAGE, boardToBeAddedName, teamToAddToName));
        }

        Team team = managementRepository.getTeams().get(teamToAddToName);
        Board board = managementFactory.createBoard(boardToBeAddedName);

        managementRepository.addBoard(boardToBeAddedName, board);

        team.addBoard(board);

        return String.format(Constants.BOARD_ADDED_SUCCESS_MESSAGE, boardToBeAddedName, teamToAddToName);
    }
}
