package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.EventLogImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;

import java.util.List;

public class ShowTeamsActivity implements Command {

    private final ManagementRepository managementRepository;

    public ShowTeamsActivity(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        if (managementRepository.getTeams().size() == 0) {
            throw new IllegalArgumentException("There is no registered teams in this list");
        }

        StringBuilder builder = new StringBuilder(String.format("---List All Teams Activity---%n"));

        for (Team team : managementRepository.getTeams().values()) {
            for (EventLogImpl eventLogImpl : team.getHistory()) {
                builder.append(String.format("%s%n",
                        eventLogImpl.viewInfo()));
            }
        }

        return builder.toString();
    }
}
