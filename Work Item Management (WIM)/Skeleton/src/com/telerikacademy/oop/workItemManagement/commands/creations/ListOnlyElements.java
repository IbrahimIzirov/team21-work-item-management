package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.workItemManagement.commands.Constants.JOIN_DELIMITER;

public class ListOnlyElements implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementRepository managementRepository;

    public ListOnlyElements(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String commandType = parameters.get(0);

        return String.join(JOIN_DELIMITER + System.lineSeparator(), WorkItemsView(commandType));
    }

    private List<String> WorkItemsView(String commandType) {
        switch (commandType.toLowerCase()) {
            case "bugs":
                return ListViewBugs();
            case "stories":
                return ListViewStories();
            case "feedback":
                return ListViewFeedback();
            case "allworkitems":
                return ListViewAll();
            default:
                throw new IllegalArgumentException("Invalid Argument. Try again!");
        }
    }

    private List<String> ListViewAll() {

        if (managementRepository.getAllWorkItems().size() == 0) {
            throw new IllegalArgumentException("There are no registered work items in list.");
        }

        List<String> allWorkItemsList = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        builder.append("--All Work Items--").append(System.lineSeparator());
        int counter = 1;

        for (WorkItems workItem : managementRepository.getAllWorkItems()) {
            builder.append(String.format("%d. %s - %s", counter++
                    , workItem.getID()
                    , workItem.getClass().getSimpleName().replace("Impl", "")))
                    .append(System.lineSeparator());
        }
        allWorkItemsList.add(builder.toString().trim());

        return allWorkItemsList;
    }

    private List<String> ListViewFeedback() {

        if (managementRepository.getAllFeedbacks().size() == 0) {
            throw new IllegalArgumentException("There are no registered feedback in list.");
        }

        List<String> allStoriesList = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        builder.append("---List Feedback Only---").append(System.lineSeparator());
        int counter = 1;

        for (Feedback feedback : managementRepository.getAllFeedbacks()) {

            builder.append(String.format("%d. %s%n", counter++
                    , feedback.getID()));

        }

        allStoriesList.add(builder.toString().trim());

        return allStoriesList;
    }

    private List<String> ListViewStories() {

        if (managementRepository.getAllStories().size() == 0) {
            throw new IllegalArgumentException("There are no registered stories in list.");
        }

        List<String> allStoriesList = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        builder.append("---List Only Stories---").append(System.lineSeparator());
        int counter = 1;

        for (Story story : managementRepository.getAllStories()) {

            builder.append(String.format("%d. %s%n", counter++
                    , story.getID()));

        }
        allStoriesList.add(builder.toString().trim());

        return allStoriesList;

    }

    private List<String> ListViewBugs() {

        if (managementRepository.getAllBugs().size() == 0) {
            throw new IllegalArgumentException("There are no registered bugs in list.");
        }

        List<String> allBugsList = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        builder.append("---List Bugs Only---").append(System.lineSeparator());
        int counter = 1;

        for (Bug bugs : managementRepository.getAllBugs()) {
            builder.append(String.format("%d. %s%n", counter++
                    , bugs.getID()));
        }

        allBugsList.add(builder.toString().trim());

        return allBugsList;
    }
}
