package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;

import java.util.List;

public class CreateBugs implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 7;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    public CreateBugs(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(Constants.INVALID_NUMBER_OF_ARGUMENTS);
        }

        String boardToAddToName = parameters.get(0);
        String bugID = parameters.get(1);
        String bugTitle = parameters.get(2);
        String bugDescription = parameters.get(3);
        String bugPriority = parameters.get(4);
        String bugSeverity = parameters.get(5);
        Member bugAssignee = managementRepository.getMembers().get(parameters.get(6));

        if (!managementRepository.getBoards().containsKey(boardToAddToName)) {
            throw new IllegalArgumentException(String.format(Constants.BOARD_NOT_FOUND_ERROR_MESSAGE, boardToAddToName));
        }

        if (managementRepository.getBoards().get(boardToAddToName).getWorkItem().toString().contains(bugID)) {
            throw new IllegalArgumentException(String.format(
                    Constants.BUG_ALREADY_EXIST_MESSAGE, bugID));
        }

        Board board = managementRepository.getBoards().get(boardToAddToName);
        Bug bug = managementFactory.createBugs(bugID, bugTitle,
                bugDescription, bugPriority, bugSeverity, bugAssignee);

        managementRepository.addBugs(bug);

        board.addWorkItems(bug);
        bugAssignee.addWorkItems(bug);

        return String.format(Constants.BUG_ADDED_SUCCESS_MESSAGE, bugID, boardToAddToName);
    }
}
