package com.telerikacademy.oop.workItemManagement.commands.enums;

public enum CommandType {
    CREATEPERSON,
    SHOWALLPEOPLE,
    SHOWPERSONSACTIVITY,
    CREATETEAM,
    SHOWALLTEAMS,
    SHOWTEAMSACTIVITY,
    ADDPERSONTOTEAM,
    SHOWALLTEAMMEMBERS,
    CREATEBOARD,
    SHOWALLBOARDS,
    SHOWBOARDSACTIVITY,
    CREATEBUGINBOARD,
    CREATESTORYINBOARD,
    CREATEFEEDBACKINBOARD,
    CHANGEPRIORITYOFBUG,
    CHANGESEVERITYOFBUG,
    CHANGESTATUSOFBUG,
    CHANGEPRIORITYOFSTORY,
    CHANGESIZEOFSTORY,
    CHANGESTATUSOFSTORY,
    CHANGERATINGOFFEEDBACK,
    CHANGESTATUSOFFEEDBACK,
    ASSIGNEEWORKITEM,
    UNASSIGNEDWORKITEM,
    ADDCOMMENTTOWORKITEM,
    SHOWWORKITEMACTIVITY,
    ADDSTEPSTOREPRODUCE,
    GETSTEPSTOREPRODUCE,
    LISTSORT,
    LISTFILTER,
    LISTONLY;
}
