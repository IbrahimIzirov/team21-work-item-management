package com.telerikacademy.oop.workItemManagement.commands.creations;

import com.telerikacademy.oop.workItemManagement.commands.Constants;
import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;

import java.util.List;

public class CreateFeedback implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;


    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    public CreateFeedback(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    Constants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        String boardToAddToName = parameters.get(0);
        String feedbackID = parameters.get(1);
        String feedbackTitle = parameters.get(2);
        String feedbackDescription = parameters.get(3);
        int feedbackRating = Integer.parseInt(parameters.get(4));

        if (!managementRepository.getBoards().containsKey(boardToAddToName)) {
            throw new IllegalArgumentException(String.format(Constants.BOARD_NOT_FOUND_ERROR_MESSAGE, boardToAddToName));
        }

        Board board = managementRepository.getBoards().get(boardToAddToName);
        Feedback feedback = managementFactory.createFeedback(feedbackID, feedbackTitle,
                feedbackDescription, feedbackRating);

        managementRepository.addFeedback(feedback);

        board.addWorkItems(feedback);

        return String.format(Constants.FEEDBACK_ADDED_SUCCESS_MESSAGE, feedbackID, boardToAddToName);
    }
}
