package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowPeopleList;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowPeopleList_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private Member testMember;
    private Member testMember2;
    private List<String> items;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowPeopleList(managementRepository);
        testMember = new MemberImpl("Kalin");
        testMember2 = new MemberImpl("Pavel");
        items = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_listBoardIsEmpty() {
        items = Collections.singletonList("");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        items = Collections.singletonList("");
        managementRepository.addMember("Kalin", testMember);
        managementRepository.addMember("Pavel", testMember2);

        testCommand.execute(items);

        Assertions.assertSame(2, managementRepository.getMembers().size());
    }
}
