package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.CreateBoard;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.BoardImpl;
import com.telerikacademy.oop.workItemManagement.models.TeamImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CreateBoard_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;


    @BeforeEach
    public void before() {
        ManagementFactory managementFactory = new ManagementFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new CreateBoard(managementRepository, managementFactory);
    }

    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("WorldWide", "Board4", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_boardAlreadyExist() {
        BoardImpl board = new BoardImpl("Board4");
        TeamImpl team = new TeamImpl("WorldWide");
        managementRepository.addBoard("Board4", board);
        managementRepository.addTeam("WorldWide", team);
        team.addBoard(board);
        parameters = Arrays.asList("WorldWide", "Board4");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_teamNotFound() {
        parameters = Arrays.asList("WorldWide", "Board4");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnBoard_when_validParameter() {
        TeamImpl team = new TeamImpl("WorldWide");
        managementRepository.addTeam("WorldWide", team);
        parameters = Arrays.asList("WorldWide", "Board4");
        testCommand.execute(parameters);

        Assertions.assertTrue(team.getAllBoards()
                .stream()
                .anyMatch(board -> board.getName().equalsIgnoreCase("Board4")));
    }
}
