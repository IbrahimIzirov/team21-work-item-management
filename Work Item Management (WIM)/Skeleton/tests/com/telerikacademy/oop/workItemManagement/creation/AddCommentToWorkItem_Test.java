package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.AddCommentToWorkItem;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddCommentToWorkItem_Test {


    private ManagementRepository managementRepository;
    private Command testCommand;
    private List<String> parameters;
    private BugImpl testBug;
    private Member testMember;


    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        parameters = new ArrayList<>();
        testCommand = new AddCommentToWorkItem(managementRepository);
        testMember = new MemberImpl("Stefan");
        testBug = new BugImpl("ID0007", "System Bug", "Test Add Comment to WorkItem", Priority.HIGH, Severity.CRITICAL, testMember);
    }

    @Test
    public void execute_should_throwException_when_incorrectNumberOfArguments() {
        parameters = Arrays.asList("ID0007", "Hello_world!!!!", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


    @Test
    public void execute_should_returnComment_when_validParameter() {
        managementRepository.addBugs(testBug);
        parameters = Arrays.asList("ID0007", "Hello_world!!!!");
        testCommand.execute(parameters);

        Assertions.assertTrue(testBug.getComment().contains("Hello_world!!!!"));
    }

    @Test
    public void execute_should_throwException_when_itemIsNotFound() {
        parameters = Arrays.asList("ID0007", "Hello_world!!!!");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

}
