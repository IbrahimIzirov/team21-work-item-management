package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ListSort;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.FeedbackImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.StoryImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ListSort_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private List<String> parameters;
    private Member testMember;

    @BeforeEach
    private void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ListSort(managementRepository);
        parameters = new ArrayList<>();
        testMember = new MemberImpl("Pavel");
    }

    @Test
    public void Execute_Should_ThrowException_When_InvalidNumberOfArguments() {
        parameters.add("Argument1");
        parameters.add("Argument2");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListSortByTitleIsEmpty() {
        parameters.add("title");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListSortByRatingIsEmpty() {
        parameters.add("rating");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListSortByPriorityIsEmpty() {
        parameters.add("priority");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListSortBySeverityIsEmpty() {
        parameters.add("severity");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListSortBySizeIsEmpty() {
        parameters.add("size");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_InvalidArgument() {
        parameters.add("SomethingElse");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_SortByTitle_When_ValidArgument() {
        managementRepository.addFeedback(new FeedbackImpl("Feedback1",
                "LongTitleOfFeedback",
                "SomethingElseOrDescription",
                6));
        parameters.add("title");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_SortByPriority_When_ValidArgument() {
        managementRepository.addBugs(new BugImpl("Bug1",
                "LongTitleOfBug",
                "SomethingElseOrDescription",
                Priority.LOW,
                Severity.CRITICAL,
                testMember));
        parameters.add("priority");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_SortBySeverity_When_ValidArgument() {
        managementRepository.addBugs(new BugImpl("Bug1",
                "LongTitleOfBug",
                "SomethingElseOrDescription",
                Priority.LOW,
                Severity.CRITICAL,
                testMember));
        parameters.add("severity");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_SortBySize_When_ValidArgument() {
        managementRepository.addStory(new StoryImpl("Story1",
                "LongTitleOfStory",
                "SomethingElseOrDescription",
                Priority.LOW,
                Size.LARGE,
                testMember));
        parameters.add("size");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_SortByRating_When_ValidArgument() {
        managementRepository.addFeedback(new FeedbackImpl("Feedback1",
                "LongTitleOfFeedback",
                "SomethingElseOrDescription",
                4));
        parameters.add("rating");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }
}
