package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.CreateBugs;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.BoardImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CreateBugs_Test {
    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        ManagementFactory managementFactory = new ManagementFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new CreateBugs(managementRepository, managementFactory);
    }


    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("Board4", "IDJFK", "Mo6veBugTitle22",
                "Reposoribug", "Medium", "Major", "Misho", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_boardNotFound() {
        parameters = Arrays.asList("Board2", "IDJFK", "Mo6veBugTitle22", "Reposoribug", "Medium", "Major", "Misho");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnBug_when_validParameter() {
        BoardImpl board = new BoardImpl("Board4");
        Member member = new MemberImpl("Misho");
        managementRepository.addBoard("Board4", board);
        managementRepository.addMember("Misho", member);

        parameters = Arrays.asList("Board4", "IDJFK", "Mo6veBugTitle22", "Reposoribug", "Medium", "Major", "Misho");
        testCommand.execute(parameters);

        Assertions.assertTrue(board.getWorkItem()
                .stream()
                .anyMatch(item -> item.getID().equalsIgnoreCase("IDJFK")));
    }
}
