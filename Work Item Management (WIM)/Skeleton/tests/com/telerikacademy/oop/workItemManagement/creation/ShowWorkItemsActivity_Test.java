package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowWorkItemsActivity;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ShowWorkItemsActivity_Test {

    private ManagementRepository managementRepository;
    private Command testCommand;
    private Bug testBug;
    private List<String> parameter;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowWorkItemsActivity(managementRepository);
        Member testMember = new MemberImpl("Jack Sparrow");
        testBug = new BugImpl("ID2013", "TestTitleBug", "Mo6veBugTitle22", Priority.HIGH, Severity.MAJOR, testMember);
        parameter = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_invalidNumberParameter() {
        parameter = Arrays.asList("ID2013", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameter));
    }

    @Test
    public void execute_should_throwException_when_workItemNotFound() {
        parameter = Collections.singletonList("ID2013");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameter));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        parameter = Collections.singletonList("ID2013");
        managementRepository.addBugs(testBug);

        testCommand.execute(parameter);

        Assertions.assertTrue(managementRepository.getAllWorkItems()
                .stream()
                .anyMatch(workItems -> workItems.getID().equalsIgnoreCase("ID2013"))
        );
    }
}
