package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.AssigneeWorkItem;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class AssigneeWorkItem_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;
    private Member testMember;
    private Bug testWorkItem;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new AssigneeWorkItem(managementRepository);
        testMember = new MemberImpl("Plamen");
        testWorkItem = new BugImpl("IDZ04", "System Bug", "Test Add Comment to WorkItem", Priority.LOW, Severity.MINOR, testMember);
    }


    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("Plamen", "IDZ04", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_memberNotFound() {
        parameters = Arrays.asList("Ivelin", "IDZ04");
        managementRepository.addMember("Plamen", testMember);
        managementRepository.addBugs(testWorkItem);

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_itemNotFound() {
        parameters = Arrays.asList("Plamen", "IDP01");
        managementRepository.addMember("Plamen", testMember);
        managementRepository.addBugs(testWorkItem);

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnWorkItem_when_validParameter() {
        managementRepository.addMember("Plamen", testMember);
        managementRepository.addBugs(testWorkItem);
        parameters = Arrays.asList("Plamen", "IDZ04");
        testCommand.execute(parameters);

        Assertions.assertTrue(testMember.getWorkItem()
                .stream()
                .anyMatch(item -> item.getID().equalsIgnoreCase("IDZ04")));
    }

}
