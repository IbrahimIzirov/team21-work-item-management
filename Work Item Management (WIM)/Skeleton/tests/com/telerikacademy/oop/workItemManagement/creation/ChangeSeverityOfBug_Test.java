package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ChangeSeverityOfBug;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Bug;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ChangeSeverityOfBug_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Bug testBug;
    private Command testCommand;
    private Member testMember;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ChangeSeverityOfBug(managementRepository);
        testMember = new MemberImpl("Gosho");
        testBug = new BugImpl("ID2000", "MoveFeedbackTitle",
                "Reposoribug", Priority.HIGH, Severity.CRITICAL, testMember);

    }

    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("ID2000", "MoveFeedbackTitle", "ReposoritoryBug");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_SeverityIsDifferentFromEnum() {
        parameters = Arrays.asList("ID2000", "MoveFeedbackTitle",
                "ReposoritoryBug", String.valueOf(Priority.HIGH),
                "DifferentSeverity",
                String.valueOf(testMember));

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_bugNotFound() {
        managementRepository.addBugs(testBug);

        parameters = Arrays.asList("ID2021", "MoveFeedbackTitle");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnNewSeverity_when_validParameter() {
        managementRepository.addBugs(testBug);

        parameters = Arrays.asList("ID2000", "Critical");

        testCommand.execute(parameters);

        Assertions.assertSame("MoveFeedbackTitle", testBug.getTitle());
    }
}
