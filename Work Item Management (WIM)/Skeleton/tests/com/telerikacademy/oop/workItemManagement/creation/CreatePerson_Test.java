package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.CreatePerson;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CreatePerson_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        ManagementFactory managementFactory = new ManagementFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new CreatePerson(managementFactory, managementRepository);
    }


    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("Jaro13", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_personExistr() {
        Member member = new MemberImpl("Jaro13");
        managementRepository.addMember("Jaro13", member);
        parameters = Arrays.asList("Jaro13");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnPerson_when_validParameter() {
        parameters = Arrays.asList("Jaro13");
        testCommand.execute(parameters);

        Assertions.assertTrue(managementRepository.getMembers().values()
                .stream()
                .anyMatch(member -> member.getName().equalsIgnoreCase("Jaro13")));
    }
}
