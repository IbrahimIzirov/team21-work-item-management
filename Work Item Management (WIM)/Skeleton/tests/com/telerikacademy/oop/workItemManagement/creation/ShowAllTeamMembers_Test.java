package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowAllTeamMembers;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.TeamImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ShowAllTeamMembers_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private Team testTeam;
    private Member testMember;
    private Member testMember2;
    private List<String> items;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowAllTeamMembers(managementRepository);
        testTeam = new TeamImpl("Alpha");
        testMember = new MemberImpl("Donald");
        testMember2 = new MemberImpl("Hristo");
        items = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_InvalidNumberOfParameter() {
        items = Arrays.asList("Alpha", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_throwException_when_teamNotFound() {
        items = Collections.singletonList("Alpha");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        items = Collections.singletonList("Alpha");
        managementRepository.addTeam("Alpha", testTeam);
        testTeam.addMember(testMember);
        testTeam.addMember(testMember2);
        testCommand.execute(items);

        Assertions.assertSame(2, testTeam.getMembers().size());
    }

}
