package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ChangeStatusOfStory;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.StoryImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Story;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusStory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ChangeStatusOfStory_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Story testStory;
    private Command testCommand;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ChangeStatusOfStory(managementRepository);
        Member testMember = new MemberImpl("Gosho");
        testStory = new StoryImpl("ID2000", "MoveFeedbackTitle",
                "Reposoribug", Priority.HIGH, Size.LARGE, testMember);

    }

    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("ID2000", "MoveFeedbackTitle", "ReposoritoryBug");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_StatusIsDifferentFromEnum() {

        Assertions.assertEquals(String.valueOf(StatusStory.NOTDONE), testStory.getStatus());
    }

    @Test
    public void execute_should_throwException_when_StoryNotFound() {

        managementRepository.addStory(testStory);

        parameters = Arrays.asList("ID2021", "MoveFeedbackTitle");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnNewStatus_when_validParameter() {

        testStory.changeStatus(StatusStory.DONE);

        Assertions.assertSame("Done", testStory.getStatus());
    }
}
