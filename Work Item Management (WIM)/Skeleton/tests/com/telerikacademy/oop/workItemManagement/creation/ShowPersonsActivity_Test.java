package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowPersonsActivity;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowPersonsActivity_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private Member testMember;
    private List<String> items;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowPersonsActivity(managementRepository);
        testMember = new MemberImpl("Toni88");
        items = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_InvalidNumberParameter() {
        items = Collections.singletonList("InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_throwException_when_listMemberIsEmpty() {
        items = Collections.emptyList();

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        items = Collections.emptyList();
        managementRepository.addMember("Toni88", testMember);

        testCommand.execute(items);

        Assertions.assertSame(1, managementRepository.getMembers().size());
    }

}
