package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.BoardImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CreateFeedback_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        ManagementFactory managementFactory = new ManagementFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new com.telerikacademy.oop.workItemManagement.commands.creations.CreateFeedback(managementRepository, managementFactory);
    }


    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("Board3", "ID001", "MoveFeedbackTitle", "Reposoribug", "6", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_boardNotFound() {
        parameters = Arrays.asList("Board222", "ID001", "MoveFeedbackTitle", "Reposoribug", "8");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnBug_when_validParameter() {
        BoardImpl board = new BoardImpl("Board4");
        managementRepository.addBoard("Board4", board);

        parameters = Arrays.asList("Board4", "ID001", "Mo6veBugTitle22", "Reposoribug", "13");
        testCommand.execute(parameters);

        Assertions.assertTrue(board.getWorkItem()
                .stream()
                .anyMatch(item -> item.getID().equalsIgnoreCase("ID001")));
    }
}
