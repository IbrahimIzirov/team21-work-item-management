package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ChangeRatingOfFeedback;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.FeedbackImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ChangeRatingOfFeedback_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Feedback testFeedback;
    private Command testCommand;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ChangeRatingOfFeedback(managementRepository);
        testFeedback = new FeedbackImpl("ID2000", "MoveFeedbackTitle", "Reposoribug", 6);

    }

    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("ID2000", "5", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_ratingIsSmallThanZero() {
        parameters = Arrays.asList("ID2000", "-2");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_feedbackNotFound() {
        managementRepository.addFeedback(testFeedback);
        parameters = Arrays.asList("ID2021", "7");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnNewRating_when_validParameter() {
        managementRepository.addFeedback(testFeedback);
        parameters = Arrays.asList("ID2000", "7");
        testCommand.execute(parameters);

        Assertions.assertSame(7, testFeedback.getRating());
    }
}
