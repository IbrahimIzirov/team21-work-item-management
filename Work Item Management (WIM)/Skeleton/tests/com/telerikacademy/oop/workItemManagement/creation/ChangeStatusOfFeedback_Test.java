package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ChangeStatusOfFeedback;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.FeedbackImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Feedback;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusFeedback;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class ChangeStatusOfFeedback_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Feedback testFeedback;
    private Command testCommand;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ChangeStatusOfFeedback(managementRepository);
        testFeedback = new FeedbackImpl("ID2000", "MoveFeedbackTitle", "Reposoribug", 6);

    }

    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("ID2000", "5", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_RatingIsDifferentFromEnum() {

        parameters = Arrays.asList("ID2000", "MoveFeedbackTitle", "Reposoribug", "rating");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_feedbackNotFound() {
        managementRepository.addFeedback(testFeedback);

        parameters = Arrays.asList("ID2021", "7");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void ChangeStatus_should_returnNewStatus_when_validParameter() {

        testFeedback.changeStatus(StatusFeedback.SCHEDULED);

        Assertions.assertSame("Scheduled", testFeedback.getStatus());
    }
}
