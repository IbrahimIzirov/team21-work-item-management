package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowTeamsActivity;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.TeamImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowTeamsActivity_Test {

    private ManagementRepository managementRepository;
    private Command testCommand;
    private Team testTeam;
    private Member testMember;
    private List<String> items;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowTeamsActivity(managementRepository);
        testTeam = new TeamImpl("TreeFools");
        testMember = new MemberImpl("Donyo Donev");
        items = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_listMemberIsEmpty() {
        items = Collections.emptyList();

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        items = Collections.emptyList();
        managementRepository.addTeam("TreeFools", testTeam);
        testTeam.addMember(testMember);

        testCommand.execute(items);

        Assertions.assertSame(1, managementRepository.getTeams().size());
    }

}
