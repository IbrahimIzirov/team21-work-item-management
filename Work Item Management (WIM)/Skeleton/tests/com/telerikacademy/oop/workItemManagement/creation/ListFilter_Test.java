package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ListFilter;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.StoryImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ListFilter_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private List<String> parameters;
    private Member testMember;

    @BeforeEach
    private void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ListFilter(managementRepository);
        parameters = new ArrayList<>();
        testMember = new MemberImpl("Pavel");
    }

    @Test
    public void Execute_Should_ThrowException_When_InvalidNumberOfArguments() {
        parameters.add("Argument1");
        parameters.add("Argument2");
        parameters.add("Argument3");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListFilterByAssigneeIsEmpty() {
        parameters.add("assignee");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListFilterByStatusIsEmpty() {
        parameters.add("status");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListFilterByStatusAndAssigneeIsEmpty() {
        parameters.add("statusandassignee");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_InvalidArgument() {
        parameters.add("SomethingElse");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_FilterByAssignee_When_ValidArgument() {
        managementRepository.addBugs(new BugImpl("Bug1",
                "LongTitleOfBug",
                "SomethingElseOrDescription",
                Priority.LOW,
                Severity.CRITICAL,
                testMember));
        parameters.add(0, "assignee");
        parameters.add(1, "Pavel");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_FilterByStatus_When_ValidArgument() {
        managementRepository.addStory(new StoryImpl("Story1",
                "LongTitleOfStory",
                "SomethingElseOrDescription",
                Priority.LOW,
                Size.LARGE,
                testMember));
        parameters.add(0, "status");
        parameters.add(1, "Pavel");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_FilterByStatusAndAssignee_When_ValidArgument() {
        managementRepository.addBugs(new BugImpl("Bug1",
                "LongTitleOfBug",
                "SomethingElseOrDescription",
                Priority.LOW,
                Severity.CRITICAL,
                testMember));
        parameters.add(0, "statusandassignee");
        parameters.add(1, "active");

        Assertions.assertNotNull(testCommand.execute(parameters));
    }
}
