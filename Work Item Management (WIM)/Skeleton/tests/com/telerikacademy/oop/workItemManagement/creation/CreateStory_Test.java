package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.CreateStory;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.BoardImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CreateStory_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;

    @BeforeEach
    public void before() {
        ManagementFactory managementFactory = new ManagementFactoryImpl();
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new CreateStory(managementRepository, managementFactory);
    }


    @Test
    public void execute_should_throwException_when_invalidParameter() {
        parameters = Arrays.asList("Board33", "IDS2000", "StoryTitle",
                "RepoStory.", "Medium", "Small", "Lubomir", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_boardNotFound() {
        parameters = Arrays.asList("Board34", "IDS2000", "StoryTitle", "RepoStory.", "Medium", "Small", "Lubomir");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void execute_should_returnStory_when_validParameter() {
        Member member = new MemberImpl("Lubomir");
        Board board = new BoardImpl("Board33");
        managementRepository.addBoard("Board33", board);
        managementRepository.addMember("Lubomir", member);

        parameters = Arrays.asList("Board33", "IDS2000", "StoryTitle", "RepoStory.", "Medium", "Small", "Lubomir");
        testCommand.execute(parameters);

        Assertions.assertTrue(board.getWorkItem()
                .stream()
                .anyMatch(item -> item.getID().equalsIgnoreCase("IDS2000")));
    }
}
