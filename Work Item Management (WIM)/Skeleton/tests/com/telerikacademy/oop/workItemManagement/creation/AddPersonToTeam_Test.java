package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.AddPersonToTeam;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.TeamImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddPersonToTeam_Test {

    private ManagementRepository managementRepository;
    private List<String> parameters;
    private Command testCommand;
    private Member testMember;
    private Team testTeam;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new AddPersonToTeam(managementRepository);
        testMember = new MemberImpl("Pesho");
        parameters = new ArrayList<>();
        testTeam = new TeamImpl("Omega");
    }


    @Test
    public void execute_should_throwException_when_incorrectNumberOfArguments() {
        parameters = Arrays.asList("Omega", "Pesho", "InvalidParameter");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


    @Test
    public void execute_should_throwException_when_personNotFound() {
        managementRepository.addMember("Pesho", testMember);
        managementRepository.addTeam("Omega", testTeam);
        parameters = Arrays.asList("Omega", "Frank");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


    @Test
    public void execute_should_throwException_when_teamNotFound() {
        managementRepository.addMember("Pesho", testMember);
        managementRepository.addTeam("Omega", testTeam);
        parameters = Arrays.asList("InvalidTeam", "Pesho");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


    @Test
    public void execute_should_returnPersonFromTeam_when_validParameter() {
        managementRepository.addMember("Pesho", testMember);
        managementRepository.addTeam("Omega", testTeam);
        parameters = Arrays.asList("Omega", "Pesho");
        testCommand.execute(parameters);

        Assertions.assertTrue(testTeam.getMembers()
                .stream()
                .anyMatch(member -> member.getName().equalsIgnoreCase("Pesho")));
    }

}
