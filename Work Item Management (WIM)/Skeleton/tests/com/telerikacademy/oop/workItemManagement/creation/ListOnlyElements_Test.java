package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ListOnlyElements;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BugImpl;
import com.telerikacademy.oop.workItemManagement.models.FeedbackImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.StoryImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ListOnlyElements_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private List<String> parameters;
    private Member testMember;

    @BeforeEach
    private void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ListOnlyElements(managementRepository);
        parameters = new ArrayList<>();
        testMember = new MemberImpl("Pavel");
    }

    @Test
    public void Execute_Should_ThrowException_When_InvalidNumberOfArguments() {
        parameters.add("Argument1");
        parameters.add("Argument2");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListOnlyElementsByBugsIsEmpty() {
        parameters.add("bugs");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListOnlyElementsByStoriesIsEmpty() {
        parameters.add("stories");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListOnlyElementsByFeedbackIsEmpty() {
        parameters.add("feedback");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Should_ThrowException_When_ListOnlyElementsByAllWorkItemsIsEmpty() {
        parameters.add("allworkitems");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }


    @Test
    public void Should_ThrowException_When_InvalidArgument() {
        parameters.add("SomethingElse");
        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_ListOnlyByBugs_When_ValidArgument() {
        managementRepository.addBugs(new BugImpl("Bug1",
                "LongTitleOfBug",
                "SomethingElseOrDescription",
                Priority.LOW,
                Severity.CRITICAL,
                testMember));
        parameters.add("bugs");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_ListOnlyByStories_When_ValidArgument() {
        managementRepository.addStory(new StoryImpl("Story1",
                "LongTitleOfStory",
                "SomethingElseOrDescription",
                Priority.LOW,
                Size.LARGE,
                testMember));
        parameters.add("stories");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_ListOnlyByFeedback_When_ValidArgument() {
        managementRepository.addFeedback(new FeedbackImpl("Feedback1",
                "LongTitleOfFeedback",
                "SomethingElseOrDescription",
                4));
        parameters.add("feedback");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }

    @Test
    public void Execute_Should_ListOnlyAllWorkItems_When_ValidArgument() {
        managementRepository.addStory(new StoryImpl("Story1",
                "LongTitleOfStory",
                "SomethingElseOrDescription",
                Priority.LOW,
                Size.LARGE,
                testMember));

        managementRepository.addStory(new StoryImpl("Story1",
                "LongTitleOfStory",
                "SomethingElseOrDescription",
                Priority.LOW,
                Size.LARGE,
                testMember));

        managementRepository.addFeedback(new FeedbackImpl("Feedback1",
                "LongTitleOfFeedback",
                "SomethingElseOrDescription",
                4));

        parameters.add("allworkitems");
        Assertions.assertNotNull(testCommand.execute(parameters));
    }
}
