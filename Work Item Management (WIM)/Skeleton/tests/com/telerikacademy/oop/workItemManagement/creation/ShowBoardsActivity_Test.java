package com.telerikacademy.oop.workItemManagement.creation;

import com.telerikacademy.oop.workItemManagement.commands.contracts.Command;
import com.telerikacademy.oop.workItemManagement.commands.creations.ShowBoardsActivity;
import com.telerikacademy.oop.workItemManagement.core.ManagementRepositoryImpl;
import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workItemManagement.models.BoardImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShowBoardsActivity_Test {
    private ManagementRepository managementRepository;
    private Command testCommand;
    private Board testBoard;
    private List<String> items;

    @BeforeEach
    public void before() {
        managementRepository = new ManagementRepositoryImpl();
        testCommand = new ShowBoardsActivity(managementRepository);
        testBoard = new BoardImpl("BlackBoard");
        items = new ArrayList<>();
    }


    @Test
    public void execute_should_throwException_when_listBoardIsEmpty() {
        items = Collections.singletonList("");

        Assertions.assertThrows(IllegalArgumentException.class, () -> testCommand.execute(items));
    }

    @Test
    public void execute_should_returnCountWorkItem_when_validParameter() {
        items = Collections.singletonList("");
        managementRepository.addBoard("BlackBoard", testBoard);

        testCommand.execute(items);

        Assertions.assertSame(1, managementRepository.getBoards().size());
    }

}
