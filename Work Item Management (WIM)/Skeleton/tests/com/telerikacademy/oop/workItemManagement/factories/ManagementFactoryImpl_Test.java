package com.telerikacademy.oop.workItemManagement.factories;

import com.telerikacademy.oop.workItemManagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workItemManagement.core.factories.ManagementFactoryImpl;
import com.telerikacademy.oop.workItemManagement.models.MemberImpl;
import com.telerikacademy.oop.workItemManagement.models.contracts.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ManagementFactoryImpl_Test {

    private ManagementFactory factory;
    private Member testMember;

    @BeforeEach
    private void before() {
        this.factory = new ManagementFactoryImpl();
        testMember = new MemberImpl("Gosho");
    }


    @Test
    public void createTeam_should_createNewTeam_when_withValidName() {

        Team team = factory.createTeam("TeamForce");

        Assertions.assertEquals("TeamForce", team.getName());
    }

    @Test
    public void createBoard_should_CreateBoard_when_withValidName() {

        Board board = factory.createBoard("BlackBoard");

        Assertions.assertTrue(board != null);
    }

    @Test
    public void createMember_should_CreateMember_when_withValidName() {

        Member member = factory.createMember("Ivancho");

        Assertions.assertTrue(member != null);
    }

    @Test
    public void createBug_should_CreateBug_when_withValidTitle() {

        Bug bug = factory.createBugs("BugNumber4", "Title_must_be_more_than_10_symbols",
                "Mo6veBugTitle22",
                "Low", "Minor", testMember);

        Assertions.assertTrue(bug instanceof WorkItems);
    }

    @Test
    public void createStory_should_CreateStory_when_withValidTitle() {

        Story story = factory.createStory("StoryID", "Title_must_be_more_than_10_symbols",
                "Mo6veBugTitle22",
                "Low", "Small", testMember);

        Assertions.assertTrue(story instanceof WorkItems);
    }

    @Test
    public void createFeedback_should_CreateFeedback_when_ratingIsMoreThanZero() {

        Feedback feedback = factory.createFeedback("StoryID", "Title_must_be_more_than_10_symbols",
                "Mo6veBugTitle22",
                6);

        Assertions.assertTrue(feedback instanceof WorkItems);
    }
}
