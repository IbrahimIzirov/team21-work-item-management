package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.*;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import com.telerikacademy.oop.workItemManagement.models.enums.Size;
import com.telerikacademy.oop.workItemManagement.models.enums.StatusBug;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class WorkItemsImpl_Test {
    private Bug testBug;
    private Story testStory;
    private Member testMember;

    @BeforeEach
    public void before() {
        testMember = new MemberImpl("Gosho");
        testBug = new BugImpl("71", "FireBugWithMoreThan10Symbols",
                "This_bug_can_be_very_impressiv",
                Priority.LOW, Severity.MINOR, testMember);
        testStory = new StoryImpl("721", "StoryTitleWithMoreThen20Symbols",
                "This_story_is_very_impressiv",
                Priority.HIGH, Size.MEDIUM, testMember);
        Feedback testFeedback = new FeedbackImpl("128", "Title_of_feedback",
                "this_is_descripton_of_feedback", 6);
    }

    @Test
    public void constructor_should_throwError_when_TitleOfBugIsNull() {

        Assertions.assertThrows(NullPointerException.class,
                () -> new BugImpl("71", null,
                        "This_bug_can_be_very_impressiv",
                        Priority.LOW, Severity.MINOR, testMember));
    }

    @Test
    public void constructor_should_throwError_when_IdOfStoryIsNull() {

        Assertions.assertThrows(NullPointerException.class,
                () -> new StoryImpl(null, "StoryTitleWithMoreThen20Symbols",
                        "This_story_is_very_impressiv",
                        Priority.HIGH, Size.MEDIUM, testMember));
    }

    @Test
    public void constructor_should_throwError_when_descriptionOfFeedbackIsLessThen10Symbols() {

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new FeedbackImpl("128", "Title_of_feedback",
                        "descript", 6));
    }

    @Test
    public void add_should_addComment_when_correctDate() {

        testBug.addComment("This is a comment");

        Assertions.assertEquals(1, testBug.getComment().size());
    }

    @Test
    public void getHistory_should_returnShallowCopy() {

        List<EventLogImpl> supposedShallowCopy = testBug.getHistory();

        testBug.changeSeverity(Severity.CRITICAL);

        Assertions.assertEquals(2, supposedShallowCopy.size());
    }

    @Test
    public void getPriority_should_return_CorrectPriority() {

        Assertions.assertEquals(Priority.HIGH, testStory.getPriority());
    }

    @Test
    public void getSeverity_should_return_CorrectSeverity() {

        Assertions.assertEquals(Severity.MINOR, testBug.getSeverity());
    }

    @Test
    public void getStatus_should_return_FirstStatusInBug() {

        Assertions.assertEquals(String.valueOf(StatusBug.ACTIVE), testBug.getStatus());
    }

    @Test
    public void getSize_should_return_CorrectSizeOfStory() {

        Assertions.assertEquals(Size.MEDIUM, testStory.getSize());
    }
}
