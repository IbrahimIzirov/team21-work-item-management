package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.WorkItems;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class MemberImpl_Test {

    private BugImpl testBug;
    private Member testMember;

    @BeforeEach
    public void before() {
        testMember = new MemberImpl("GoshoBorda");
        testBug = new BugImpl("71", "FireBugWithMoreThan10Symbols", "This_bug_can_be_very_impressiv", Priority.LOW, Severity.MINOR, testMember);
    }

    @Test
    public void constructor_should_throwError_when_NameIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new MemberImpl(null));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsMoreThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MemberImpl("TestNameWith15Symbols"));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsLessThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MemberImpl("Team"));
    }

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        Member member = new MemberImpl("Griffindor22");

        Assertions.assertEquals(member.getName(), "Griffindor22");
    }

    @Test
    public void getWorkItems_should_returnShallowCopy() {
        Member member = new MemberImpl("Ivancho");

        List<WorkItems> supposedShallowCopy = member.getWorkItem();

        member.addWorkItems(testBug);

        Assertions.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void add_should_addWorkItems_when_passedPresentWorkItems() {
        Member member = new MemberImpl("Stefan");

        member.addWorkItems(testBug);

        Assertions.assertEquals(1, member.getWorkItem().size());
    }

    @Test
    public void remove_should_removeWorkItems_when_passedPresentWorkItems() {
        Member member = new MemberImpl("Marian");

        member.addWorkItems(testBug);

        member.removeWorkItem(testBug);

        Assertions.assertEquals(0, member.getWorkItem().size());
    }
}
