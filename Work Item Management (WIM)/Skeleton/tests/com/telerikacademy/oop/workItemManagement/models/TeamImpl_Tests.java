package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.Board;
import com.telerikacademy.oop.workItemManagement.models.contracts.Member;
import com.telerikacademy.oop.workItemManagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TeamImpl_Tests {

    private Member testMember;
    private Board testBoard;

    @BeforeEach
    public void before() {
        testMember = new MemberImpl("Gosho");
        testBoard = new BoardImpl("BlackBoard");
    }

    @Test
    public void constructor_should_throwError_when_NameIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new TeamImpl(null));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsMoreThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TeamImpl("TestNameWith15Symbols"));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsLessThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TeamImpl("Team"));
    }

    @Test
    public void constructor_should_returnTeam_when_valuesAreValid() {
        Team team = new TeamImpl("Griffindor");

        Assertions.assertEquals(team.getName(), "Griffindor");
    }

    @Test
    public void getMembers_should_returnShallowCopy() {
        Team team = new TeamImpl("Telerik");

        List<Member> supposedShallowCopy = team.getMembers();

        team.addMember(testMember);

        Assertions.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void getBoards_should_returnShallowCopy() {
        Team team = new TeamImpl("Telerik");

        List<Board> supposedShallowCopy = team.getAllBoards();
        team.addBoard(testBoard);

        Assertions.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void add_should_addBoard_when_passedPresentWorkItems() {
        Team team = new TeamImpl("TheWolf");

        team.addBoard(testBoard);

        Assertions.assertEquals(1, team.getAllBoards().size());
    }

    @Test
    public void add_should_addMember_when_passedPresentWorkItems() {
        Team team = new TeamImpl("TheWolf");

        team.addMember(testMember);

        Assertions.assertEquals(1, team.getMembers().size());
    }

    @Test
    public void catalogMembers_should_thrownException_when_invalidDate_return() {
        Team team = new TeamImpl("TheWolf");

        team.addMember(testMember);

        team.catalogMembers();

        Assertions.assertEquals(1, team.getMembers().size());
    }

    @Test
    public void catalogBoards_should_thrownException_when_invalidDate_return() {
        Team team = new TeamImpl("TheWolf");

        team.addBoard(testBoard);

        team.catalogBoards();

        Assertions.assertEquals(1, team.getAllBoards().size());
    }
}
