package com.telerikacademy.oop.workItemManagement.models;

import com.telerikacademy.oop.workItemManagement.models.contracts.*;
import com.telerikacademy.oop.workItemManagement.models.enums.Priority;
import com.telerikacademy.oop.workItemManagement.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class BoardImpl_Test {

    private BugImpl testBug;
    private Member testMember;

    @BeforeEach
    public void before() {
        testMember = new MemberImpl("Gosho");
        testBug = new BugImpl("71", "FireBugWithMoreThan10Symbols", "This_bug_can_be_very_impressiv", Priority.LOW, Severity.MINOR, testMember);
    }

    @Test
    public void constructor_should_throwError_when_NameIsNull() {
        Assertions.assertThrows(NullPointerException.class,
                () -> new BoardImpl(null));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsMoreThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BoardImpl("TestNameWith15Symbols"));
    }

    @Test
    public void constructor_should_throwError_when_nameLengthIsLessThanRequired() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BoardImpl("Team"));
    }

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        Board board = new BoardImpl("Griffindor22");

        Assertions.assertEquals(board.getName(), "Griffindor22");
    }

    @Test
    public void getWorkItems_should_returnShallowCopy() {
        Board board = new BoardImpl("Telerik");

        List<WorkItems> supposedShallowCopy = board.getWorkItem();
        board.addWorkItems(testBug);

        Assertions.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void add_should_addWorkItems_when_passedPresentWorkItems() {
        Board board = new BoardImpl("TheWolf");

        board.addWorkItems(testBug);

        Assertions.assertEquals(1, board.getWorkItem().size());
    }

    @Test
    public void remove_should_removeWorkItems_when_passedPresentWorkItems() {
        Board board = new BoardImpl("TheWolf");

        board.addWorkItems(testBug);

        board.removeWorkItem(testBug);

        Assertions.assertEquals(0, board.getWorkItem().size());
    }
}
